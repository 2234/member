package com.member.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.member.entity.Organizer;

public interface OrganizerService {
	void insertOrganizer(String name, String phone);

	PageInfo<Organizer> selectAllOrganizer(Integer pageNum, Integer pageSize);

	void updateOrganizer(String id, String name, String phone);

	Organizer selectOrganizer(String id);

	void deleteOrganizer(String id);

	List<Organizer> findAll();

}
