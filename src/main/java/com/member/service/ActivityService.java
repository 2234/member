package com.member.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.member.entity.ChuTuan;
import com.member.entity.GoodsActivity;
import com.member.entity.Shopping;
import com.member.entity.YeWu;
import com.member.entity.base.BaseModel;
import com.member.entity.dto.ChuTuanDTO;
import com.member.entity.dto.YeWuDTO;
import com.member.entity.vo.ChuTuanVo;
import com.member.entity.vo.YeWuVo;

public interface ActivityService {
	public BaseModel insertChuTuanActivity(ChuTuanDTO voActivity);
	
	public PageInfo<ChuTuan> allActivityList(Integer pageNum,Integer pageSize);

	public void insertYeWu(YeWuDTO yewuDTO);

	public ChuTuanVo findChuTuanById(String id);

	public List<GoodsActivity> findChuTuanTaoChanById(String id);
	public List<GoodsActivity> findLipinById(String id);

	public PageInfo<YeWu> yeWuActivityList(Integer pageNum, Integer pageSize);

	public List<Shopping> findShoppingListByYeWuId(String id);

	public YeWuVo findYeWuVoById(String id);

	public void updateYeWu(YeWuDTO shop);

	public List<YeWu> findYeWuListByActivityId(String id);

	public void insertLiPin(GoodsActivity goodsActivtity);
	public void delLiPin(String id,String activtityId);
	public void deleteChuTuanById(String id);
}
