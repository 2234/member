package com.member.service;

import com.github.pagehelper.PageInfo;
import com.member.entity.Employee;

public interface EmployeeService {
	// 添加员工
	void insertEmployee(String name, String age, String birthday, String address, String entryTime, String probation,
			String remarks);

	// 查找所有员工
	PageInfo<Employee> selectAllEmployee(Integer pageNum, Integer pageSize);

	// 删除员工
	void deleteEmployee(String id);

	// 模态框回显员工信息
	Employee selectEmployee(String id);

	// 修改员工信息
	void updateEmployee(String id,String name, String age, String birthday, String address, String entryTime, String probation,
			String remarks);

	PageInfo<Employee> findAllByFilter(String name, Integer pageNum, Integer pageSize);
}
