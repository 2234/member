package com.member.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.member.entity.BusinessManager;

public interface ManagerService {

	void insertManager(String name, String phone);

	PageInfo<BusinessManager> selectAllManager(Integer pageNum, Integer pageSize);

	void updateManager(String id, String name, String phone);

	BusinessManager selectManager(String id);

	void deleteManager(String id);
	List<BusinessManager> findAll();
	
	
	
}
