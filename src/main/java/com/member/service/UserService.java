package com.member.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.member.entity.GoodsActivity;
import com.member.entity.User;
import com.member.entity.vo.UserShoppingVo;

public interface UserService {
	// 添加会员
	void insertUser(String name, String idNum, String businessManager, String address, String phone, String cardNum,
			String createCardTime, String integration, String level, String organizerName);

	// 查找所有会员
	PageInfo<User> selectAllUser(Integer pageNum, Integer pageSize);

	// 按建卡时间排序
	PageInfo<User> sortUser(String sortNum, Integer pageNum, Integer pageSize);

	// 删除会员
	void deleteUser(String id);

	// 模态框回显会员信息
	User selectUser(String id);

	// 修改会员信息
	void updateUser(String id, String idNum, String businessManager, String name, String address, String phone,
			String cardNum, String createCardTime, String integration, String level,String organizerName);

	//根据条件查找会员
	PageInfo<User> selectUserByKey(String key, String sortNum, Integer pageNum, Integer pageSize);

	List<User> checkUser(String name);

	List<String> findAllAddress();

	PageInfo<UserShoppingVo> findshoplist(String name, String phone,Integer pageNum, Integer pageSize);

	List<GoodsActivity> findShoppingTaoChanById(String id);

	void updateTuihuo(String id, String name, String phone);
	
	List<User> findAllUser();
}
