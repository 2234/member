package com.member.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.member.entity.Goods;
import com.member.entity.base.BaseModel;
import com.member.entity.base.BaseModelJson;
import com.member.entity.vo.GoodsDTO;

public interface GoodsService {

	PageInfo<Goods> selectAllGoods(Integer pageNum, Integer pageSize);

	void deleteGoods(String id);

	Goods selectGoods(String id);

	BaseModel insertGoods(GoodsDTO dto);

	void updateGoods(Goods goods);
	
	PageInfo<Goods> findAllByKindOrName(String findByKind, String kindName, Integer pageNum, Integer pageSize);

	void updateGoodsInventory(String id, Integer num);

	BaseModelJson<List<Goods>> checkGoods(String goodsName);

}