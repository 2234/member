package com.member.service;

import com.member.entity.Admin;

public interface AdminService {
	public Admin login(String account,String password);
}
