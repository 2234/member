package com.member.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.member.common.utils.UUIDUtil;
import com.member.entity.BusinessManager;
import com.member.mapper.BusinessManagerMapper;
import com.member.service.ManagerService;

@Service
public class ManagerServiceImpl implements ManagerService {

	@Autowired
	BusinessManagerMapper businessManagerMapper;
	
	@Override
	public void insertManager(String name, String phone) {
		BusinessManager manager = new BusinessManager();
		manager.setId(UUIDUtil.getRandom32PK());
		manager.setName(name);
		manager.setPhone(phone);
		manager.setState(0);
		businessManagerMapper.insert(manager);

	}

	@Override
	public PageInfo<BusinessManager> selectAllManager(Integer pageNum, Integer pageSize) {
		List<BusinessManager> list = new ArrayList<BusinessManager>();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", "0");
		PageHelper.startPage(pageNum, pageSize, true);
		list = businessManagerMapper.findAllByFilter(map);
		return new PageInfo<BusinessManager>(list);
	}

	@Override
	public void updateManager(String id, String name, String phone) {
		BusinessManager manager = new BusinessManager();
		manager.setId(id);
		manager.setName(name);
		manager.setPhone(phone);
		businessManagerMapper.update(manager);
	}

	@Override
	public BusinessManager selectManager(String id) {
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("id", id);
		BusinessManager businessManager = businessManagerMapper.selectBusinessManager(map);
		return businessManager;
	}

	@Override
	public void deleteManager(String id) {
		BusinessManager businessManager = new BusinessManager();
		businessManager.setId(id);
		businessManager.setState(1);
		businessManagerMapper.update(businessManager);
	}

	@Override
	public List<BusinessManager> findAll() {
		Map<String,Object> map=new HashMap<>();
		map.put("state", "0");
	return businessManagerMapper.findAllByFilter(map);
	}

}
