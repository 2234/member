package com.member.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.member.common.utils.UUIDUtil;
import com.member.entity.Goods;
import com.member.entity.base.BaseModel;
import com.member.entity.base.BaseModelJson;
import com.member.entity.vo.GoodsDTO;
import com.member.entity.vo.SpecVo;
import com.member.mapper.GoodsMapper;
import com.member.service.GoodsService;

@Service
public class GoodsServiceImpl implements GoodsService {

	@Autowired
	GoodsMapper goodsMapper;


	//列表查询
	public PageInfo<Goods> selectAllGoods(Integer pageNum, Integer pageSize) {
		List<Goods> list = new ArrayList<Goods>();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", "0");
		PageHelper.startPage(pageNum, pageSize, true);
		list = goodsMapper.findAllByFilter(map);
		return new PageInfo<Goods>(list);
	}
	//删除
	public void deleteGoods(String id) {
		Goods goods = new Goods();
		goods.setId(id);
		goods.setState(1);
		goodsMapper.update(goods);
	}
	//修改前查询
	public Goods selectGoods(String id) {
		return goodsMapper.findById(id);
	}
	//修改
	public void updateGoods(Goods goods) {
		goodsMapper.update(goods);
	}

	@Override
	public PageInfo<Goods> findAllByKindOrName(String findByKind, String kindName, Integer pageNum, Integer pageSize) {
		PageHelper.startPage(pageNum, pageSize, true);
		if (findByKind.equals("1")) {//根据商品名
			List<Goods> list = goodsMapper.findAllByKind(kindName);
			return new PageInfo<Goods>(list);
		} else if (findByKind.equals("2")) {//根据类别名
			List<Goods> list = goodsMapper.findAllByName(kindName);
			return new PageInfo<Goods>(list);
		}
		return null;
	}

	//添加
	@Override
	public BaseModel insertGoods(GoodsDTO dto) {
		for(SpecVo vo:dto.getList()){
			Goods goods = new Goods();
			goods.setId(UUIDUtil.getRandom32PK());
			goods.setName(dto.getName());
			goods.setKind(dto.getKind());
			goods.setState(0);
			goods.setSpec(vo.getSpec());
			goods.setCost_price(vo.getCost_price());
			goods.setRetail_price(vo.getRetail_price());
			goods.setTotal_inventory(vo.getTotal_inventory());
			goods.setExisting_inventory(vo.getTotal_inventory());
			goods.setTransfer_cargo("0");
			goods.setOut_goods_num("0");
			goodsMapper.insert(goods);
		}
		BaseModel result=new BaseModel();
		result.Successful=true;
		return result;
	}
	@Override
	public void updateGoodsInventory(String id, Integer num) {
			Goods goods=goodsMapper.findById(id);
			Integer totalnventory=Integer.valueOf(goods.getTotal_inventory());
			Integer existingnventory=Integer.valueOf(goods.getExisting_inventory());
			Integer outGoodsum=Integer.valueOf(goods.getOut_goods_num());
			
			System.out.println(existingnventory);
			existingnventory=existingnventory+num;
			System.out.println(existingnventory);
			goods.setExisting_inventory(existingnventory+"");
			
			if(0>num){
				System.out.println(outGoodsum);
				outGoodsum=outGoodsum-num;
				System.out.println(outGoodsum);
				goods.setOut_goods_num(outGoodsum+"");
			}else{
				System.out.println(totalnventory);
				totalnventory=totalnventory+num;
				System.out.println(totalnventory);
				goods.setTotal_inventory(totalnventory+"");
			}
			goodsMapper.update(goods);
	}
	@Override
	public BaseModelJson<List<Goods>> checkGoods(String goodsName) {
		Map<String,Object> map=new HashMap<>();
		map.put("name1", goodsName);
		List<Goods> list=	goodsMapper.findAllByFilter(map);
		BaseModelJson<List<Goods>> result=new BaseModelJson<List<Goods>>();
		
		if(null!=list&&list.size()>0){
			result.Successful=true;
			result.Data=list;
		}else{
			result.Successful=false;
		}
			return result;
		}
}
