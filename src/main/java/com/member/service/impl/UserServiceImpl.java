package com.member.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.member.common.utils.UUIDUtil;
import com.member.entity.Goods;
import com.member.entity.GoodsActivity;
import com.member.entity.Shopping;
import com.member.entity.User;
import com.member.entity.vo.UserShoppingVo;
import com.member.mapper.GoodsActivityMapper;
import com.member.mapper.GoodsMapper;
import com.member.mapper.ShoppingMapper;
import com.member.mapper.UserMapper;
import com.member.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserMapper userMapper;
	@Autowired
	ShoppingMapper shoppingMapper;
	@Autowired
	GoodsActivityMapper  goodsActivityMapper;
	@Autowired
	GoodsMapper  goodsMapper;
	public void insertUser(String name, String idNum,String businessManager, String address, String phone, String cardNum, String createCardTime,
			String integration, String level,String organizerName) {
		User user = new User();
		user.setId(UUIDUtil.getRandom32PK());
		user.setName(name);
		user.setIdNum(idNum);
		user.setBusinessManager(businessManager);
		user.setAddress(address);
		user.setPhone(phone);
		user.setCardNum(cardNum);
		user.setCreateCardTime(createCardTime);
		user.setIntegration(integration);
		user.setLevel(level);
		user.setState(0);
		user.setOrganizerName(organizerName);
		userMapper.insert(user);

	}

	public PageInfo<User> selectAllUser(Integer pageNum, Integer pageSize) {
		List<User> list = new ArrayList<User>();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", "0");
		PageHelper.startPage(pageNum, pageSize, true);
		list = userMapper.findAllByFilter(map);
		return new PageInfo<User>(list);
	}

	public PageInfo<User> sortUser(String sortNum, Integer pageNum, Integer pageSize) {
		Map<String, Object> map = new HashMap<String, Object>();
		PageHelper.startPage(pageNum, pageSize, true);
		if ("1".equals(sortNum)) {
			List<User> list = userMapper.sortUser1(map);
			return new PageInfo<User>(list);
		} else {
			List<User> list = userMapper.sortUser2(map);
			return new PageInfo<User>(list);
		}
	}

	public void deleteUser(String id) {
		User user = new User();
		user.setId(id);
		user.setState(1);
		userMapper.update(user);
	}

	public User selectUser(String id) {
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("id", id);
		User user = userMapper.selectUser(map);
		return user;
	}

	public void updateUser(String id, String name, String idNum,String businessManager,String address, String phone, String cardNum, String createCardTime,
			String integration, String level,String organizerName) {
		User user = new User();
		user.setId(id);
		user.setName(name);
		user.setIdNum(idNum);
		user.setBusinessManager(businessManager);
		user.setAddress(address);
		user.setPhone(phone);
		user.setCardNum(cardNum);
		user.setCreateCardTime(createCardTime);
		user.setIntegration(integration);
		user.setLevel(level);
		user.setOrganizerName(organizerName);
		userMapper.update(user);
	}

	public PageInfo<User> selectUserByKey(String key, String sortNum, Integer pageNum, Integer pageSize) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", "0");
		PageHelper.startPage(pageNum, pageSize, true);
		if ("3".equals(sortNum)) {
			map.put("name", key);
			List<User> list = userMapper.findAllByFilter(map);
			return new PageInfo<User>(list);
		} else if ("4".equals(sortNum)) {
			map.put("phone", key);
			List<User> list = userMapper.findAllByFilter(map);
			return new PageInfo<User>(list);
		} else if ("5".equals(sortNum)) {
			map.put("idNum", key);
			List<User> list = userMapper.findAllByFilter(map);
			return new PageInfo<User>(list);
		} else if ("6".equals(sortNum)) {
			map.put("cardNum", key);
			List<User> list = userMapper.findAllByFilter(map);
			return new PageInfo<User>(list);
		} else if ("7".equals(sortNum)) {
			map.put("businessManager", key);
			List<User> list = userMapper.findAllByFilter(map);
			return new PageInfo<User>(list);
		}
		return null;
	}

	@Override
	public List<User> checkUser(String name) {
		Map<String, Object>map = new HashMap<>();
		map.put("name", name);
		map.put("state", "0");
		return userMapper.findAllByFilter(map);
	}

	@Override
	public List<String> findAllAddress() {
		// TODO Auto-generated method stub
		return userMapper.findAllAddress();
	}

	@Override
	public PageInfo<UserShoppingVo> findshoplist(String name, String phone, Integer pageNum, Integer pageSize) {
		PageHelper.startPage(pageNum, pageSize, true);
		Map<String, Object> map=new HashMap<>();
		map.put("name", name);
		map.put("phone", phone);
		List<UserShoppingVo> list=shoppingMapper.findshoplist(map);
		return new PageInfo<UserShoppingVo>(list);
	}

	@Override
	public List<GoodsActivity> findShoppingTaoChanById(String id) {
		return shoppingMapper.findShoppingTaoChanById(id);
	}

	@Override
	public void updateTuihuo(String id, String name, String phone) {
		Map<String, Object> map=new HashMap<>();
		map.put("name", name);
		map.put("phone", phone);
		map.put("id", id);
		List<UserShoppingVo> list=shoppingMapper.findshoplist(map);
		for(UserShoppingVo vo:list){
			Shopping shop=new Shopping();
			shop.setId(id);
			shop.setState(1);
			shoppingMapper.update(shop);
		}
		// TODO Auto-generated method stub
		List<GoodsActivity> goodslist=	shoppingMapper.findShoppingTaoChanById(id);
		for(GoodsActivity good:goodslist){
			Goods goods=goodsMapper.findGoodsByGoodsNameAndSpec(good.getGoodsName(), good.getGoodsSpec());
			Integer xianyou=Integer.valueOf(goods.getExisting_inventory())+Integer.valueOf(good.getGoodsCount());
			Integer chuku=Integer.valueOf(goods.getOut_goods_num())-Integer.valueOf(good.getGoodsCount());
			goods.setExisting_inventory(String.valueOf(xianyou));
			goods.setOut_goods_num(String.valueOf(chuku));
			goodsMapper.update(goods);
		}

	}

	@Override
	public List<User> findAllUser() {
		// TODO Auto-generated method stub
		Map<String ,Object> map=new HashMap<>();
		map.put("state", "0");
		return userMapper.findAllByFilter(map);
	}

}
