package com.member.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.member.common.utils.UUIDUtil;
import com.member.entity.Employee;
import com.member.entity.Goods;
import com.member.mapper.EmployeeMapper;
import com.member.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeMapper employeeMapper;

	public void insertEmployee(String name, String age, String birthday, String address, String entryTime, String probation,
			String remarks) {
		Employee employee = new Employee();
		employee.setId(UUIDUtil.getRandom32PK());
		employee.setName(name);
		employee.setAge(age);
		employee.setBirthday(birthday);
		employee.setAddress(address);
		employee.setEntryTime(entryTime);
		employee.setProbation(probation);
		employee.setRemarks(remarks);
		employee.setState(0);
		employeeMapper.insert(employee);

	}

	public PageInfo<Employee> selectAllEmployee(Integer pageNum, Integer pageSize) {
		List<Employee> list = new ArrayList<Employee>();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", "0");
		list = employeeMapper.findAllByFilter(map);
		PageHelper.startPage(pageNum, pageSize, true);
		return new PageInfo<Employee>(list);
	}

	

	public void deleteEmployee(String id) {
		Employee employee = new Employee();
		employee.setId(id);
		employee.setState(1);
		employeeMapper.update(employee);
	}

	public Employee selectEmployee(String id) {
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("id", id);
		Employee employee = employeeMapper.selectEmployee(map);
		return employee;
	}

	public void updateEmployee(String id, String name, String age, String birthday, String address, String entryTime, String probation,
			String remarks) {
		Employee employee = new Employee();
		employee.setId(id);
		employee.setName(name);
		employee.setAge(age);
		employee.setBirthday(birthday);
		employee.setAddress(address);
		employee.setEntryTime(entryTime);
		employee.setProbation(probation);
		employee.setRemarks(remarks);
		employeeMapper.update(employee);
	}

	@Override
	public PageInfo<Employee> findAllByFilter(String name, Integer pageNum, Integer pageSize) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("name", name);
		map.put("state", 0);
		PageHelper.startPage(pageNum, pageSize, true);
		List<Employee> list = employeeMapper.findAllByFilter(map);
		return new PageInfo<Employee>(list);
	}

	


}
