package com.member.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.member.common.utils.Md5;
import com.member.entity.Admin;
import com.member.mapper.AdminMapper;
import com.member.service.AdminService;

@Service
public class AdminServiceImpl implements AdminService {

	@Autowired
	AdminMapper mapper;

	@Override
	public Admin login(String account, String password) {
		
		Map<String,Object> map=new HashMap<>();
		map.put("account", account);
		map.put("password", Md5.getVal_UTF8(password));
		return	mapper.login(map);
	}
}