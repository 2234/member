package com.member.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.member.common.utils.DateUtils;
import com.member.common.utils.UUIDUtil;
import com.member.entity.CarActivity;
import com.member.entity.ChuTuan;
import com.member.entity.EatActivity;
import com.member.entity.Goods;
import com.member.entity.GoodsActivity;
import com.member.entity.Picking;
import com.member.entity.Shopping;
import com.member.entity.StayActivity;
import com.member.entity.User;
import com.member.entity.YeWu;
import com.member.entity.base.BaseModel;
import com.member.entity.dto.ChuTuanDTO;
import com.member.entity.dto.YeWuDTO;
import com.member.entity.vo.ChuTuanVo;
import com.member.entity.vo.ShoppingVo;
import com.member.entity.vo.YeWuVo;
import com.member.mapper.CarActivityMapper;
import com.member.mapper.ChuTuanMapper;
import com.member.mapper.EatActivityMapper;
import com.member.mapper.GoodsActivityMapper;
import com.member.mapper.GoodsMapper;
import com.member.mapper.PickingMapper;
import com.member.mapper.ShoppingMapper;
import com.member.mapper.StayActivityMapper;
import com.member.mapper.UserMapper;
import com.member.mapper.YeWuMapper;
import com.member.service.ActivityService;

@Service
public class ActivityServiceImpl implements ActivityService {
	@Autowired
	ChuTuanMapper chuTuanMapper;
	@Autowired
	CarActivityMapper carActivityMapper;
	@Autowired
	EatActivityMapper eatActivityMapper;
	@Autowired
	StayActivityMapper stayActivityMapper;
	@Autowired
	GoodsActivityMapper goodsActivityMapper;
	@Autowired
	YeWuMapper yeWuMapper;
	@Autowired
	ShoppingMapper shopMapper;
	@Autowired
	UserMapper userMapper;
	@Autowired
	GoodsMapper goodsMapper;
	@Autowired
	PickingMapper pickingMapper;

	@Override
	public BaseModel insertChuTuanActivity(ChuTuanDTO voActivity) {
		BaseModel result = new BaseModel();
		// TODO Auto-generated method stub
		 String id = UUIDUtil.getRandom32EndTimePK();
//		String id = DateUtils.currentDateTime();
		Goods trueGood = null;
		for (GoodsActivity goods : voActivity.getLipinList()) {
			trueGood = goodsMapper.findGoodsByGoodsNameAndSpec(goods.getGoodsName(), goods.getGoodsSpec());
			if (null != trueGood) {
				if (null != goods && StringUtils.isNotBlank(goods.getGoodsName())) {
					goods.setId(UUIDUtil.getRandom32PK());
					goods.setActivityId(id);
				}
			} else {
				result.Successful = false;
				result.Message = "礼品:" + goods.getGoodsName() + "-----" + goods.getGoodsSpec() + "-----填写有误";
				return result;
			}
		}
		for (GoodsActivity goods : voActivity.getTaochanList()) {
			trueGood = goodsMapper.findGoodsByGoodsNameAndSpec(goods.getGoodsName(), goods.getGoodsSpec());
			if (null != trueGood) {
				if (null != goods && StringUtils.isNotBlank(goods.getGoodsName())) {
					goods.setId(UUIDUtil.getRandom32PK());
					goods.setActivityId(id);
				}
			} else {
				result.Successful = false;
				result.Message = "货品:" + goods.getGoodsName() + "-----" + goods.getGoodsSpec() + "-----填写有误";
				return result;
			}
		}
		ChuTuan day = new ChuTuan();
		day.setId(id);
		day.setStartTime(voActivity.getStartTime());
		day.setEndTime(voActivity.getEndTime());
		day.setDayNum(voActivity.getDayNum());
		day.setLoadStart(voActivity.getLoadStart());
		day.setLoadEnd(voActivity.getLoadEnd());
		day.setPeopleNum(voActivity.getPeopleNum());
		day.setManagerName(voActivity.getManagerName());
		day.setRemark(voActivity.getRemark());
		day.setEveningSiteCost(voActivity.getEveningShowCost());
		day.setEveningShowCost(voActivity.getEveningSiteCost());
		day.setEveningShareCost(voActivity.getEveningShareCost());
		day.setEveningWorkersCost(voActivity.getEveningWorkersCost());
		day.setSiteCost(voActivity.getSiteCost());
		day.setSiteAllCost(voActivity.getSiteAllCost());
		day.setSiteWorkerCost(voActivity.getSiteWorkerCost());
		day.setSharedCost(voActivity.getSharedCost());
		day.setInsuranceStandard(voActivity.getInsuranceStandard());
		day.setInsuranceCost(voActivity.getInsuranceCost());
		day.setServiceCost(voActivity.getServiceCost());
		day.setCarCost(voActivity.getDeliveryCarCost());
		day.setRemainningCost(voActivity.getRemainingCost());
		day.setGroupMonkey(voActivity.getGroupMonkey());
		day.setGroupMonkeyStandard(voActivity.getGroupMonkeyStandard());
		day.setDeliveryCost(voActivity.getDeliveryCost());
		day.setDinghuoNum(voActivity.getDinghuoNum());
		day.setYshouhuokuanNum(voActivity.getYshouhuokuanNum());
		day.setWshouhuokuanNum(voActivity.getWshouhuokuanNum());
		day.setTuihuokuanNum(voActivity.getTuihuokuanNum());
		day.setSendNum(voActivity.getSendNum());
		day.setShoukuanNum(voActivity.getShoukuanNum());
		day.setEarning(voActivity.getEarning());
		day.setLecturerCost(voActivity.getLecturerCost());
		chuTuanMapper.insert(day);
		for (EatActivity eat : voActivity.getEatList()) {
			if (null != eat && StringUtils.isNotBlank(eat.getEatCost())) {
				eat.setActivityId(id);
			}
			eatActivityMapper.insert(eat);
		}

		for (StayActivity stay : voActivity.getStayList()) {
			if (null != stay && StringUtils.isNotBlank(stay.getStayPosition())) {
				stay.setActivityId(id);
			}
			stayActivityMapper.insert(stay);
		}
		for (CarActivity car : voActivity.getCarList()) {
			if (null != car && StringUtils.isNotBlank(car.getCarModel())) {
				car.setActivityId(id);
			}
			carActivityMapper.insert(car);
		}

		for (GoodsActivity goods : voActivity.getLipinList()) {
			goodsActivityMapper.insert(goods);
		}
		for (GoodsActivity goods : voActivity.getTaochanList()) {
			goodsActivityMapper.insert(goods);
		}
		result.Successful = true;
		return result;
	}

	@Override
	public PageInfo<ChuTuan> allActivityList(Integer pageNum, Integer pageSize) {
		// TODO Auto-generated method stub
		if (pageNum == null)
			pageNum = 1;
		if (pageSize == null)
			pageSize = 10;
		PageHelper.startPage(pageNum, pageSize, true);
		List<ChuTuan> list = chuTuanMapper.findAllByFilter(null);
		return new PageInfo<ChuTuan>(list);
	}

	@Override
	public void insertYeWu(YeWuDTO yewuVo) {
		// 添加业务基础开始
		String id = DateUtils.currentDateSecondTime();
		YeWu yewu = new YeWu();
		yewu.setId(id);
		yewu.setActivityId(yewuVo.getActivityId());
		yewu.setLecturer(yewuVo.getLecturer());
		yewu.setPosition(yewuVo.getPosition());
		yewu.setWorkers(yewuVo.getWorkers());
		yewu.setState(0);
		yeWuMapper.insert(yewu);
		// 添加业务基础结束

		// 添加用户购买开始
		for (ShoppingVo shop : yewuVo.getShopList()) {
			Shopping userShop = new Shopping();
			if (null != shop && StringUtils.isNotBlank(shop.getAdvance())) {
				userShop.setYewuId(yewu.getId());
			}
			String userShopId = UUIDUtil.getRandom32PK();
			userShop.setAddress(shop.getAddress());
			userShop.setAdvance(shop.getAdvance());
			userShop.setDeposit(shop.getDeposit());
			userShop.setId(userShopId);
			userShop.setManagerName(shop.getManagerName());
			userShop.setPaid(shop.getPaid());
			userShop.setPayType(shop.getPayType());
			userShop.setUserName(shop.getUserName());
			userShop.setUserPhone(shop.getUserPhone());
			userShop.setWeikuan(shop.getWeikuan());
			userShop.setState(0);
			shopMapper.insert(userShop);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("name1", shop.getUserName());
			map.put("phone", shop.getUserPhone());
			List<User> list = userMapper.findAllByFilter(map);
			if (!(null != list && list.size() > 0)) {
				User user = new User();
				user.setId(UUIDUtil.getRandom32PK());
				user.setName(shop.getUserName());
				user.setBusinessManager(shop.getManagerName());
				user.setAddress(shop.getAddress());
				user.setPhone(shop.getUserPhone());
				user.setState(0);
				user.setIntegration("0");
				user.setLevel("0");
				userMapper.insert(user);
			}
			for (int i = 0; i < shop.getPickingList().size(); i++) {
				Picking pick = shop.getPickingList().get(i);
				pick.setShoppingId(userShopId);
				pick.setId(UUIDUtil.getRandom32PK());
				pickingMapper.insert(pick);
				if (("1").equals(pick.getState())) {
					Goods goods = goodsMapper.findGoodsByGoodsNameAndSpec(pick.getGoodsName(), pick.getGoodsSpec());
					Integer xianyou = Integer.valueOf(goods.getExisting_inventory())
							- Integer.valueOf(pick.getGoodsCount());
					Integer chuku = Integer.valueOf(goods.getOut_goods_num()) + Integer.valueOf(pick.getGoodsCount());
					goods.setExisting_inventory(String.valueOf(xianyou));
					goods.setOut_goods_num(String.valueOf(chuku));
					goodsMapper.update(goods);
				}
			}
		}

	}

	@Override
	public ChuTuanVo findChuTuanById(String id) {
		ChuTuanVo data = new ChuTuanVo();
		ChuTuan tuan = chuTuanMapper.findById(id);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("activityId", tuan.getId());
		List<EatActivity> eatList = eatActivityMapper.findAllByFilter(map);
		List<StayActivity> stayList = stayActivityMapper.findAllByFilter(map);
		List<CarActivity> carList = carActivityMapper.findAllByFilter(map);
		Map<String, Object> map1 = new HashMap<String, Object>();
		map1.put("activityId", tuan.getId());
		map1.put("type", "1");
		List<GoodsActivity> lipinList = goodsActivityMapper.findAllByFilter(map1);
		Map<String, Object> map2 = new HashMap<String, Object>();
		map2.put("activityId", tuan.getId());
		map2.put("type", "2");
		List<GoodsActivity> taochanList = goodsActivityMapper.findAllByFilter(map2);
		data.setTuan(tuan);
		data.setCarList(carList);
		data.setEatList(eatList);
		data.setStayList(stayList);
		data.setTaochanList(taochanList);
		data.setLipinList(lipinList);
		return data;
	}

	@Override
	public List<GoodsActivity> findChuTuanTaoChanById(String id) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("activityId", id);
		map.put("type", "2");
		List<GoodsActivity> taochanList = goodsActivityMapper.findAllByFilter(map);
		return taochanList;
	}

	@Override
	public PageInfo<YeWu> yeWuActivityList(Integer pageNum, Integer pageSize) {
		// TODO Auto-generated method stub
		if (pageNum == null)
			pageNum = 1;
		if (pageSize == null)
			pageSize = 10;
		PageHelper.startPage(pageNum, pageSize, true);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", 0);
		List<YeWu> list = yeWuMapper.findAllByFilter(map);
		return new PageInfo<YeWu>(list);
	}

	@Override
	public List<Shopping> findShoppingListByYeWuId(String id) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("yewuId", id);
		return shopMapper.findAllByFilter(map);
	}

	@Override
	public YeWuVo findYeWuVoById(String id) {
		// TODO Auto-generated method stub
		YeWuVo data=new YeWuVo();
		YeWu yewu = yeWuMapper.findById(id);
		List<ShoppingVo> shoplist=shopMapper.findShopListByYeWuId(id);
		for(int i=0;i<shoplist.size();i++){
			Map<String,Object> map=new HashMap<>();
			map.put("shoppingId", shoplist.get(i).getId());
			List<Picking> pickingList=pickingMapper.findAllByFilter(map);
			shoplist.get(i).setPickingList(pickingList);
		}
		data.setShopList(shoplist);
		data.setYewu(yewu);
		return data;
	}

	@Override
	public void updateYeWu(YeWuDTO shop) {
		// TODO Auto-generated method stub
		YeWu yewu = new YeWu();
		yewu.setId(shop.getId());
		yewu.setActivityId(shop.getActivityId());
		yewu.setLecturer(shop.getLecturer());
		yewu.setPosition(shop.getPosition());
		yewu.setWorkers(shop.getWorkers());
		yewu.setState(0);
		yeWuMapper.update(yewu);
		for(ShoppingVo vo:shop.getShopList()){
			Shopping userShop = new Shopping();
			userShop.setAddress(vo.getAddress());
			userShop.setAdvance(vo.getAdvance());
			userShop.setDeposit(vo.getDeposit());
			userShop.setId(vo.getId());
			userShop.setManagerName(vo.getManagerName());
			userShop.setPaid(vo.getPaid());
			userShop.setPayType(vo.getPayType());
			userShop.setUserName(vo.getUserName());
			userShop.setUserPhone(vo.getUserPhone());
			userShop.setWeikuan(vo.getWeikuan());
			userShop.setState(0);
			shopMapper.update(userShop);
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("name1", vo.getUserName());
			map.put("phone", vo.getUserPhone());
			List<User> list = userMapper.findAllByFilter(map);
			if (!(null != list && list.size() > 0)) {
				User user = new User();
				user.setId(UUIDUtil.getRandom32PK());
				user.setName(vo.getUserName());
				user.setBusinessManager(vo.getManagerName());
				user.setAddress(vo.getAddress());
				user.setPhone(vo.getUserPhone());
				user.setState(0);
				user.setIntegration("0");
				user.setLevel("0");
				userMapper.insert(user);
			}
			for (int i = 0; i < vo.getPickingList().size(); i++) {
				Picking pick = vo.getPickingList().get(i);
				pick.setShoppingId(vo.getId());
				Picking yuanPick=pickingMapper.findById(pick.getId());
				if (!(yuanPick.getState()).equals(pick.getState())) {
					if(("1").equals(pick.getState())){//原来是未配货 现在是已配货
						Goods goods = goodsMapper.findGoodsByGoodsNameAndSpec(pick.getGoodsName(), pick.getGoodsSpec());
						Integer xianyou = Integer.valueOf(goods.getExisting_inventory())
								- Integer.valueOf(pick.getGoodsCount());
						Integer chuku = Integer.valueOf(goods.getOut_goods_num()) + Integer.valueOf(pick.getGoodsCount());
						goods.setExisting_inventory(String.valueOf(xianyou));
						goods.setOut_goods_num(String.valueOf(chuku));
						goodsMapper.update(goods);
					}else{//原来是已配货 现在是未配货 
						Goods goods = goodsMapper.findGoodsByGoodsNameAndSpec(pick.getGoodsName(), pick.getGoodsSpec());
						Integer xianyou = Integer.valueOf(goods.getExisting_inventory())
								+Integer.valueOf(pick.getGoodsCount());
						Integer chuku = Integer.valueOf(goods.getOut_goods_num())- Integer.valueOf(pick.getGoodsCount());
						goods.setExisting_inventory(String.valueOf(xianyou));
						goods.setOut_goods_num(String.valueOf(chuku));
						goodsMapper.update(goods);
					}
				
				}
				pickingMapper.update(pick);
			}
		}
	}

	@Override
	public List<YeWu> findYeWuListByActivityId(String id) {
		// TODO Auto-generated method stub
		Map<String,Object> map=new HashMap<>();
		map.put("activityId", id);
		return yeWuMapper.findAllByFilter(map);
	}

	@Override
	public void deleteChuTuanById(String id) {
		chuTuanMapper.deleteByActivityId(id);
		eatActivityMapper.deleteByActivityId(id);
		stayActivityMapper.deleteByActivityId(id);
		carActivityMapper.deleteByActivityId(id);
		goodsActivityMapper.deleteByActivityId(id);
		
	}

	@Override
	public List<GoodsActivity> findLipinById(String id) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("activityId", id);
		map.put("type", "1");
		List<GoodsActivity> taochanList = goodsActivityMapper.findAllByFilter(map);
		return taochanList;
	}


	@Override
	public void insertLiPin(GoodsActivity goodsActivtity) {
		// TODO Auto-generated method stub
		//修改出团盈亏情况
		ChuTuan	tuan=	chuTuanMapper.findById(goodsActivtity.getActivityId());
		Double earning=Double.parseDouble(tuan.getEarning())-Double.parseDouble(goodsActivtity.getGoodsCost());
		ChuTuan	gaiTuan=new ChuTuan();
		gaiTuan.setId(tuan.getId());
		gaiTuan.setEarning(earning+"");
		chuTuanMapper.update(gaiTuan);
		//增加礼品
		goodsActivtity.setId(UUIDUtil.getRandom32PK());
		goodsActivityMapper.insert(goodsActivtity);
	}

	@Override
	public void delLiPin(String id, String activtityId) {
		// TODO Auto-generated method stub
		GoodsActivity goods=goodsActivityMapper.findById(id);
		//修改出团盈亏情况
		ChuTuan	tuan=	chuTuanMapper.findById(activtityId);
		Double earning=Double.parseDouble(tuan.getEarning())+Double.parseDouble(goods.getGoodsCost());
		ChuTuan	gaiTuan=new ChuTuan();
		gaiTuan.setId(tuan.getId());
		gaiTuan.setEarning(earning+"");
		chuTuanMapper.update(gaiTuan);
		//删除
		goodsActivityMapper.detele(id);
		
	}

}
