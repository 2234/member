package com.member.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.member.common.utils.UUIDUtil;
import com.member.entity.Organizer;
import com.member.mapper.OrganizerMapper;
import com.member.service.OrganizerService;

@Service
public class OrganizerServiceImpl implements OrganizerService {
	@Autowired
	OrganizerMapper organizerMapper;

	@Override
	public void insertOrganizer(String name, String phone) {
		Organizer organizer = new Organizer();
		organizer.setId(UUIDUtil.getRandom32PK());
		organizer.setName(name);
		organizer.setPhone(phone);
		organizer.setState(0);
		organizerMapper.insert(organizer);

	}

	@Override
	public PageInfo<Organizer> selectAllOrganizer(Integer pageNum, Integer pageSize) {
		List<Organizer> list = new ArrayList<Organizer>();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", "0");
		PageHelper.startPage(pageNum, pageSize, true);
		list = organizerMapper.findAllByFilter(map);
		return new PageInfo<Organizer>(list);
	}

	@Override
	public void updateOrganizer(String id, String name, String phone) {
		Organizer organizer = new Organizer();
		organizer.setId(id);
		organizer.setName(name);
		organizer.setPhone(phone);
		organizerMapper.update(organizer);

	}

	@Override
	public Organizer selectOrganizer(String id) {
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("id", id);
		Organizer organizer = organizerMapper.selectOrganizer(map);
		return organizer;
	}

	@Override
	public void deleteOrganizer(String id) {
		Organizer organizer = new Organizer();
		organizer.setId(id);
		organizer.setState(1);
		organizerMapper.update(organizer);

	}

	@Override
	public List<Organizer> findAll() {
		Map<String,Object> map=new HashMap<>();
		map.put("state", "0");
	return organizerMapper.findAllByFilter(map);
	}

}
