package com.member.service;


import java.util.List;

import com.member.entity.base.BaseModel;
import com.member.entity.vo.BusinessManagerOrOrganizer;
import com.member.entity.vo.UserVo;

public interface MessageService {

	List<BusinessManagerOrOrganizer> select(String id);

	List<UserVo> selectUser(String id);

	BaseModel insertMessage(String managerName, String address, String organizerName,String title, String content);
	
}
