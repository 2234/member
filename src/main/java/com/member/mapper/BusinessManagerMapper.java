package com.member.mapper;

import java.util.Map;

import com.member.entity.BusinessManager;
import com.member.mapper.base.BaseMapper;
/**
 * 业务经理
 * @author jettylee
 *
 */
public interface BusinessManagerMapper extends BaseMapper<String , BusinessManager>{

	BusinessManager selectBusinessManager(Map<String, Object> map);

}
