package com.member.mapper;

import com.member.entity.EatActivity;
import com.member.mapper.base.BaseMapper;
/**
 * 出团活动吃饭
 * @author jettylee
 *
 */
public interface EatActivityMapper extends BaseMapper<String, EatActivity> {
	public	void deleteByActivityId(String id);
}
