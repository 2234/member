package com.member.mapper;

import java.util.Map;

import com.member.entity.Organizer;
import com.member.mapper.base.BaseMapper;

public interface OrganizerMapper extends BaseMapper<String, Organizer>{

	Organizer selectOrganizer(Map<String, Object> map);

}
