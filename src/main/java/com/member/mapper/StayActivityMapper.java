package com.member.mapper;

import com.member.entity.StayActivity;
import com.member.mapper.base.BaseMapper;

public interface StayActivityMapper extends BaseMapper<String, StayActivity> {
	public	void deleteByActivityId(String id);
}
