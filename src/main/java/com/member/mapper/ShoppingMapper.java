package com.member.mapper;

import java.util.List;
import java.util.Map;

import com.member.entity.GoodsActivity;
import com.member.entity.Shopping;
import com.member.entity.vo.ShoppingVo;
import com.member.entity.vo.UserShoppingVo;
import com.member.mapper.base.BaseMapper;

public interface ShoppingMapper extends BaseMapper<String, Shopping> {


	List<UserShoppingVo> findshoplist(Map<String, Object> map);

	List<GoodsActivity> findShoppingTaoChanById(String id);

	List<ShoppingVo> findShopListByYeWuId(String id);


}
