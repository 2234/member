package com.member.mapper;

import com.member.entity.GoodsActivity;
import com.member.mapper.base.BaseMapper;

public interface GoodsActivityMapper extends BaseMapper<String, GoodsActivity> {
	void detele(String id);
	public	void deleteByActivityId(String id);
}
