package com.member.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.member.entity.Goods;
import com.member.entity.vo.GoodsVo;
import com.member.mapper.base.BaseMapper;

public interface GoodsMapper extends BaseMapper<String, Goods> {

	Goods selectGoods(Map<String, Object> map);
	
	List<GoodsVo> selectAllGoods();

	void insertSpec(Map<String, Object> map);

	List<Goods> findAllByKind(String kindName);

	List<Goods> findAllByName(String kindName);

	Goods findGoodsByGoodsNameAndSpec(@Param("goodsName") String goodsName,@Param("goodsSpec")  String goodsSpec);
	
	


}