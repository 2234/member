package com.member.mapper;


import java.util.List;
import java.util.Map;

import com.member.entity.Messages;
import com.member.entity.MessagesRecord;
import com.member.entity.vo.BusinessManagerOrOrganizer;
import com.member.entity.vo.UserVo;
import com.member.mapper.base.BaseMapper;

public interface MessageMapper extends BaseMapper<String, Messages> {

	void insertMessage(MessagesRecord record);

	List<BusinessManagerOrOrganizer> selectBusinessManager();

	List<BusinessManagerOrOrganizer> selectOrganizer();

	List<UserVo> selectUser(String id);
	
}
