package com.member.mapper;

import com.member.entity.ChuTuan;
import com.member.mapper.base.BaseMapper;
/**
 * 出团活动
 * @author jettylee
 *
 */
public interface ChuTuanMapper extends BaseMapper<String, ChuTuan> {
	public	void deleteByActivityId(String id);
}
