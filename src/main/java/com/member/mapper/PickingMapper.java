package com.member.mapper;

import com.member.entity.Picking;
import com.member.mapper.base.BaseMapper;

public interface PickingMapper extends BaseMapper<String, Picking> {

}
