package com.member.mapper;

import java.util.List;
import java.util.Map;

import com.member.entity.User;
import com.member.mapper.base.BaseMapper;

public interface UserMapper extends BaseMapper<String, User> {

	//最新会员优先
	List<User> sortUser1(Map<String, Object> map);
	
	//最老会员优先
	List<User> sortUser2(Map<String, Object> map);

	//模态框回显会员数据
	User selectUser(Map<String, Object> map);

	List<String> findAllAddress();

	

}