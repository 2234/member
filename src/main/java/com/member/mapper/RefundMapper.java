package com.member.mapper;

import com.member.entity.Refund;
import com.member.mapper.base.BaseMapper;

public interface RefundMapper extends BaseMapper<String, Refund> {

}
