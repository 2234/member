package com.member.mapper;

import com.member.entity.RefundDetails;
import com.member.mapper.base.BaseMapper;

public interface RefundDetailsMapper extends BaseMapper<String, RefundDetails> {

}
