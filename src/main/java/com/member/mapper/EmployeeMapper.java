package com.member.mapper;

import java.util.Map;

import com.member.entity.Employee;
import com.member.mapper.base.BaseMapper;

public interface EmployeeMapper extends BaseMapper<String, Employee> {

	//模态框回显会员数据
	Employee selectEmployee(Map<String, Object> map);
	

}