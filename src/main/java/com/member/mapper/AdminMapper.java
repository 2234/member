package com.member.mapper;

import java.util.Map;

import com.member.entity.Admin;
/**
 * 管理员帐号密码
 * @author jettylee
 *
 */
public interface AdminMapper {
	public Admin login(Map<String,Object> map);
	public void updateAdmin(Map<String,Object> map);
}
