package com.member.mapper;

import com.member.entity.CarActivity;
import com.member.mapper.base.BaseMapper;
/**
 * 车辆
 * @author jettylee
 *
 */
public interface CarActivityMapper extends BaseMapper<String, CarActivity> {
	public	void deleteByActivityId(String id);
}
