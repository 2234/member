package com.member.entity;

import com.member.entity.base.BaseEntity;

/**
 * 活动基础
 * @author jettylee
 *
 */
public class StayActivity implements BaseEntity<Integer>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;//主键
	private String activityId;//活动ID
	private Integer dayNum;//第几天
	private String stayPosition;
	private String stayStandard;
	private String stayCost;
	private String stayRemark;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getActivityId() {
		return activityId;
	}
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	public Integer getDayNum() {
		return dayNum;
	}
	public void setDayNum(Integer dayNum) {
		this.dayNum = dayNum;
	}
	public String getStayPosition() {
		return stayPosition;
	}
	public void setStayPosition(String stayPosition) {
		this.stayPosition = stayPosition;
	}
	public String getStayStandard() {
		return stayStandard;
	}
	public void setStayStandard(String stayStandard) {
		this.stayStandard = stayStandard;
	}
	public String getStayCost() {
		return stayCost;
	}
	public void setStayCost(String stayCost) {
		this.stayCost = stayCost;
	}
	
	public String getStayRemark() {
		return stayRemark;
	}
	public void setStayRemark(String stayRemark) {
		this.stayRemark = stayRemark;
	}
	@Override
	public String toString() {
		return "StayActivity [id=" + id + ", activityId=" + activityId + ", dayNum=" + dayNum + ", stayPosition="
				+ stayPosition + ", stayStandard=" + stayStandard + ", stayCost=" + stayCost + "]";
	}
	
	
	
}
