package com.member.entity;
import com.member.entity.base.BaseEntity;

public class RefundDetails implements BaseEntity<String>{
	private String id;
	private String refundId;
	private String goodsName;
	private String goodsSpec;
	private String goodsCount;
	private String goodsMonkey;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGoodsName() {
		return goodsName;
	}
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	public String getGoodsSpec() {
		return goodsSpec;
	}
	public void setGoodsSpec(String goodsSpec) {
		this.goodsSpec = goodsSpec;
	}
	public String getGoodsCount() {
		return goodsCount;
	}
	public void setGoodsCount(String goodsCount) {
		this.goodsCount = goodsCount;
	}
	
	public String getRefundId() {
		return refundId;
	}
	public void setRefundId(String refundId) {
		this.refundId = refundId;
	}
	public String getGoodsMonkey() {
		return goodsMonkey;
	}
	public void setGoodsMonkey(String goodsMonkey) {
		this.goodsMonkey = goodsMonkey;
	}
	public RefundDetails(){
	}
	public RefundDetails(String id, String refundId, String goodsName, String goodsSpec, String goodsCount,
			String goodsMonkey) {
		super();
		this.id = id;
		this.refundId = refundId;
		this.goodsName = goodsName;
		this.goodsSpec = goodsSpec;
		this.goodsCount = goodsCount;
		this.goodsMonkey = goodsMonkey;
	}
	@Override
	public String toString() {
		return "RefundDetails [id=" + id + ", refundId=" + refundId + ", goodsName=" + goodsName + ", goodsSpec="
				+ goodsSpec + ", goodsCount=" + goodsCount + ", goodsMonkey=" + goodsMonkey + "]";
	}
	
}
