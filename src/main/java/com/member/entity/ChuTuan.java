package com.member.entity;
import java.util.Date;

import com.member.entity.base.BaseEntity;

public class ChuTuan implements BaseEntity<String>{
	
	private static final long serialVersionUID = 1434612846375230111L;
	private String id;
	private Date startTime;//起始时间
	private Date endTime;//结束时间
	private Integer dayNum;//天数
	private String loadStart;//路线起点
	private String loadEnd;//路线终点
	private Integer peopleNum;//1.人数
	private String managerName;//业务经理
	private String groupMonkeyStandard;//2.团费标准
	private String groupMonkey;//团费收入= 1*2
	private String remark;//备注
	private String eveningSiteCost;//晚会场地费
	private String eveningShowCost;//晚会表演费
	private String eveningShareCost;//晚会公摊费
	private String eveningWorkersCost;//晚会工作人员费
	private String siteCost;//会场费用
	private String sharedCost;//会场公摊费用
	private String siteWorkerCost;//会场工作人员费用
	private String siteAllCost;//会场费用小计
	private String insuranceStandard;//保险费标准
	private String insuranceCost;//保险费费用
	private String serviceCost;//服务费
	private String deliveryCost;//送货开支
	private String carCost;//送货车费
	private String dinghuoNum;//订货数量
	private String yshouhuokuanNum;//已收货款数量
	private String wshouhuokuanNum;//未收货款数量
	private String tuihuokuanNum;//退货款数量
	private String shoukuanNum;//收款合计
	private String sendNum;//发货数量
	private String remainingCost;//开支剩余
	private String earning;//盈亏情况
	private String lecturerCost;//讲师费用
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public Integer getDayNum() {
		return dayNum;
	}
	public void setDayNum(Integer dayNum) {
		this.dayNum = dayNum;
	}
	public String getLoadStart() {
		return loadStart;
	}
	public void setLoadStart(String loadStart) {
		this.loadStart = loadStart;
	}
	public String getLoadEnd() {
		return loadEnd;
	}
	public void setLoadEnd(String loadEnd) {
		this.loadEnd = loadEnd;
	}
	public Integer getPeopleNum() {
		return peopleNum;
	}
	public void setPeopleNum(Integer peopleNum) {
		this.peopleNum = peopleNum;
	}
	public String getManagerName() {
		return managerName;
	}
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
	public String getGroupMonkeyStandard() {
		return groupMonkeyStandard;
	}
	public void setGroupMonkeyStandard(String groupMonkeyStandard) {
		this.groupMonkeyStandard = groupMonkeyStandard;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getEveningSiteCost() {
		return eveningSiteCost;
	}
	public void setEveningSiteCost(String eveningSiteCost) {
		this.eveningSiteCost = eveningSiteCost;
	}
	public String getEveningShowCost() {
		return eveningShowCost;
	}
	public void setEveningShowCost(String eveningShowCost) {
		this.eveningShowCost = eveningShowCost;
	}
	public String getSiteCost() {
		return siteCost;
	}
	public void setSiteCost(String siteCost) {
		this.siteCost = siteCost;
	}
	public String getSharedCost() {
		return sharedCost;
	}
	public void setSharedCost(String sharedCost) {
		this.sharedCost = sharedCost;
	}
	public String getInsuranceStandard() {
		return insuranceStandard;
	}
	public void setInsuranceStandard(String insuranceStandard) {
		this.insuranceStandard = insuranceStandard;
	}
	public String getInsuranceCost() {
		return insuranceCost;
	}
	public void setInsuranceCost(String insuranceCost) {
		this.insuranceCost = insuranceCost;
	}
	public String getServiceCost() {
		return serviceCost;
	}
	public void setServiceCost(String serviceCost) {
		this.serviceCost = serviceCost;
	}
	public String getCarCost() {
		return carCost;
	}
	public void setCarCost(String carCost) {
		this.carCost = carCost;
	}
	public String getRemainingCost() {
		return remainingCost;
	}
	public void setRemainningCost(String remainingCost) {
		this.remainingCost = remainingCost;
	}
	

	public String getEveningShareCost() {
		return eveningShareCost;
	}
	public void setEveningShareCost(String eveningShareCost) {
		this.eveningShareCost = eveningShareCost;
	}
	
	public String getGroupMonkey() {
		return groupMonkey;
	}
	public void setGroupMonkey(String groupMonkey) {
		this.groupMonkey = groupMonkey;
	}
	public String getSiteWorkerCost() {
		return siteWorkerCost;
	}
	public void setSiteWorkerCost(String siteWorkerCost) {
		this.siteWorkerCost = siteWorkerCost;
	}
	public String getSiteAllCost() {
		return siteAllCost;
	}
	public void setSiteAllCost(String siteAllCost) {
		this.siteAllCost = siteAllCost;
	}
	public String getDeliveryCost() {
		return deliveryCost;
	}
	public void setDeliveryCost(String deliveryCost) {
		this.deliveryCost = deliveryCost;
	}
	public String getDinghuoNum() {
		return dinghuoNum;
	}
	public void setDinghuoNum(String dinghuoNum) {
		this.dinghuoNum = dinghuoNum;
	}
	public String getYshouhuokuanNum() {
		return yshouhuokuanNum;
	}
	public void setYshouhuokuanNum(String yshouhuokuanNum) {
		this.yshouhuokuanNum = yshouhuokuanNum;
	}
	public String getWshouhuokuanNum() {
		return wshouhuokuanNum;
	}
	public void setWshouhuokuanNum(String wshouhuokuanNum) {
		this.wshouhuokuanNum = wshouhuokuanNum;
	}
	public String getTuihuokuanNum() {
		return tuihuokuanNum;
	}
	public void setTuihuokuanNum(String tuihuokuanNum) {
		this.tuihuokuanNum = tuihuokuanNum;
	}
	public String getShoukuanNum() {
		return shoukuanNum;
	}
	public void setShoukuanNum(String shoukuanNum) {
		this.shoukuanNum = shoukuanNum;
	}
	public String getSendNum() {
		return sendNum;
	}
	public void setSendNum(String sendNum) {
		this.sendNum = sendNum;
	}
	public String getEarning() {
		return earning;
	}
	public void setEarning(String earning) {
		this.earning = earning;
	}
	public void setRemainingCost(String remainingCost) {
		this.remainingCost = remainingCost;
	}
	
	public String getEveningWorkersCost() {
		return eveningWorkersCost;
	}
	public void setEveningWorkersCost(String eveningWorkersCost) {
		this.eveningWorkersCost = eveningWorkersCost;
	}
	
	public ChuTuan() {
	}
	@Override
	public String toString() {
		return "ChuTuan [id=" + id + ", startTime=" + startTime + ", endTime=" + endTime + ", dayNum=" + dayNum
				+ ", loadStart=" + loadStart + ", loadEnd=" + loadEnd + ", peopleNum=" + peopleNum + ", managerName="
				+ managerName + ", groupMonkeyStandard=" + groupMonkeyStandard + ", groupMonkey=" + groupMonkey
				+ ", remark=" + remark + ", eveningSiteCost=" + eveningSiteCost + ", eveningShowCost=" + eveningShowCost
				+ ", eveningShareCost=" + eveningShareCost + ", eveningWorkersCost=" + eveningWorkersCost
				+ ", siteCost=" + siteCost + ", sharedCost=" + sharedCost + ", siteWorkerCost=" + siteWorkerCost
				+ ", siteAllCost=" + siteAllCost + ", insuranceStandard=" + insuranceStandard + ", insuranceCost="
				+ insuranceCost + ", serviceCost=" + serviceCost + ", deliveryCost=" + deliveryCost + ", carCost="
				+ carCost + ", dinghuoNum=" + dinghuoNum + ", yshouhuokuanNum=" + yshouhuokuanNum + ", wshouhuokuanNum="
				+ wshouhuokuanNum + ", tuihuokuanNum=" + tuihuokuanNum + ", shoukuanNum=" + shoukuanNum + ", sendNum="
				+ sendNum + ", remainingCost=" + remainingCost + ", earning=" + earning + "]";
	}
	public String getLecturerCost() {
		return lecturerCost;
	}
	public void setLecturerCost(String lecturerCost) {
		this.lecturerCost = lecturerCost;
	}
	
}
