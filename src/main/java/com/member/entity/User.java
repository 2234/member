package com.member.entity;

import java.util.Date;

import com.member.entity.base.BaseEntity;

public class User implements BaseEntity<String> {
	private static final long serialVersionUID = 1L;
	
	
	//主键
	private String id;
	//姓名
	private String name;
	//身份证号
	private String idNum;
	//业务经理
	private String businessManager;
	//地区
	private String address;
	//电话
	private String phone;
	//会员卡号
	private String cardNum;
	//建卡时间
	private String createCardTime;
	//积分
	private String integration;
	//等级
	private String level;
	
	//创建时间
	private Date createTime;
	//更新时间
	private Date updateTime;
	//状态
	private Integer state;
	//组织者
	private String organizerName;
	public User() {
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdNum() {
		return idNum;
	}

	public void setIdNum(String idNum) {
		this.idNum = idNum;
	}

	public String getBusinessManager() {
		return businessManager;
	}

	public void setBusinessManager(String businessManager) {
		this.businessManager = businessManager;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCardNum() {
		return cardNum;
	}

	public void setCardNum(String cardNum) {
		this.cardNum = cardNum;
	}

	public String getCreateCardTime() {
		return createCardTime;
	}

	public void setCreateCardTime(String createCardTime) {
		this.createCardTime = createCardTime;
	}

	public String getIntegration() {
		return integration;
	}

	public void setIntegration(String integration) {
		this.integration = integration;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}


	public String getOrganizerName() {
		return organizerName;
	}

	public void setOrganizerName(String organizerName) {
		this.organizerName = organizerName;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", idNum=" + idNum + ", businessManager=" + businessManager
				+ ", address=" + address + ", phone=" + phone + ", cardNum=" + cardNum + ", createCardTime="
				+ createCardTime + ", integration=" + integration + ", level=" + level + ", createTime=" + createTime
				+ ", updateTime=" + updateTime + ", state=" + state + ", organizerName=" + organizerName + "]";
	}
	


}
