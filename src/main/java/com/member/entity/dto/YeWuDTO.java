package com.member.entity.dto;

import java.util.List;

import com.member.entity.Shopping;
import com.member.entity.base.BaseEntity;
import com.member.entity.vo.ShoppingVo;

/**
 * 活动基础
 * @author jettylee
 *
 */
public class YeWuDTO implements BaseEntity<String>{
	private String id;//主键
	private String activityId;//活动ID
	private String position;//地点
	private String lecturer;//讲师
	private String workers;//场务
	List<ShoppingVo> shopList;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getActivityId() {
		return activityId;
	}
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	public String getLecturer() {
		return lecturer;
	}
	public void setLecturer(String lecturer) {
		this.lecturer = lecturer;
	}
	public String getWorkers() {
		return workers;
	}
	public void setWorkers(String workers) {
		this.workers = workers;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public List<ShoppingVo> getShopList() {
		return shopList;
	}
	public void setShopList(List<ShoppingVo> shopList) {
		this.shopList = shopList;
	}
	

	public YeWuDTO(String id, String activityId, String position, String lecturer, String workers, 
			List<ShoppingVo> shopList) {
		super();
		this.id = id;
		this.activityId = activityId;
		this.position = position;
		this.lecturer = lecturer;
		this.workers = workers;
		this.shopList = shopList;
	}
	public YeWuDTO() {
	}
	@Override
	public String toString() {
		return "YeWuDTO [id=" + id + ", activityId=" + activityId + ", position=" + position + ", lecturer=" + lecturer
				+ ", workers=" + workers + ", shopList=" + shopList + "]";
	}
	
	
}
