package com.member.entity;

import com.member.entity.base.BaseEntity;

/**
 * 活动基础
 * @author jettylee
 *
 */
public class CarActivity implements BaseEntity<Integer>{
	private static final long serialVersionUID = 1L;
	private Integer id;//主键
	private String activityId;//活动ID
	private String carModel;
	private Integer carSite;
	private Integer carNum;
	private String carStandard;
	private String carCost;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getActivityId() {
		return activityId;
	}
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	public String getCarModel() {
		return carModel;
	}
	public void setCarModel(String carModel) {
		this.carModel = carModel;
	}
	public Integer getCarSite() {
		return carSite;
	}
	public void setCarSite(Integer carSite) {
		this.carSite = carSite;
	}
	public Integer getCarNum() {
		return carNum;
	}
	public void setCarNum(Integer carNum) {
		this.carNum = carNum;
	}
	public String getCarStandard() {
		return carStandard;
	}
	public void setCarStandard(String carStandard) {
		this.carStandard = carStandard;
	}
	public String getCarCost() {
		return carCost;
	}
	public void setCarCost(String carCost) {
		this.carCost = carCost;
	}
	
	@Override
	public String toString() {
		return "CarActivity [id=" + id + ", activityId=" + activityId + ", carModel=" + carModel + ", carSite="
				+ carSite + ", carNum=" + carNum + ", carStandard=" + carStandard + ", carCost=" + carCost + "]";
	}
	
}
