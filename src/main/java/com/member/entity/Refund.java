package com.member.entity;
import java.util.Date;

import com.member.entity.base.BaseEntity;

public class Refund implements BaseEntity<String>{
	private String id;
	private String shoppingId;
	private String refundMonkey;
	private Date createTime;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getShoppingId() {
		return shoppingId;
	}
	public void setShoppingId(String shoppingId) {
		this.shoppingId = shoppingId;
	}
	public String getRefundMonkey() {
		return refundMonkey;
	}
	public void setRefundMonkey(String refundMonkey) {
		this.refundMonkey = refundMonkey;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Refund(){
	}
	
	public Refund(String id, String shoppingId, String refundMonkey, Date createTime) {
		this.id = id;
		this.shoppingId = shoppingId;
		this.refundMonkey = refundMonkey;
		this.createTime = createTime;
	}
	@Override
	public String toString() {
		return "Refund [id=" + id + ", shoppingId=" + shoppingId + ", refundMonkey=" + refundMonkey + "]";
	}
	
}
