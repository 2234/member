package com.member.entity;
import com.member.entity.base.BaseEntity;

public class Picking implements BaseEntity<String>{
	private String id;
	private String shoppingId;
	private String goodsName;
	private String goodsSpec;
	private String goodsCount;
	private String state;//1.已配货  2.未配货
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getShoppingId() {
		return shoppingId;
	}
	public void setShoppingId(String shoppingId) {
		this.shoppingId = shoppingId;
	}
	public String getGoodsName() {
		return goodsName;
	}
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	public String getGoodsSpec() {
		return goodsSpec;
	}
	public void setGoodsSpec(String goodsSpec) {
		this.goodsSpec = goodsSpec;
	}
	public String getGoodsCount() {
		return goodsCount;
	}
	public void setGoodsCount(String goodsCount) {
		this.goodsCount = goodsCount;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Picking(){
	}
	public Picking(String id, String shoppingId, String goodsName, String goodsSpec, String goodsCount, String state) {
		this.id = id;
		this.shoppingId = shoppingId;
		this.goodsName = goodsName;
		this.goodsSpec = goodsSpec;
		this.goodsCount = goodsCount;
		this.state = state;
	}
	@Override
	public String toString() {
		return "Picking [id=" + id + ", shoppingId=" + shoppingId + ", goodsName=" + goodsName + ", goodsSpec="
				+ goodsSpec + ", goodsCount=" + goodsCount + ", state=" + state + "]";
	}
	

	
	
}
