package com.member.entity.vo;
import java.io.Serializable;
import java.util.List;

import com.member.entity.CarActivity;
import com.member.entity.ChuTuan;
import com.member.entity.EatActivity;
import com.member.entity.GoodsActivity;
import com.member.entity.StayActivity;

public class ChuTuanVo implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private ChuTuan tuan;
	private List<EatActivity> eatList;
	private List<StayActivity> stayList;
	private List<CarActivity> carList;
	private List<GoodsActivity> lipinList;
	private List<GoodsActivity> taochanList;

	public List<EatActivity> getEatList() {
		return eatList;
	}
	public void setEatList(List<EatActivity> eatList) {
		this.eatList = eatList;
	}
	public List<StayActivity> getStayList() {
		return stayList;
	}
	public void setStayList(List<StayActivity> stayList) {
		this.stayList = stayList;
	}
	public List<CarActivity> getCarList() {
		return carList;
	}
	public void setCarList(List<CarActivity> carList) {
		this.carList = carList;
	}
	public List<GoodsActivity> getLipinList() {
		return lipinList;
	}
	public void setLipinList(List<GoodsActivity> lipinList) {
		this.lipinList = lipinList;
	}
	public List<GoodsActivity> getTaochanList() {
		return taochanList;
	}
	public void setTaochanList(List<GoodsActivity> taochanList) {
		this.taochanList = taochanList;
	}

	public ChuTuan getTuan() {
		return tuan;
	}
	public void setTuan(ChuTuan tuan) {
		this.tuan = tuan;
	}
	public ChuTuanVo() {
	}
	@Override
	public String toString() {
		return "ChuTuanVo [tuan=" + tuan + ", eatList=" + eatList + ", stayList=" + stayList + ", carList=" + carList
				+ ", lipinList=" + lipinList + ", taochanList=" + taochanList + "]";
	}
	
}
