package com.member.entity.vo;

import java.io.Serializable;
import java.util.List;

import com.member.entity.Shopping;
import com.member.entity.YeWu;
import com.member.entity.base.BaseEntity;

/**
 * 活动基础
 * @author jettylee
 *
 */
public class YeWuVo implements Serializable{
	private YeWu yewu;
	private List<ShoppingVo> shopList;
	public YeWuVo() {
	}
	public YeWu getYewu() {
		return yewu;
	}
	public void setYewu(YeWu yewu) {
		this.yewu = yewu;
	}
	public List<ShoppingVo> getShopList() {
		return shopList;
	}
	public void setShopList(List<ShoppingVo> shopList) {
		this.shopList = shopList;
	}
	@Override
	public String toString() {
		return "YeWuVo [yewu=" + yewu + ", shopList=" + shopList + "]";
	}
	
}
