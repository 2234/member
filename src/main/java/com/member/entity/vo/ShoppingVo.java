package com.member.entity.vo;

import java.util.List;

import com.member.entity.Picking;
import com.member.entity.base.BaseEntity;

/**
 * 购买信息
 * @author jettylee
 *
 */
public class ShoppingVo implements BaseEntity<String>{
	private String id;//主键
	private String yewuId;//活动中业务活动的ID
	private String userName;//名
	private String userPhone;//手机号
	private String managerName;//业务经理
	private String address;//地址
	private String deposit;//定金
	private String advance;//预付款
	private String weikuan;//尾款
	private String paid;//已付
	private String payType;//支付类别
	private Integer state;//0正常 1退货
	private List<Picking> pickingList;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	public String getManagerName() {
		return managerName;
	}
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getDeposit() {
		return deposit;
	}
	public void setDeposit(String deposit) {
		this.deposit = deposit;
	}
	public String getAdvance() {
		return advance;
	}
	public void setAdvance(String advance) {
		this.advance = advance;
	}
	public String getPaid() {
		return paid;
	}
	public void setPaid(String paid) {
		this.paid = paid;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	
	public String getYewuId() {
		return yewuId;
	}
	public void setYewuId(String yewuId) {
		this.yewuId = yewuId;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}

	public String getWeikuan() {
		return weikuan;
	}
	public void setWeikuan(String weikuan) {
		this.weikuan = weikuan;
	}
	
	public List<Picking> getPickingList() {
		return pickingList;
	}
	public void setPickingList(List<Picking> pickingList) {
		this.pickingList = pickingList;
	}
	public ShoppingVo() {
	}
	public ShoppingVo(String id, String yewuId, String userName, String userPhone, String managerName, String address,
			String deposit, String advance, String weikuan, String paid, String payType, Integer state,
			List<Picking> pickingList) {
		this.id = id;
		this.yewuId = yewuId;
		this.userName = userName;
		this.userPhone = userPhone;
		this.managerName = managerName;
		this.address = address;
		this.deposit = deposit;
		this.advance = advance;
		this.weikuan = weikuan;
		this.paid = paid;
		this.payType = payType;
		this.state = state;
		this.pickingList = pickingList;
	}
	@Override
	public String toString() {
		return "Shopping [id=" + id + ", yewuId=" + yewuId + ", userName=" + userName + ", userPhone=" + userPhone
				+ ", managerName=" + managerName + ", address=" + address + ", deposit=" + deposit + ", advance="
				+ advance + ", weikuan=" + weikuan + ", paid=" + paid + ", payType=" + payType + ", state=" + state
				+ ", pickingList=" + pickingList + "]";
	}
	
}
