package com.member.entity.vo;

import java.io.Serializable;
import java.util.List;

public class GoodsDTO implements Serializable{

	private static final long serialVersionUID = -6857915194407638742L;
	private String name;
	private String kind;
	private List<SpecVo> list;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getKind() {
		return kind;
	}
	public void setKind(String kind) {
		this.kind = kind;
	}
	public List<SpecVo> getList() {
		return list;
	}
	public void setList(List<SpecVo> list) {
		this.list = list;
	}
	public GoodsDTO(String name, String kind, List<SpecVo> list) {
		super();
		this.name = name;
		this.kind = kind;
		this.list = list;
	}
	public GoodsDTO() {
	}
	@Override
	public String toString() {
		return "GoodsDTO [name=" + name + ", kind=" + kind + ", list=" + list + "]";
	}
	
	
}
