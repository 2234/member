package com.member.entity.vo;

import java.io.Serializable;
import java.util.List;

import com.member.entity.Picking;
import com.member.entity.base.BaseEntity;

/**
 * 购买信息
 * @author jettylee
 *
 */
public class UserShoppingVo implements Serializable{
	private String activityId;//活动编号
	private String yewuId;//业务编号
	private String shoppingId;//购物编号
	private String userName;//名
	private String userPhone;//手机号
	private String managerName;//业务经理
	private String address;//地址
	private String deposit;//定金
	private String advance;//预付款
	private String weikuan;//尾款
	private String paid;//已付
	private String payType;//支付类别
	private Integer state;//0正常 1退货
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	public String getManagerName() {
		return managerName;
	}
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getDeposit() {
		return deposit;
	}
	public void setDeposit(String deposit) {
		this.deposit = deposit;
	}
	public String getAdvance() {
		return advance;
	}
	public void setAdvance(String advance) {
		this.advance = advance;
	}
	public String getPaid() {
		return paid;
	}
	public void setPaid(String paid) {
		this.paid = paid;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	
	public String getYewuId() {
		return yewuId;
	}
	public void setYewuId(String yewuId) {
		this.yewuId = yewuId;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}

	public String getWeikuan() {
		return weikuan;
	}
	public void setWeikuan(String weikuan) {
		this.weikuan = weikuan;
	}
	
	public String getActivityId() {
		return activityId;
	}
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	
	public String getShoppingId() {
		return shoppingId;
	}
	public void setShoppingId(String shoppingId) {
		this.shoppingId = shoppingId;
	}
	public UserShoppingVo() {
	}
	@Override
	public String toString() {
		return "UserShoppingVo [activityId=" + activityId + ", yewuId=" + yewuId + ", userName=" + userName
				+ ", userPhone=" + userPhone + ", managerName=" + managerName + ", address=" + address + ", deposit="
				+ deposit + ", advance=" + advance + ", weikuan=" + weikuan + ", paid=" + paid + ", payType=" + payType
				+ ", state=" + state + "]";
	}
	
}
