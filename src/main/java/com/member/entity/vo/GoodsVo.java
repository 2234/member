package com.member.entity.vo;

import java.io.Serializable;

public class GoodsVo implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;
	private String good_id;
	private String name;
	private String kind;
	private String spec;
	private String cost_price;
	private String retail_price;
	private String total_inventory;
	private String existing_inventory;
	private String transfer_cargo;
	private String out_goods_num;
	public GoodsVo() {
		// TODO Auto-generated constructor stub
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGood_id() {
		return good_id;
	}
	public void setGood_id(String good_id) {
		this.good_id = good_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getKind() {
		return kind;
	}
	public void setKind(String kind) {
		this.kind = kind;
	}
	public String getSpec() {
		return spec;
	}
	public void setSpec(String spec) {
		this.spec = spec;
	}
	public String getCost_price() {
		return cost_price;
	}
	public void setCost_price(String cost_price) {
		this.cost_price = cost_price;
	}
	public String getRetail_price() {
		return retail_price;
	}
	public void setRetail_price(String retail_price) {
		this.retail_price = retail_price;
	}
	public String getTotal_inventory() {
		return total_inventory;
	}
	public void setTotal_inventory(String total_inventory) {
		this.total_inventory = total_inventory;
	}
	public String getExisting_inventory() {
		return existing_inventory;
	}
	public void setExisting_inventory(String existing_inventory) {
		this.existing_inventory = existing_inventory;
	}
	public String getTransfer_cargo() {
		return transfer_cargo;
	}
	public void setTransfer_cargo(String transfer_cargo) {
		this.transfer_cargo = transfer_cargo;
	}
	public String getOut_goods_num() {
		return out_goods_num;
	}
	public void setOut_goods_num(String out_goods_num) {
		this.out_goods_num = out_goods_num;
	}
	@Override
	public String toString() {
		return "GoodsVo [id=" + id + ", good_id=" + good_id + ", name=" + name + ", kind=" + kind + ", spec=" + spec
				+ ", cost_price=" + cost_price + ", retail_price=" + retail_price + ", total_inventory="
				+ total_inventory + ", existing_inventory=" + existing_inventory + ", transfer_cargo=" + transfer_cargo
				+ ", out_goods_num=" + out_goods_num + "]";
	}
	
}
