package com.member.entity.vo;

import java.io.Serializable;

public class SpecVo implements Serializable {
	private static final long serialVersionUID = 1L;
	// 规格
	private String spec;
	// 成本价
	private String cost_price;
	// 零售价
	private String retail_price;
	// 总库存
	private String total_inventory;
	public String getSpec() {
		return spec;
	}
	public void setSpec(String spec) {
		this.spec = spec;
	}
	public String getCost_price() {
		return cost_price;
	}
	public void setCost_price(String cost_price) {
		this.cost_price = cost_price;
	}
	public String getRetail_price() {
		return retail_price;
	}
	public void setRetail_price(String retail_price) {
		this.retail_price = retail_price;
	}
	public String getTotal_inventory() {
		return total_inventory;
	}
	public void setTotal_inventory(String total_inventory) {
		this.total_inventory = total_inventory;
	}
	public SpecVo(String spec, String cost_price, String retail_price, String total_inventory) {
		super();
		this.spec = spec;
		this.cost_price = cost_price;
		this.retail_price = retail_price;
		this.total_inventory = total_inventory;
	}
	public SpecVo() {
	}
	@Override
	public String toString() {
		return "SpecVo [spec=" + spec + ", cost_price=" + cost_price + ", retail_price=" + retail_price
				+ ", total_inventory=" + total_inventory + "]";
	}
	
}
