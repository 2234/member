package com.member.entity.vo;

import java.io.Serializable;

public class UserVo implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String name;
	private String phone;
	
	public UserVo() {
		// TODO Auto-generated constructor stub
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	@Override
	public String toString() {
		return "UserVo [id=" + id + ", name=" + name + ", phone=" + phone + "]";
	}
	

}
