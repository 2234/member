package com.member.entity;

import java.util.Date;

import com.member.entity.base.BaseEntity;

public class Employee implements BaseEntity<String> {
	private static final long serialVersionUID = 1L;

	// id
	public String id;
	// 姓名
	public String name;
	// 年龄
	public String age;
	// 生日
	public String birthday;
	// 住址
	public String address;
	// 入职时间
	public String entryTime;
	// 试用期
	public String probation;
	// 备注
	public String remarks;

	// 创建时间
	private Date createTime;
	// 更新时间
	private Date updateTime;
	// 状态
	private Integer state;

	public Employee() {
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEntryTime() {
		return entryTime;
	}

	public void setEntryTime(String entryTime) {
		this.entryTime = entryTime;
	}

	public String getProbation() {
		return probation;
	}

	public void setProbation(String probation) {
		this.probation = probation;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", age=" + age + ", birthday=" + birthday + ", address="
				+ address + ", entryTime=" + entryTime + ", probation=" + probation + ", remarks=" + remarks
				+ ", createTime=" + createTime + ", updateTime=" + updateTime + ", state=" + state + "]";
	}



}
