package com.member.entity;

import java.util.Date;

import com.member.entity.base.BaseEntity;

public class Goods implements BaseEntity<String> {
	private static final long serialVersionUID = 1L;

	// 主键
	private String id;
	// 姓名
	private String name;
	// 种类
	private String kind;
	// 规格
	private String spec;
	// 成本价
	private String cost_price;
	// 零售价
	private String retail_price;
	// 总库存
	private String total_inventory;
	// 现有库存
	private String existing_inventory;
	// 调货数量
	private String transfer_cargo;
	// 出库数量
	private String out_goods_num;
	// 创建时间
	private Date createTime;
	// 更新时间
	private Date updateTime;
	// 状态
	private Integer state;

	public Goods() {
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getSpec() {
		return spec;
	}

	public void setSpec(String spec) {
		this.spec = spec;
	}

	public String getCost_price() {
		return cost_price;
	}

	public void setCost_price(String cost_price) {
		this.cost_price = cost_price;
	}

	public String getRetail_price() {
		return retail_price;
	}

	public void setRetail_price(String retail_price) {
		this.retail_price = retail_price;
	}

	public String getTotal_inventory() {
		return total_inventory;
	}

	public void setTotal_inventory(String total_inventory) {
		this.total_inventory = total_inventory;
	}

	public String getExisting_inventory() {
		return existing_inventory;
	}

	public void setExisting_inventory(String existing_inventory) {
		this.existing_inventory = existing_inventory;
	}

	public String getTransfer_cargo() {
		return transfer_cargo;
	}

	public void setTransfer_cargo(String transfer_cargo) {
		this.transfer_cargo = transfer_cargo;
	}

	public String getOut_goods_num() {
		return out_goods_num;
	}

	public void setOut_goods_num(String out_goods_num) {
		this.out_goods_num = out_goods_num;
	}

	@Override
	public String toString() {
		return "Goods [id=" + id + ", name=" + name + ", kind=" + kind + ", createTime=" + createTime + ", updateTime="
				+ updateTime + ", state=" + state + "]";
	}

}
