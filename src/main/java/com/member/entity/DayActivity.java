package com.member.entity;

import com.member.entity.base.BaseEntity;

/**
 * 活动基础
 * @author jettylee
 *
 */
public class DayActivity implements BaseEntity<Integer>{
	private Integer id;//主键
	private String activityId;//活动ID
	private Integer dayNum;//第几天
	private String lecturer;//讲师
	private String workers;//场务
	private String workersCost;//场务费
	private String shared;
	private String sharedCost;//公摊费
	private String insuranceStandard;//保险标准
	private String insuranceCost;//保险费
	private String serviceCost;//服务费
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getActivityId() {
		return activityId;
	}
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	public Integer getDayNum() {
		return dayNum;
	}
	public void setDayNum(Integer dayNum) {
		this.dayNum = dayNum;
	}
	public String getLecturer() {
		return lecturer;
	}
	public void setLecturer(String lecturer) {
		this.lecturer = lecturer;
	}
	public String getWorkers() {
		return workers;
	}
	public void setWorkers(String workers) {
		this.workers = workers;
	}
	public String getWorkersCost() {
		return workersCost;
	}
	public void setWorkersCost(String workersCost) {
		this.workersCost = workersCost;
	}
	public String getShared() {
		return shared;
	}
	public void setShared(String shared) {
		this.shared = shared;
	}
	public String getSharedCost() {
		return sharedCost;
	}
	public void setSharedCost(String sharedCost) {
		this.sharedCost = sharedCost;
	}
	public String getInsuranceStandard() {
		return insuranceStandard;
	}
	public void setInsuranceStandard(String insuranceStandard) {
		this.insuranceStandard = insuranceStandard;
	}
	public String getInsuranceCost() {
		return insuranceCost;
	}
	public void setInsuranceCost(String insuranceCost) {
		this.insuranceCost = insuranceCost;
	}
	public String getServiceCost() {
		return serviceCost;
	}
	public void setServiceCost(String serviceCost) {
		this.serviceCost = serviceCost;
	}
	
}
