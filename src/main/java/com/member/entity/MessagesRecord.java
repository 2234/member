package com.member.entity;

import com.member.entity.base.BaseEntity;

public class MessagesRecord implements BaseEntity<Integer> {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String messageId;
	private String name;
	private String phone;
	private Integer state;//1 成功  2.失败
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public MessagesRecord() {
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}


	public MessagesRecord(Integer id, String messageId, String name, String phone, Integer state) {
		super();
		this.id = id;
		this.messageId = messageId;
		this.name = name;
		this.phone = phone;
		this.state = state;
	}

}
