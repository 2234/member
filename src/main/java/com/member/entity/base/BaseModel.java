package com.member.entity.base;

public class BaseModel {
	
	public Boolean Successful=false;
	
	public String Message;

	@Override
	public String toString() {
		return "BaseModel [Successful=" + Successful + ", Message=" + Message + "]";
	}
	

}
