package com.member.entity.base;

public class BaseModelJson<T> extends BaseModel {
	
	public T Data;

	@Override
	public String toString() {
		return "BaseModelJson [Data=" + Data + "]";
	}
	
	
}
