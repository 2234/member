package com.member.entity;

import java.util.Date;

import com.member.entity.base.BaseEntity;

public class BusinessManager implements BaseEntity<String> {
	private static final long serialVersionUID = 1L;

	// 主键
	private String id;
	// 姓名
	private String name;
	// 电话
	private String phone;
	// 创建时间
	private Date createTime;
	// 更新时间
	private Date updateTime;
	// 状态 0正常  1删除
	private Integer state;

	public BusinessManager() {
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "BusinessManager [id=" + id + ", name=" + name + ", phone=" + phone + ", createTime=" + createTime
				+ ", updateTime=" + updateTime + ", state=" + state + "]";
	}

	

}
