package com.member.entity;

import java.util.Date;

import com.member.entity.base.BaseEntity;

/**
 * 活动基础
 * @author jettylee
 *
 */
public class YeWu implements BaseEntity<String>{
	private String id;//主键
	private String activityId;//活动ID
	private String position;//地点
	private String lecturer;//讲师
	private String workers;//场务

	// 创建时间
	private Date createTime;
	// 更新时间
	private Date updateTime;
	// 状态 0正常  1删除
	private Integer state;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getActivityId() {
		return activityId;
	}
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	public String getLecturer() {
		return lecturer;
	}
	public void setLecturer(String lecturer) {
		this.lecturer = lecturer;
	}
	public String getWorkers() {
		return workers;
	}
	public void setWorkers(String workers) {
		this.workers = workers;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	
	public YeWu() {
	}
	public YeWu(String id, String activityId, String position, String lecturer, String workers, 
			Date createTime, Date updateTime, Integer state) {
		super();
		this.id = id;
		this.activityId = activityId;
		this.position = position;
		this.lecturer = lecturer;
		this.workers = workers;
		this.createTime = createTime;
		this.updateTime = updateTime;
		this.state = state;
	}
	@Override
	public String toString() {
		return "YeWu [id=" + id + ", activityId=" + activityId + ", position=" + position + ", lecturer=" + lecturer
				+ ", workers=" + workers + ", createTime=" + createTime
				+ ", updateTime=" + updateTime + ", state=" + state + "]";
	}
	
}
