package com.member.entity;

import java.util.Date;

import com.member.entity.base.BaseEntity;

public class Messages implements BaseEntity<String> {
	private static final long serialVersionUID = 1L;

	private String id;
	private String title;
	private String content;
	private Integer size;
	private Date createTime;
	private Date updateTime;
	private Integer state;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Integer getSize() {
		return size;
	}
	public void setSize(Integer size) {
		this.size = size;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Messages() {
	}
	public Messages(String id, String title, String content, Integer size, Date createTime, Date updateTime,
			Integer state) {
		super();
		this.id = id;
		this.title = title;
		this.content = content;
		this.size = size;
		this.createTime = createTime;
		this.updateTime = updateTime;
		this.state = state;
	}
	@Override
	public String toString() {
		return "Messages [id=" + id + ", title=" + title + ", content=" + content + ", size=" + size + ", createTime="
				+ createTime + ", updateTime=" + updateTime + ", state=" + state + "]";
	}
	

}
