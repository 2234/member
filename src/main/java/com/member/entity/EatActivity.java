package com.member.entity;

import com.member.entity.base.BaseEntity;

/**
 * 活动基础
 * @author jettylee
 *
 */
public class EatActivity implements BaseEntity<Integer>{
	private Integer id;//主键
	private String activityId;//活动ID
	private Integer dayNum;//第几天
	private String eatPosition;
	private String eatStandard;
	private String eatCost;
	private String eatRemark;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getActivityId() {
		return activityId;
	}
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	public Integer getDayNum() {
		return dayNum;
	}
	public void setDayNum(Integer dayNum) {
		this.dayNum = dayNum;
	}
	public String getEatPosition() {
		return eatPosition;
	}
	public void setEatPosition(String eatPosition) {
		this.eatPosition = eatPosition;
	}
	public String getEatStandard() {
		return eatStandard;
	}
	public void setEatStandard(String eatStandard) {
		this.eatStandard = eatStandard;
	}
	public String getEatCost() {
		return eatCost;
	}
	public void setEatCost(String eatCost) {
		this.eatCost = eatCost;
	}
	
	public String getEatRemark() {
		return eatRemark;
	}
	public void setEatRemark(String eatRemark) {
		this.eatRemark = eatRemark;
	}
	@Override
	public String toString() {
		return "EatActivity [id=" + id + ", activityId=" + activityId + ", dayNum=" + dayNum + ", eatPosition="
				+ eatPosition + ", eatStandard=" + eatStandard + ", eatCost=" + eatCost + ", eatRemark=" + eatRemark
				+ "]";
	}
	
}
