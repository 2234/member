package com.member.entity;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.member.entity.base.BaseEntity;

/**
 * 活动基础
 * @author jettylee
 *
 */
public class Admin implements BaseEntity<String>{
	private String id;//主键
	private String account;
	private String password;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void Admin(){
		
	}
	
}
