package com.member.entity;

import com.member.entity.base.BaseEntity;

/**
 * 活动基础
 * @author jettylee
 *
 */
public class GoodsActivity implements BaseEntity<String>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String id;//主键
	private String activityId;//活动ID
	private String goodsName;
	private String goodsSpec;
	private Integer goodsCount;
	private double goodsPrice;
	private String goodsCost;
	private Integer type;//1.礼品 2.套餐
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getActivityId() {
		return activityId;
	}
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	public String getGoodsName() {
		return goodsName;
	}
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	public String getGoodsSpec() {
		return goodsSpec;
	}
	public void setGoodsSpec(String goodsSpec) {
		this.goodsSpec = goodsSpec;
	}
	public Integer getGoodsCount() {
		return goodsCount;
	}
	public void setGoodsCount(Integer goodsCount) {
		this.goodsCount = goodsCount;
	}
	public double getGoodsPrice() {
		return goodsPrice;
	}
	public void setGoodsPrice(double goodsPrice) {
		this.goodsPrice = goodsPrice;
	}
	public String getGoodsCost() {
		return goodsCost;
	}
	public void setGoodsCost(String goodsCost) {
		this.goodsCost = goodsCost;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	
	@Override
	public String toString() {
		return "GoodsActivity [id=" + id + ", activityId=" + activityId + ", goodsName=" + goodsName + ", goodsSpec="
				+ goodsSpec + ", goodsCount=" + goodsCount + ", goodsPrice=" + goodsPrice + ", goodsCost=" + goodsCost
				+ ", type=" + type + "]";
	}
	
}
