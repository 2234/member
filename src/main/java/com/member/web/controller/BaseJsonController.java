package com.member.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.member.entity.base.BaseModelJson;
import com.member.exception.BusinessException;
import com.member.exception.ParameterException;

public class BaseJsonController {
	
	/** 基于@ExceptionHandler异常处理 */
	@ExceptionHandler(value = { BusinessException.class, ParameterException.class, Exception.class})
	@ResponseBody
	public  BaseModelJson<String> exp(HttpServletRequest request, Exception ex) {
		BaseModelJson<String> model=new BaseModelJson<String>();
		model.Successful=false;
		model.Data=ex.toString();
		// 根据不同错误转向不同页面
		if(ex instanceof BusinessException) {
			model.Message="业务异常";
		}else if(ex instanceof ParameterException) {
			model.Message="参数异常";
		} else {
			model.Message="异常";
		}
		 String sOut = "";
	        StackTraceElement[] trace = ex.getStackTrace();
	        for (StackTraceElement s : trace) {
	            sOut += "\tat " + s + "\r\n";
	        }
	        System.out.println(ex.toString());
	        System.out.println(sOut);
		return model;
	}
}