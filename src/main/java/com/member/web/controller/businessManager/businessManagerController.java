package com.member.web.controller.businessManager;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.member.entity.BusinessManager;
import com.member.service.ManagerService;
import com.member.web.controller.BaseController;

@Controller
@RequestMapping("/manager")
public class businessManagerController extends BaseController {
	@Autowired
	ManagerService managerService;

	// 跳转添加业务经理页面
	@RequestMapping("/tiao")
	public String tiao() {
		return "manager/insert_manager";
	}

	// 添加会员
	@RequestMapping("/insertManager")
	public String insertManager(String name, String phone,Model model) {
		if (StringUtils.isNotEmpty(name) && StringUtils.isNotEmpty(phone)) {
			managerService.insertManager(name, phone);
			return "redirect:managerList";
		} else {
			model.addAttribute("ERROR", "参数有误");
			return "manager/insert_manager";
		}
	}

	// 查找所有会员
	@RequestMapping("/managerList")
	public String managerList(Model model, Integer pageNum, Integer pageSize) {
		if (pageNum == null)
			pageNum = 1;
		if (pageSize == null)
			pageSize = 50;
		PageInfo<BusinessManager> managerList = managerService.selectAllManager(pageNum, pageSize);
		model.addAttribute("page", managerList);
		return "manager/manager_list";
	}


	// 删除会员
	@RequestMapping("/deleteManager")
	public String deleteManager(String id, Model model) {
		managerService.deleteManager(id);
		return "redirect:managerList";
	}

	// 模态框回显
	@ResponseBody
	@RequestMapping("/selectManager")
	public BusinessManager selectManager(String id) {
		BusinessManager manager = managerService.selectManager(id);
		return manager;
	}

	// 修改会员信息
	@RequestMapping("/updateManager")
	public String updateManager(String id, String name, String phone) {
		managerService.updateManager(id, name, phone);
		return "redirect:managerList";
	}

}
