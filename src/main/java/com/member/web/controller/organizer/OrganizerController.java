package com.member.web.controller.organizer;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.member.entity.Organizer;
import com.member.service.OrganizerService;
import com.member.web.controller.BaseController;

@Controller
@RequestMapping("/organizer")
public class OrganizerController extends BaseController {
	@Autowired
	OrganizerService organizerService;

	// 跳转添加业务经理页面
	@RequestMapping("/tiao")
	public String tiao() {
		return "organizer/insert_organizer";
	}

	// 添加会员
	@RequestMapping("/insertOrganizer")
	public String insertOrganizer(String name, String phone, Model model) {
		if (StringUtils.isNotEmpty(name) && StringUtils.isNotEmpty(phone)) {
			organizerService.insertOrganizer(name, phone);
			return "redirect:organizerList";
		} else {
			model.addAttribute("ERROR", "参数有误");
			return "organizer/insert_organizer";
		}
	}

	// 查找所有会员
	@RequestMapping("/organizerList")
	public String organizerList(Model model, Integer pageNum, Integer pageSize) {
		if (pageNum == null)
			pageNum = 1;
		if (pageSize == null)
			pageSize = 50;
		PageInfo<Organizer> organizerList = organizerService.selectAllOrganizer(pageNum, pageSize);
		model.addAttribute("page", organizerList);
		return "organizer/organizer_list";
	}

	// 删除会员
	@RequestMapping("/deleteOrganizer")
	public String deleteOrganizer(String id, Model model) {
		organizerService.deleteOrganizer(id);
		return "redirect:organizerList";
	}

	// 模态框回显
	@ResponseBody
	@RequestMapping("/selectOrganizer")
	public Organizer selectOrganizer(String id) {
		Organizer organizer = organizerService.selectOrganizer(id);
		return organizer;
	}

	// 修改会员信息
	@RequestMapping("/updateOrganizer")
	public String updateOrganizer(String id, String name, String phone) {
		organizerService.updateOrganizer(id, name, phone);
		return "redirect:organizerList";
	}

}
