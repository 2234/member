package com.member.web.controller.identity;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.member.entity.Admin;
import com.member.service.AdminService;
import com.member.web.controller.BaseController;


@Controller
@RequestMapping("/identity")
public class IdentityController extends BaseController {
	@Autowired
	AdminService service;
	
	@RequestMapping("/login")
	public String login(Model model, HttpSession session, String account, String password) {
				if (StringUtils.isNotBlank(account) && StringUtils.isNotBlank(password)) {
					Admin result = service.login(account, password);
					if (null != result) {
						session.setAttribute("ADMIN", result);
							return "redirect:/user/tiao";
					} else {
						model.addAttribute("msg", "账号或密码错误，请重试！");
						return "login";
					}
				
				} else {
					model.addAttribute("msg", "账号或密码错误，请重试！");
					return "login";
				}

			}


	@RequestMapping("/logout")
	public String logout(HttpSession session) {
		session.invalidate();
		return "login";

	}
	@RequestMapping("/loginView")
	public String view() {
		return "login";
		
	}
}
