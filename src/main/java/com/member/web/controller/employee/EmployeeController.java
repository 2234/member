package com.member.web.controller.employee;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.member.common.contants.Constants;
import com.member.entity.Employee;
import com.member.service.EmployeeService;
import com.member.web.controller.BaseController;

@Controller
@RequestMapping("/employee")
public class EmployeeController extends BaseController {
	@Autowired
	EmployeeService employeeService;

	// 跳转添加员工页面
	@RequestMapping("/tiao")
	public String tiao() {
		return "employee/insert_employee";
	}

	// 添加员工
	@RequestMapping("/insertEmployee")
	public String insertEmployee(String name, String age, String birthday, String address, String entryTime,
			String probation, String remarks, Model model) {
		if (StringUtils.isNotEmpty(name) && StringUtils.isNotEmpty(age) && StringUtils.isNotEmpty(birthday)
				&& StringUtils.isNotEmpty(address) && StringUtils.isNotEmpty(entryTime)
				&& StringUtils.isNotEmpty(probation) && StringUtils.isNotEmpty(remarks)) {
			employeeService.insertEmployee(name, age, birthday, address, entryTime, probation, remarks);
			return "redirect:employeeList";
		} else {
			model.addAttribute("ERROR", "参数有误");
			return "employee/insert_employee";
		}
	}

	// 查找所有员工
	@RequestMapping("/employeeList")
	public String employeeList(Model model, Integer pageNum, Integer pageSize) {
		if (pageNum == null)
			pageNum = 1;
		if (pageSize == null)
			pageSize = Constants.PAGE_SIZE;
		PageInfo<Employee> employeeList = employeeService.selectAllEmployee(pageNum, pageSize);
		model.addAttribute("page", employeeList);
		return "employee/employee_list";
	}

	// 删除员工
	@RequestMapping("/deleteEmployee")
	public String deleteEmployee(String id, Model model) {
		employeeService.deleteEmployee(id);
		return "redirect:employeeList";
	}

	// 模态框回显
	@ResponseBody
	@RequestMapping("/selectEmployee")
	public Employee selectEmployee(String id) {
		Employee employee = employeeService.selectEmployee(id);
		return employee;
	}

	// 修改员工信息
	@RequestMapping("/updateEmployee")
	public String updateEmployee(String id,String name, String age, String birthday, String address, String entryTime,
			String probation, String remarks) {
		employeeService.updateEmployee(id,name, age, birthday, address, entryTime, probation, remarks);
		return "redirect:employeeList";
	}
	
	@RequestMapping("/findByName")
	public String findByEmployeeName(Model model, String name,Integer pageNum, Integer pageSize) {
		if (pageNum == null)
			pageNum = 1;
		if (pageSize == null)
			pageSize = Constants.PAGE_SIZE;
		PageInfo<Employee> employee = employeeService.findAllByFilter(name,pageNum, pageSize);
		model.addAttribute("page", employee);
		
		
		List<String> params = new ArrayList<String>();
		 if(name!=null && name.length()!=0  ){
			 String param1 = "name="+name+"&";
			 params.add(param1);
		 }
		model.addAttribute("params", params);
		
		
		return "employee/employee_list";
	}

}
