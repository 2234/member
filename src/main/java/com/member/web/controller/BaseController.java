package com.member.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ExceptionHandler;

import com.member.exception.BusinessException;
import com.member.exception.ParameterException;


public class BaseController {
	
	/** 基于@ExceptionHandler异常处理 */
	@ExceptionHandler(value = { BusinessException.class, ParameterException.class, Exception.class})
	public String exp(HttpServletRequest request, Exception ex) {
		
		request.setAttribute("ex", ex);
		
		// 根据不同错误转向不同页面
		if(ex instanceof BusinessException) {
			return "error/error-business";
		}else if(ex instanceof ParameterException) {
			return "error/error-parameter";
		} else {
			return "error/error";
		}
	}
}