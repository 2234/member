package com.member.web.controller.message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.member.entity.base.BaseModel;
import com.member.service.ManagerService;
import com.member.service.MessageService;
import com.member.service.OrganizerService;
import com.member.service.UserService;
import com.member.web.controller.BaseController;

@Controller
@RequestMapping("/message")
public class MessageController extends BaseController {
	@Autowired
	MessageService messageService;
	@Autowired
	OrganizerService organizerService;
	@Autowired
	UserService userService;
	@Autowired
	ManagerService managerService;

	// 跳转添加会员页面
	@RequestMapping("/tiao")
	public String tiao(Model model) {
		model.addAttribute("managerList", managerService.findAll());
		model.addAttribute("addressList", userService.findAllAddress());
		model.addAttribute("organizerList", organizerService.findAll());
		return "messages/msgView";
	}

	@RequestMapping("/insertMessage")
	public String insertMessage(String ManagerName, String address, String organizerName,String title, String content,Model model) {
		System.out.println(ManagerName);
		System.out.println(address);
		System.out.println(organizerName);
		System.out.println(content);
		
		BaseModel result = messageService.insertMessage(ManagerName, address, organizerName, title,content);
		if (!result.Successful) {
			model.addAttribute("ERROR", result.Message);
		}
		model.addAttribute("managerList", managerService.findAll());
		model.addAttribute("addressList", userService.findAllAddress());
		model.addAttribute("organizerList", organizerService.findAll());
		return "messages/msgView";
		
	}
}
