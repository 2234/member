package com.member.web.controller.goods;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.member.common.contants.Constants;
import com.member.entity.Goods;
import com.member.entity.base.BaseModel;
import com.member.entity.base.BaseModelJson;
import com.member.entity.vo.GoodsDTO;
import com.member.entity.vo.GoodsVo;
import com.member.service.GoodsService;
import com.member.web.controller.BaseController;

@Controller
@RequestMapping("/goods")
public class GoodsController extends BaseController {
	@Autowired
	GoodsService goodsService;

	@RequestMapping("/tiao")
	public String tiao() {
		return "goods/insert_goods";
	}

	/**
	 * 添加商品
	 * @param dto
	 * @param model
	 * @return
	 */
	@RequestMapping("/insertGoods")
	public String insertGoods(GoodsDTO dto, Model model) {
		System.out.println(dto.toString());
		BaseModel result=goodsService.insertGoods(dto);
		if(result.Successful){
			return "redirect:goodsList";
		}else{
			model.addAttribute("ERROR", result.Message);
			return "goods/insert_goods";
		}
		
	}
	/**
	 * 商品列表
	 * @param model
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	@RequestMapping("/goodsList")
	public String goodsList(Model model, Integer pageNum, Integer pageSize) {
		if (pageNum == null)
			pageNum = 1;
		if (pageSize == null)
			pageSize = 50;
		PageInfo<Goods> goodsList = goodsService.selectAllGoods(pageNum, pageSize);
		model.addAttribute("page", goodsList);
		return "goods/goods_list";
	}
	//删除
	@RequestMapping("/deleteGoods")
	public String deleteGoods(String id, Model model) {
		goodsService.deleteGoods(id);
		System.out.println(id);
		return "redirect:goodsList";
	}

	//修改前查询
	@ResponseBody
	@RequestMapping("/selectGoods")
	public Goods selectGoods(String id) {
		return goodsService.selectGoods(id);
	}
	//修改前查询
	@ResponseBody
	@RequestMapping("/checkGoods")
	public BaseModelJson<List<Goods>> checkGoods(String goodsName) {
		return goodsService.checkGoods(goodsName);
	}
	//修改
	@RequestMapping("/updateGoods")
	public String updateGoods(Goods goods) {
		goodsService.updateGoods(goods);
		return "redirect:goodsList";
	}
	//增加库存
	@RequestMapping("/addGoodsInventory")
	public String addGoodsInventory(String id,Integer num) {
		goodsService.updateGoodsInventory(id,num);
		return "redirect:goodsList";
	}
	//减少库存
	@RequestMapping("/deleteGoodsInventory")
	public String deleteGoodsInventory(String id,Integer num) {
		goodsService.updateGoodsInventory(id,-num);
		return "redirect:goodsList";
	}

	@RequestMapping("/findByKind")
	public String findByKind(Model model, String kindName, String findByKind, Integer pageNum, Integer pageSize) {
		if (pageNum == null)
			pageNum = 1;
		if (pageSize == null)
			pageSize = Constants.PAGE_SIZE;
		PageInfo<Goods> goods = goodsService.findAllByKindOrName(findByKind, kindName, pageNum, pageSize);
		model.addAttribute("page", goods);

		List<String> params = new ArrayList<String>();
		if (findByKind != null) {
			String param1 = "findByKind=" + findByKind + "&";
			params.add(param1);
		}
		if (kindName != null && kindName.length() != 0) {
			String param1 = "kindName=" + kindName + "&";
			params.add(param1);
		}
		model.addAttribute("params", params);

		return "goods/goods_list";
	}

}
