package com.member.common.contants;


/***
 * 
 * @author JettyLee
 *
 */
public class Constants {

	
	/* 分页操作时，每页只显示10条 */
	public static final Integer PAGE_SIZE = 8;
	
	/**
	 * 文件上传变量
	 * 
	 */

	public static final String UPLOAD_USER_IMG_PATH = "/resources/upload/user";


	/**
	 *用户登录状态 1.已登录  0.未登录
	 */
	public static final Integer USER_LOGIN = 1; // 用户已登录
	public static final Integer USER_LOGOUT = 0; // 用户未登录
	
	/** 阿里大鱼请求地址 */
	public static String ALIBABA_MSG_URL = "http://gw.api.taobao.com/router/rest";
	/** 阿里大鱼key */
	public static String ALIBABA_APP_KEY = "LTAIj9KR9IA0CQ8L";
	/** 阿里大鱼secret */
	public static String ALIBABA_APP_SECRET = "5rFEkGYs7B17gEFxMcm9SJ2bKee0HP";
	/** 阿里大鱼template */
	public static String ALIBABA_MSG_TEMPLATE_ID = "SMS_88190024";
	
	public static final String PIC_URL="/picture/";
}
