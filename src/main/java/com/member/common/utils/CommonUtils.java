package com.member.common.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class CommonUtils {
	  
	    public static String gbToUtf8(String str) throws UnsupportedEncodingException {   
	        StringBuffer sb = new StringBuffer();   
	        for (int i = 0; i < str.length(); i++) {   
	            String s = str.substring(i, i + 1);   
	            if (s.charAt(0) > 0x80) {   
	                byte[] bytes = s.getBytes("Unicode");   
	                String binaryStr = "";   
	                for (int j = 2; j < bytes.length; j += 2) {   
	                    // the first byte   
	                    String hexStr = getHexString(bytes[j + 1]);   
	                    String binStr = getBinaryString(Integer.valueOf(hexStr, 16));   
	                    binaryStr += binStr;   
	                    // the second byte   
	                    hexStr = getHexString(bytes[j]);   
	                    binStr = getBinaryString(Integer.valueOf(hexStr, 16));   
	                    binaryStr += binStr;   
	                }   
	                // convert unicode to utf-8   
	                String s1 = "1110" + binaryStr.substring(0, 4);   
	                String s2 = "10" + binaryStr.substring(4, 10);   
	                String s3 = "10" + binaryStr.substring(10, 16);   
	                byte[] bs = new byte[3];   
	                bs[0] = Integer.valueOf(s1, 2).byteValue();   
	                bs[1] = Integer.valueOf(s2, 2).byteValue();   
	                bs[2] = Integer.valueOf(s3, 2).byteValue();   
	                String ss = new String(bs, "UTF-8");   
	                sb.append(ss);   
	            } else {   
	                sb.append(s);   
	            }   
	        }   
	        return sb.toString();   
	    }   
	  
	    private static String getHexString(byte b) {   
	        String hexStr = Integer.toHexString(b);   
	        int m = hexStr.length();   
	        if (m < 2) {   
	            hexStr = "0" + hexStr;   
	        } else {   
	            hexStr = hexStr.substring(m - 2);   
	        }   
	        return hexStr;   
	    }   
	  
	    private static String getBinaryString(int i) {   
	        String binaryStr = Integer.toBinaryString(i);   
	        int length = binaryStr.length();   
	        for (int l = 0; l < 8 - length; l++) {   
	            binaryStr = "0" + binaryStr;   
	        }   
	        return binaryStr;   
	    }   
    
    public static void main (String [] args){
    	
    	String tempStr="%BE%A9AF0236";//准备转换的字符
    	try {
    		  String name=java.net.URLEncoder.encode("京AF0236", "GB2312");
    			System.out.println(name);
		String resl=	URLDecoder.decode(tempStr, "GB2312");
		System.out.println(resl);
		String result1=new String(resl.getBytes("GB2312"),"UTF-8");
		System.out.println(result1);
		} catch (UnsupportedEncodingException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	try {
    		String result=new String(tempStr.getBytes("UTF-8"),"GB2312");
			System.out.println(result);
			String result1=new String(result.getBytes("GB2312"),"UTF-8");
			System.out.println(result1);
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}//转换后的结果
    	   String str = "%u4EACAF0236";   
	        try {   
	            File f = new File("D:/test.txt");   
	            FileOutputStream fio = new FileOutputStream(f);   
	            String s = gbToUtf8(str);   
	            fio.write(s.getBytes("UTF-8"));   
	            fio.close();   
	        }   
	        catch (Exception e) {   
	            e.printStackTrace();   
	        }  
//    	System.out.println(utf8Togb2312("京AF0236"));
//    	System.out.println(gb2312ToUtf8("%u4EACAF0236"));
    }
}
