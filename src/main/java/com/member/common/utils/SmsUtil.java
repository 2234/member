package com.member.common.utils;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.member.common.contants.Constants;

/**
 * 类功能说明：阿里大鱼短信接口
 */
public class SmsUtil {
	private static final Log LOG = LogFactory.getLog(SmsUtil.class);

    
    public static Boolean sendHuiYuanMessage(String moblie, String name,String keyword1,String keyword2,String msgId) {
    	String msg="{\"name\":\"" + name + "\",\"keyword1\":\""+keyword1+"\",\"keyword2\":\""+keyword2+"\",\"keyword4\":\""+DateUtils.getReqDate(new Date())+"\"}";
    	System.out.println(msg);
    	return sendNewMsg(moblie,msg,msgId);
    }
    
    
    private static Boolean sendNewMsg(String moblie,String smsParamString,String msgId) {
    	
    	//设置超时时间-可自行调整
    	System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
    	System.setProperty("sun.net.client.defaultReadTimeout", "10000");
    	//初始化ascClient需要的几个参数
    	final String product = "Dysmsapi";//短信API产品名称（短信产品名固定，无需修改）
    	final String domain = "dysmsapi.aliyuncs.com";//短信API产品域名（接口地址固定，无需修改）
    	//替换成你的AK
    	final String accessKeyId = Constants.ALIBABA_APP_KEY;//你的accessKeyId,参考本文档步骤2
    	final String accessKeySecret =  Constants.ALIBABA_APP_SECRET;//你的accessKeySecret，参考本文档步骤2
    	//初始化ascClient,暂时不支持多region
    	IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId,
    	accessKeySecret);
    	try {
			DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
		} catch (ClientException e) {
			// TODO Auto-generated catch block
			System.out.println(e.toString());
			e.printStackTrace();
		}
    	IAcsClient acsClient = new DefaultAcsClient(profile);
    	 //组装请求对象
    	 SendSmsRequest request = new SendSmsRequest();
    	 //使用post提交
    	 request.setMethod(MethodType.POST);
    	 //必填:待发送手机号。支持以逗号分隔的形式进行批量调用，批量上限为1000个手机号码,批量调用相对于单条调用及时性稍有延迟,验证码类型的短信推荐使用单条调用的方式
    	 request.setPhoneNumbers(moblie);
    	 //必填:短信签名-可在短信控制台中找到
    	 request.setSignName("成都龙特名");
    	 //必填:短信模板-可在短信控制台中找到
    	 request.setTemplateCode(Constants.ALIBABA_MSG_TEMPLATE_ID);
    	 //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
    	 //友情提示:如果JSON中需要带换行符,请参照标准的JSON协议对换行符的要求,比如短信内容中包含\r\n的情况在JSON中需要表示成\\r\\n,否则会导致JSON在服务端解析失败
    	 request.setTemplateParam(smsParamString);
    	 //可选-上行短信扩展码(无特殊需求用户请忽略此字段)
    	 //request.setSmsUpExtendCode("90997");
    	 //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
    	 request.setOutId(msgId);
    	//请求失败这里会抛ClientException异常
    	SendSmsResponse sendSmsResponse;
		try {
			sendSmsResponse = acsClient.getAcsResponse(request);
			if(sendSmsResponse.getCode() != null && sendSmsResponse.getCode().equals("OK")) {
		    	//请求成功
		    		return true;
		    	}else{
		    		System.out.println("短信错误响应码："+sendSmsResponse.getCode()+",提示信息:"+sendSmsResponse.getMessage());
		    		LOG.info("短信错误响应码："+sendSmsResponse.getCode()+",提示信息:"+sendSmsResponse.getMessage());
		    	}
		} catch (ServerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			LOG.info("发送短信异常："+e.toString());
		} catch (ClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			LOG.info("发送短信异常："+e.toString());
		}
    	
		return false;
    }
public static void main(String[] args) {
	System.out.println(SmsUtil.sendHuiYuanMessage("18940988676,13998405849", "用户","123456测试","123456测试", UUIDUtil.getRandom32PK()));	
}
}
