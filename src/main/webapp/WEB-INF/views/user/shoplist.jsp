<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html lang="en">
<head>
</head>
<body>
	<div id="wrap">
		<!-- Top menu -->
		<%@ include file="../common/top_menu.jsp"%>

		<div class="container-fluid">

			<!-- Side menu -->
			<%@ include file="../common/side_menu.jsp"%>

			<!-- Main window -->
			<div class="main_container" id="users_page">

				<div class="row-fluid">
					<ul class="breadcrumb">
						<li>会员管理系统 <span class="divider">/</span></li>
						<li>会员管理<span class="divider">/</span></li>
						<li class="active">会员列表</li>
					</ul>
					<h2 class="heading">会员管理</h2>
				</div>
				<!-- /row-fluid -->

				<div class="row-fluid">
					<div class="widget widget-padding span12">
						<div class="widget-header">
							<i class="icon-group"></i>
							<h5>会员购买列表</h5>

						</div>
						<div class="widget-body">
							<div style="float: right; padding: 5px;">
								<%-- 								<form method="POST" action="${pageContext.request.contextPath}/user/sort" --%>
								<!-- 								class="form-horizontal " role="form"> -->
								<!-- 									<input value="" type="text" name="key" class="col-md-4"/>  -->
								<!-- 									<select name="sortNum" class="form-control col-md-4"> -->
								<!-- 										<option value="3">按姓名搜索</option> -->
								<!-- 										<option value="4">按电话号搜索</option> -->
								<!-- 										<option value="5">按身份证号搜索</option> -->
								<!-- 										<option value="6">按会员卡号搜索</option> -->
								<!-- 										<option value="7">按业务经理搜索</option> -->
								<!-- 									</select> -->
								<!-- 									<input type="submit" class="btn btn-primary" value="查詢" /> -->
								<!-- 								</form> -->
							</div>
							<div style="both: clear;"></div>
							<table class="table table-striped table-bordered dataTable">

								<thead>
									<tr>
										<th>序号</th>
										<th>活动编号</th>
										<th>业务编号</th>
										<th>姓名</th>
										<th>电话</th>
										<th>业务经理</th>
										<th>地址</th>
										<th>定金</th>
										<th>预付款</th>
										<th>尾款</th>
										<th>已付</th>
										<th>支付类别</th>
										<th>状态</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${page.list}" var="list" varStatus="vs">

										<tr>
											<td>${vs.index+1 }</td>
											<td>${list.activityId}</td>
											<td>${list.yewuId}</td>
											<td>${list.userName}</td>
											<td>${list.userPhone}</td>
											<td>${list.managerName}</td>
											<td>${list.address}</td>
											<td>${list.deposit}</td>
											<td>${list.advance}</td>
											<td>${list.weikuan}</td>
											<td>${list.paid}</td>
											<td>${list.payType}</td>
											<td><c:if test="${list.state==0}">
											正常
											</c:if> <c:if test="${list.state==1}">
											已退货
											</c:if></td>
											<td>
												<button class="btn btn-success" data-toggle="modal"
													data-target="#myModal"
													onclick="initModal('${list.shoppingId}')">查看套餐</button> 
													<c:if test="${list.state==0}">

													<form
														action="${pageContext.request.contextPath}/user/tuihuo"
														method="post">
														<input type="hidden" name="id" value="${list.shoppingId}">
														<input type="hidden" name="name" value="${list.userName}">
														<input type="hidden" name="phone" value="${list.userPhone}">
														<button class="btn btn-danger">退货</button>
													</form>
												</c:if>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
							<!-- 分页 -->
							<c:if test="${page.pages>0}">
								<jsp:include page="../common/pager.jsp">
									<jsp:param value="c_id" name="paramKey" />
									<jsp:param value="${c_id}" name="paramVal" />
								</jsp:include>
							</c:if>
						</div>
						<!-- /widget-body -->
					</div>
					<!-- /widget -->
				</div>
				<!-- /row-fluid -->

			</div>
			<!-- /Main window -->

		</div>
		<!--/.fluid-container-->
	</div>
	<!-- wrap ends-->


	<!-- Modal -->
	<div class="modal fade" id="myModal" style="display: none;"
		tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
		aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only"></span>
					</button>
					<h4 class="modal-title" id="myModalLabel">套餐详情</h4>
				</div>
				<div class="modal-body">
					<table>
						<tr>
							<th>商品名称</th>
							<th>商品规格</th>
							<th>商品价格</th>
							<th>商品数量</th>
						</tr>
						<tbody id="taochanContent">

						</tbody>

					</table>

				</div>
			</div>
		</div>
	</div>

	<script>
		function initModal(id) {
			var args = {
				"id" : id
			};
					$.post(
									"${pageContext.request.contextPath}/user/findShoppingTaoChan",
									args, function(data) {$("#taochanContent").empty();
										for(i=0;i<data.length;i++){
										$("#taochanContent").append(
											"<tr>"
											+"<td>"+data[i].goodsName+"</td>"
											+"<td>"+data[i].goodsSpec+"</td>"
											+"<td>"+data[i].goodsPrice+"</td>"
											+"<td>"+data[i].goodsCount+"</td>"
										+"</tr>");
										}
									}), "JSON";
		}
	</script>

	<%@ include file="../common/foot.jsp"%>
</body>
</html>
