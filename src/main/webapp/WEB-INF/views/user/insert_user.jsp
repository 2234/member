<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/lib/jquery.js"></script>
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script>
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/localization/messages_zh.js"></script>
<script>
$().ready(function() {
    $("#commentForm").validate();
});
</script>
<style>
.error{
	color:red;
}
</style>
</head>
<body>
	<div id="wrap">
		<!-- Top menu -->
		<%@ include file="../common/top_menu.jsp"%>

		<div class="container-fluid">

			<!-- Side menu -->
			<%@ include file="../common/side_menu.jsp"%>

			<!-- Main window -->
			<div class="main_container" id="dashboard_page">

				<div class="row-fluid">
					<ul class="breadcrumb">
						<li>会员管理系统<span class="divider">/</span></li>
						<li>人员管理 <span class="divider">/</span></li>
						<li class="active">新增</li>
					</ul>
					<h2 class="heading">人员管理</h2>
				</div>

				<div class="row-fluid">
					<div class="widget span12">
						<div class="widget-header">
							<i class="icon-plus"></i>
							<h5>新增人员</h5>
							<span>${ERROR}</span>
						</div>

						<div class="widget-body" style="overflow: hidden;">
							<form method="POST" id="commentForm" action="${pageContext.request.contextPath}/user/insertUser">
								<div class="form-group" style="margin-left: 5px;">
									<label>姓名：</label> <input type="text" class="form-control"
										id="name" placeholder="" name="name" minlength="2" required onblur="checkName();"/>
								</div><span id="ckName"></span>
								<div class="form-group" style="margin-left: 5px;">
									<label>身份证号：</label> <input type="text" class="form-control"
										id="idNum" placeholder="" name="idNum"/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>业务经理：</label> <input type="text" class="form-control"
										id="businessManager" placeholder="" name="businessManager" minlength="2" required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>地区：</label> <input type="text" class="form-control"
										id="address" placeholder="" name="address" required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>电话：</label> <input type="text" class="form-control"
										id="phone" placeholder="" name="phone" required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>会员卡号：</label> <input type="text" class="form-control"
										id="cardNum" placeholder="" name="cardNum"/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>建卡时间：</label> <input type="date" class="form-control"
										id="createCardTime" placeholder="" name="createCardTime"/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>积分：</label> <input type="text" class="form-control"
										id="integration" placeholder="" name="integration" required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>等级：</label> <input type="text" class="form-control"
										id="level" placeholder="" name="level" required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>组织者姓名：</label> <input type="text" class="form-control"
										id="organizerName" placeholder="" name="organizerName" />
								</div>
								
								<div class="form-group"
									style="float: right; marign-right: 5px; marign-bottom: 5px;">
									<input type="submit" class="btn btn-success" value="提交" />
								</div>
							</form>
						</div>

					</div>
					<!-- /widget span5 -->
				</div>
				<!-- /row-fluid -->

				<!-- /Main window -->

			</div>
			<!--/.fluid-container-->
		</div>
		<!-- wrap ends-->
<script type="text/javascript">
debugger
function checkName(){
var a = $("#name").val();
$.post("${pageContext.request.contextPath}/user/checkUser",{"name":a},function(data){
	var sp = document.getElementById("ckName");
    var msg="";
    if(data == "1"){
        sp.style.color="red";
        msg="用户名已存在。";
    }else if(data == "0"){
        sp.style.color="green";
        msg="用户名正确。";
    }
    sp.innerHTML = msg;
})
}
</script>


		<%@ include file="../common/foot.jsp"%>
</body>
</html>
