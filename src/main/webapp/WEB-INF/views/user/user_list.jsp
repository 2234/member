<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html lang="en">
<head>
</head>
<body>
	<div id="wrap">
		<!-- Top menu -->
		<%@ include file="../common/top_menu.jsp"%>

		<div class="container-fluid">

			<!-- Side menu -->
			<%@ include file="../common/side_menu.jsp"%>

			<!-- Main window -->
			<div class="main_container" id="users_page">

				<div class="row-fluid">
					<ul class="breadcrumb">
						<li>会员管理系统 <span class="divider">/</span></li>
						<li>会员管理<span class="divider">/</span></li>
						<li class="active">会员列表</li>
					</ul>
					<h2 class="heading">会员管理</h2>
				</div>
				<!-- /row-fluid -->

				<div class="row-fluid">
					<div class="widget widget-padding span12">
						<div class="widget-header">
							<i class="icon-group"></i>
							<h5>用户列表</h5>
							
						</div>
						<div class="widget-body">
						<div style="float: left; padding: 5px;">
							<form method="POST"
								action="${pageContext.request.contextPath}/user/export"
								role="form">
								<button class="btn btn-info">导出用户数据</button>
							</form>
						</div>
							<div style="float: right; padding: 5px;">
								<form method="POST"
									action="${pageContext.request.contextPath}/user/sort"
									class="form-horizontal " role="form">
									<input value="" type="text" name="key" class="col-md-4" /> <select
										name="sortNum" class="form-control col-md-4">
										<option value="3">按姓名搜索</option>
										<option value="4">按电话号搜索</option>
										<option value="5">按身份证号搜索</option>
										<option value="6">按会员卡号搜索</option>
										<option value="7">按业务经理搜索</option>
									</select> <input type="submit" class="btn btn-primary" value="查詢" />
								</form>
							
							</div>
							<div style="both: clear;"></div>
							<table id="users"
								class="table table-striped table-bordered dataTable">

								<thead>
									<tr>
										<th>序号</th>
										<th>姓名</th>
										<th>身份证号</th>
										<th>业务经理</th>
										<th>地区</th>
										<th>电话</th>
										<th>会员卡号</th>
										<th>建卡时间</th>
										<th>积分</th>
										<th>会员等级</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${page.list}" var="list" varStatus="vs">

										<tr>
											<td>${vs.index+1 }</td>
											<td>${list.name}</td>
											<td>${list.idNum}</td>
											<td>${list.businessManager}</td>
											<td>${list.address}</td>
											<td>${list.phone}</td>
											<td>${list.cardNum}</td>
											<td>${list.createCardTime}</td>
											<td>${list.integration}</td>
											<td>${list.level}</td>
											<td>
												<button class="btn btn-info" data-toggle="modal"
													data-target="#myModal" onclick="initModal('${list.id}')">修改</button>
												<form
													action="${pageContext.request.contextPath}/user/deleteUser"
													method="post">
													<input type="hidden" name="id" value="${list.id}">
													<button class="btn btn-danger">删除</button>
												</form>
												<form
													action="${pageContext.request.contextPath}/user/findshoplist"
													method="post">
													<input type="hidden" name="name" value="${list.name}">
													<input type="hidden" name="phone" value="${list.phone}">
													<button class="btn btn-success">查看购买记录</button>
												</form>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
							<!-- 分页 -->
							<c:if test="${page.pages>0}">
								<jsp:include page="../common/pager.jsp">
									<jsp:param value="c_id" name="paramKey" />
									<jsp:param value="${c_id}" name="paramVal" />
								</jsp:include>
							</c:if>
						</div>
						<!-- /widget-body -->
					</div>
					<!-- /widget -->
				</div>
				<!-- /row-fluid -->

			</div>
			<!-- /Main window -->

		</div>
		<!--/.fluid-container-->
	</div>
	<!-- wrap ends-->


	<!-- Modal -->
	<div class="modal fade" id="myModal" style="display: none;"
		tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
		aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only"></span>
					</button>
					<h4 class="modal-title" id="myModalLabel">修改人员信息</h4>
				</div>
				<div class="modal-body">
					<form method="POST"
						action="${pageContext.request.contextPath}/user/updateUser">
						<input type="hidden" name="id" id="id">
						<div class="form-group" style="margin-left: 5px;">
							<label>姓名：</label> <input type="text" class="form-control"
								id="name" placeholder="" name="name" minlength="2" required />
						</div>
						<div class="form-group" style="margin-left: 5px;">
							<label>身份证号：</label> <input type="text" class="form-control"
								id="idNum" placeholder="" name="idNum" />
						</div>
						<div class="form-group" style="margin-left: 5px;">
							<label>业务经理：</label> <input type="text" class="form-control"
								id="businessManager" placeholder="" name="businessManager"
								minlength="2" required />
						</div>
						<div class="form-group" style="margin-left: 5px;">
							<label>地区：</label> <input type="text" class="form-control"
								id="address" placeholder="" name="address" required />
						</div>
						<div class="form-group" style="margin-left: 5px;">
							<label>电话：</label> <input type="text" class="form-control"
								id="phone" placeholder="" name="phone" required />
						</div>
						<div class="form-group" style="margin-left: 5px;">
							<label>会员卡号：</label> <input type="text" class="form-control"
								id="cardNum" placeholder="" name="cardNum" />
						</div>
						<div class="form-group" style="margin-left: 5px;">
							<label>建卡时间：</label> <input type="date" class="form-control"
								id="createCardTime" placeholder="" name="createCardTime" />
						</div>
						<div class="form-group" style="margin-left: 5px;">
							<label>积分：</label> <input type="text" class="form-control"
								id="integration" placeholder="" name="integration" required />
						</div>
						<div class="form-group" style="margin-left: 5px;">
							<label>等级：</label> <input type="text" class="form-control"
								id="level" placeholder="" name="level" required />
						</div>
						<div class="form-group" style="margin-left: 5px;">
							<label>组织者姓名：</label> <input type="text" class="form-control"
								id="organizerName" placeholder="" name="organizerName" />
						</div>
						<div class="form-group"
							style="float: right; marign-right: 5px; marign-bottom: 5px;">
							<input type="submit" class="btn btn-success" value="提交" />
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<script>
		function initModal(id) {
			var args = {
				"id" : id
			};
			$.post("${pageContext.request.contextPath}/user/selectUser", args,
					function(data) {
						// 					var obj = eval(data);
						$("#id").val(data.id);
						$("#name").val(data.name);
						$("#idNum").val(data.idNum);
						$("#businessManager").val(data.businessManager);
						$("#address").val(data.address);
						$("#phone").val(data.phone);
						$("#cardNum").val(data.cardNum);
						$("#createCardTime").val(data.createCardTime);
						$("#integration").val(data.integration);
						$("#level").val(data.level);
						$("#organizerName").val(data.organizerName);
					}), "JSON";
		}
	</script>

	<%@ include file="../common/foot.jsp"%>
</body>
</html>
