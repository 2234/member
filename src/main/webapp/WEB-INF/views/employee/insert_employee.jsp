<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/lib/jquery.js"></script>
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script>
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/localization/messages_zh.js"></script>
<script>
$().ready(function() {
    $("#commentForm").validate();
});
</script>
<style>
.error{
	color:red;
}
</style>
</head>
<body>
	<div id="wrap">
		<!-- Top menu -->
		<%@ include file="../common/top_menu.jsp"%>

		<div class="container-fluid">

			<!-- Side menu -->
			<%@ include file="../common/side_menu.jsp"%>

			<!-- Main window -->
			<div class="main_container" id="dashboard_page">

				<div class="row-fluid">
					<ul class="breadcrumb">
						<li>会员管理系统 <span class="divider">/</span></li>
						<li>员工管理 <span class="divider">/</span></li>
						<li class="active">新增</li>
					</ul>
					<h2 class="heading">员工管理</h2>
				</div>

				<div class="row-fluid">
					<div class="widget span12">
						<div class="widget-header">
							<i class="icon-plus"></i>
							<h5>新增员工</h5>
							<span>${ERROR}</span>
						</div>

						<div class="widget-body" style="overflow: hidden;">
							<form method="POST" id="commentForm" action="${pageContext.request.contextPath}/employee/insertEmployee">
								<div class="form-group" style="margin-left: 5px;">
									<label>姓名：</label> <input type="text" class="form-control"
										id="name" placeholder="" name="name" minlength="2" required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>年龄：</label> <input type="text" class="form-control"
										id="age" placeholder="" name="age" required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>生日：</label> <input type="text" class="form-control"
										id="birthday" placeholder="" name="birthday" required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>电话：</label> <input type="text" class="form-control"
										id="address" placeholder="" name="address" required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>入职时间：</label> <input type="text" class="form-control"
										id="entryTime" placeholder="" name="entryTime" required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>试用期：</label> <input type="text" class="form-control"
										id="probation" placeholder="" name="probation" required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>备注：</label> <input type="text" class="form-control"
										id="remarks" placeholder="" name="remarks" required/>
								</div>
								
								
								<div class="form-group"
									style="float: right; marign-right: 5px; marign-bottom: 5px;">
									<input type="submit" class="btn btn-success" value="提交" />
								</div>
							</form>
						</div>

					</div>
					<!-- /widget span5 -->
				</div>
				<!-- /row-fluid -->

				<!-- /Main window -->

			</div>
			<!--/.fluid-container-->
		</div>
		<!-- wrap ends-->



		<%@ include file="../common/foot.jsp"%>
</body>
</html>
