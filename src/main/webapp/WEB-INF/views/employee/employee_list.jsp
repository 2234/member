<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
</head>
<body>
	<div id="wrap">
		<!-- Top menu -->
		<%@ include file="../common/top_menu.jsp"%>

		<div class="container-fluid">

			<!-- Side menu -->
			<%@ include file="../common/side_menu.jsp"%>

			<!-- Main window -->
			<div class="main_container" id="users_page">

				<div class="row-fluid">
					<ul class="breadcrumb">
						<li>会员管理系统 <span class="divider">/</span></li>
						<li>员工管理 <span class="divider">/</span></li>
						<li class="active">员工列表</li>
					</ul>
					<h2 class="heading">员工管理</h2>
				</div>
				<!-- /row-fluid -->

				<div class="row-fluid">
					<div class="widget widget-padding span12">
						<div class="widget-header">
							<i class="icon-group"></i>
							<h5>员工列表</h5>

						</div>
						<div class="widget-body">
							<div style="float: right; padding: 5px;">
								<form method="POST" action="${pageContext.request.contextPath}/employee/findByName"
								class="form-horizontal " role="form">
									<input value="" type="text" name="name" class="col-md-4"/> 
									<select name="findByName" class="form-control col-md-4">
										<option value="1">按姓名搜索</option>
									</select>
									<input type="submit" class="btn btn-primary" value="查詢" />
								</form>
							</div>
							<div style="both: clear;"></div>
							<table id="users"
								class="table table-striped table-bordered dataTable">

								<thead>
									<tr>
										<th>序号</th>
										<th>姓名</th>
										<th>年龄</th>
										<th>生日</th>
										<th>联系方式</th>
										<th>入职时间</th>
										<th>试用期</th>
										<th>备注</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${page.list}" var="list" varStatus="vs">

										<tr>
											<td>${vs.index+1 }</td>
											<td>${list.name}</td>
											<td>${list.age}</td>
											<td>${list.birthday}</td>
											<td>${list.address}</td>
											<td>${list.entryTime}</td>
											<td>${list.probation}</td>
											<td>${list.remarks}</td>
											<td>
												<button class="btn btn-info" data-toggle="modal" data-target="#myModal" onclick="initModal('${list.id}')">修改</button>
												<form action="${pageContext.request.contextPath}/employee/deleteEmployee" method="post">
													<input type="hidden" name="id" value="${list.id}">
													<button class="btn btn-danger">删除</button>
												</form>
											</td>
<%-- 											<td><fmt:formatDate value="${list.createCardTime}" type="date" pattern="yyyy-MM-dd"/> </td> --%>
										</tr>
									</c:forEach>
								</tbody>
							</table>
							<!-- 分页 -->
							<c:if test="${page.pages>0}">
								<jsp:include page="../common/pager.jsp">
									<jsp:param value="c_id" name="paramKey" />
									<jsp:param value="${c_id}" name="paramVal" />
								</jsp:include>
							</c:if>
						</div>
						<!-- /widget-body -->
					</div>
					<!-- /widget -->
				</div>
				<!-- /row-fluid -->

			</div>
			<!-- /Main window -->

		</div>
		<!--/.fluid-container-->
	</div>
	<!-- wrap ends-->
	
	
<!-- Modal -->
<div class="modal fade" id="myModal" style="display:none;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
        <h4 class="modal-title" id="myModalLabel">修改人员信息</h4>
      </div>
      <div class="modal-body">
      <form method="POST"  action="${pageContext.request.contextPath}/employee/updateEmployee">
      							<input type="hidden" name="id" id="id">
								<div class="form-group" style="margin-left: 5px;">
									<label>姓名：</label> <input type="text" class="form-control"
										id="name" placeholder="" name="name" minlength="2" required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>年龄：</label> <input type="text" class="form-control"
										id="age" placeholder="" name="age" required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>生日：</label> <input type="text" class="form-control"
										id="birthday" placeholder="" name="birthday" required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>电话：</label> <input type="text" class="form-control"
										id="address" placeholder="" name="address" required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>入职时间：</label> <input type="text" class="form-control"
										id="entryTime" placeholder="" name="entryTime" required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>试用期：</label> <input type="text" class="form-control"
										id="probation" placeholder="" name="probation" required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>备注：</label> <input type="text" class="form-control"
										id="remarks" placeholder="" name="remarks" required/>
								</div>
								<div class="form-group"
									style="float: right; marign-right: 5px; marign-bottom: 5px;">
									<input type="submit" class="btn btn-success" value="提交" />
								</div>
							</form>
      </div>
    </div>
  </div>
</div>

		<script>
			function initModal(id) {
				var args = {"id":id};
				$.post("${pageContext.request.contextPath}/employee/selectEmployee",args,function(data){
// 					var obj = eval(data);
					$("#id").val(data.id);
					$("#name").val(data.name);
					$("#age").val(data.age);
					$("#birthday").val(data.birthday);
					$("#address").val(data.address);
					$("#entryTime").val(data.entryTime);
					$("#probation").val(data.probation);
					$("#remarks").val(data.remarks);
				}),"JSON";
			}
		</script>

	<%@ include file="../common/foot.jsp"%>
</body>
</html>
