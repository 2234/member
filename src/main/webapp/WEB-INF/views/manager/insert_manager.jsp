<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/lib/jquery.js"></script>
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script>
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/localization/messages_zh.js"></script>
<script>
$().ready(function() {
    $("#commentForm").validate();
});
</script>
<style>
.error{
	color:red;
}
</style>
</head>
<body>
	<div id="wrap">
		<!-- Top menu -->
		<%@ include file="../common/top_menu.jsp"%>

		<div class="container-fluid">

			<!-- Side menu -->
			<%@ include file="../common/side_menu.jsp"%>

			<!-- Main window -->
			<div class="main_container" id="dashboard_page">

				<div class="row-fluid">
					<ul class="breadcrumb">
						<li>会员管理系统<span class="divider">/</span></li>
						<li>业务经理管理<span class="divider">/</span></li>
						<li class="active">新增</li>
					</ul>
					<h2 class="heading">业务经理管理</h2>
				</div>

				<div class="row-fluid">
					<div class="widget span12">
						<div class="widget-header">
							<i class="icon-plus"></i>
							<h5>新增业务经理</h5>
							<span>${ERROR}</span>
						</div>

						<div class="widget-body" style="overflow: hidden;">
							<form method="POST" id="commentForm" action="${pageContext.request.contextPath}/manager/insertManager">
								<div class="form-group" style="margin-left: 5px;">
									<label>姓名：</label> <input type="text" class="form-control"
										id="name" placeholder="" name="name" minlength="2" required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>电话：</label> <input type="text" class="form-control"
										id="phone" placeholder="" name="phone" required/>
								</div>
								<div class="form-group"
									style="float: right; marign-right: 5px; marign-bottom: 5px;">
									<input type="submit" class="btn btn-success" value="提交" />
								</div>
							</form>
						</div>

					</div>
					<!-- /widget span5 -->
				</div>
				<!-- /row-fluid -->

				<!-- /Main window -->

			</div>
			<!--/.fluid-container-->
		</div>
		<!-- wrap ends-->



		<%@ include file="../common/foot.jsp"%>
</body>
</html>
