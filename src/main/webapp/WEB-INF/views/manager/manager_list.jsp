<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
</head>
<body>
	<div id="wrap">
		<!-- Top menu -->
		<%@ include file="../common/top_menu.jsp"%>

		<div class="container-fluid">

			<!-- Side menu -->
			<%@ include file="../common/side_menu.jsp"%>

			<!-- Main window -->
			<div class="main_container" id="users_page">

				<div class="row-fluid">
					<ul class="breadcrumb">
						<li>会员管理系统 <span class="divider">/</span></li>
						<li>业务经理管理<span class="divider">/</span></li>
						<li class="active">业务经理列表</li>
					</ul>
					<h2 class="heading">业务经理管理</h2>
				</div>
				<!-- /row-fluid -->

				<div class="row-fluid">
					<div class="widget widget-padding span12">
						<div class="widget-header">
							<i class="icon-group"></i>
							<h5>业务经理列表</h5>

						</div>
						<div class="widget-body">
							<div style="both: clear;"></div>
							<table id="users"
								class="table table-striped table-bordered dataTable">

								<thead>
									<tr>
										<th>序号</th>
										<th>姓名</th>
										<th>电话</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${page.list}" var="list" varStatus="vs">

										<tr>
											<td>${vs.index+1 }</td>
											<td>${list.name}</td>
											<td>${list.phone}</td>
											<td>
												<button class="btn btn-info" data-toggle="modal" data-target="#myModal" onclick="initModal('${list.id}')">修改</button>
												<form action="${pageContext.request.contextPath}/manager/deleteManager" method="post">
													<input type="hidden" name="id" value="${list.id}">
													<button class="btn btn-danger">删除</button>
												</form>
											</td>
<%-- 											<td><fmt:formatDate value="${list.createCardTime}" type="date" pattern="yyyy-MM-dd"/> </td> --%>
										</tr>
									</c:forEach>
								</tbody>
							</table>
							<!-- 分页 -->
							<c:if test="${page.pages>0}">
								<jsp:include page="../common/pager.jsp">
									<jsp:param value="c_id" name="paramKey" />
									<jsp:param value="${c_id}" name="paramVal" />
								</jsp:include>
							</c:if>
						</div>
						<!-- /widget-body -->
					</div>
					<!-- /widget -->
				</div>
				<!-- /row-fluid -->

			</div>
			<!-- /Main window -->

		</div>
		<!--/.fluid-container-->
	</div>
	<!-- wrap ends-->
	
	
<!-- Modal -->
<div class="modal fade" id="myModal" style="display:none;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
        <h4 class="modal-title" id="myModalLabel">修改业务经理信息</h4>
      </div>
      <div class="modal-body">
      <form method="POST"  action="${pageContext.request.contextPath}/manager/updateManager">
      							<input type="hidden" name="id" id="id">
								<div class="form-group" style="margin-left: 5px;">
									<label>姓名：</label> <input type="text" class="form-control"
										id="name" placeholder="" name="name" minlength="2" required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>电话：</label> <input type="text" class="form-control"
										id="phone" placeholder="" name="phone" required/>
								</div>
								<div class="form-group"
									style="float: right; marign-right: 5px; marign-bottom: 5px;">
									<input type="submit" class="btn btn-success" value="提交" />
								</div>
							</form>
      </div>
    </div>
  </div>
</div>

		<script>
			function initModal(id) {
				var args = {"id":id};
				$.post("${pageContext.request.contextPath}/manager/selectManager",args,function(data){
// 					var obj = eval(data);
					$("#id").val(data.id);
					$("#name").val(data.name);
					$("#phone").val(data.phone);
				}),"JSON";
			}
		</script>

	<%@ include file="../common/foot.jsp"%>
</body>
</html>
