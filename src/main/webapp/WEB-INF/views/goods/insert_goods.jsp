<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<script
	src="http://static.runoob.com/assets/jquery-validation-1.14.0/lib/jquery.js"></script>
<script
	src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script>
<script
	src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/localization/messages_zh.js"></script>
<script>
	$().ready(function() {
		$("#commentForm").validate();
	});
</script>
<style>
.error {
	color: red;
}
</style>
</head>
<body>
	<div id="wrap">
		<!-- Top menu -->
		<%@ include file="../common/top_menu.jsp"%>

		<div class="container-fluid">

			<!-- Side menu -->
			<%@ include file="../common/side_menu.jsp"%>

			<!-- Main window -->
			<div class="main_container" id="dashboard_page">

				<div class="row-fluid">
					<ul class="breadcrumb">
						<li>会员管理系统 <span class="divider">/</span></li>
						<li>商品管理<span class="divider">/</span></li>
						<li class="active">商品列表</li>
					</ul>
					<h2 class="heading">商品管理</h2>
				</div>

				<div class="row-fluid">
					<div class="widget span12">
						<div class="widget-header">
							<i class="icon-plus"></i>
							<h5>新增商品</h5>
							<span>${ERROR}</span>
						</div>

						<div class="widget-body" style="overflow: hidden;">
							<form method="POST" id="commentForm"
								action="${pageContext.request.contextPath}/goods/insertGoods">
								<table>
									<tr>
										<td>商品名：</td>
										<td><input type="text" class="form-control" id="name"
											placeholder="" name="name" required /></td>
									</tr>
									<tr>
										<td>种类:</td>
										<td><input type="text" class="form-control" id="kind"
											placeholder="" name="kind" required /></td>
									</tr>
								</table>
								<table id="speclist">
									<tr>
										<th>规格</th>
										<th>总库存</th>
										<th>成本价</th>
										<th>零售价</th>
										<th><button onclick="addSpecList()">添加</button></th>
									</tr>
									<tr>
										<td><input type="text" class="form-control"
											placeholder="" name="list[0].spec" required /></td>
										<td><input type="text" class="form-control"
											placeholder="" name="list[0].total_inventory" required /></td>
										<td><input type="text" class="form-control"
											placeholder="" name="list[0].cost_price" required /></td>
										<td><input type="text" class="form-control"
											placeholder="" name="list[0].retail_price" required /></td>
									</tr>
								</table>
								<input type="submit" class="btn btn-success" value="提交" />
							</form>
						</div>

					</div>
					<!-- /widget span5 -->
				</div>
				<!-- /row-fluid -->


			</div>
			<!--/.fluid-container-->
		</div>
		<!-- wrap ends-->



		<%@ include file="../common/foot.jsp"%>
		<script>
			function addSpecList() {
				var lth = $('#speclist').children().children().length - 1;
				$("#speclist")
						.append(
								"<tr>"
										+ "<td>"
										+ "<input type='text' class='form-control' name='list["+lth+"].spec' required />"
										+ "</td>"
										+ "<td>"
										+ "<input type='text' class='form-control' name='list["+lth+"].total_inventory' required />"
										+ "</td>"
										+ "<td>"
										+ "<input type='text' class='form-control' name='list["+lth+"].cost_price' required />"
										+ "</td>"
										+ "<td>"
										+ "<input type='text' class='form-control' name='list["+lth+"].retail_price' required />"
										+ "</td>"
										+ "<td>"
										+ "<button onclick='deleteTr(this)'>移除</button>"
										+ "</td>" + "</tr>");
			}

			function deleteTr(e) {
				e.parentElement.parentElement.remove();
			}
		</script>
</body>
</html>
