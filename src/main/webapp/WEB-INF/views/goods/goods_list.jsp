<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
</head>
<body>
	<div id="wrap">
		<!-- Top menu -->
		<%@ include file="../common/top_menu.jsp"%>

		<div class="container-fluid">

			<!-- Side menu -->
			<%@ include file="../common/side_menu.jsp"%>

			<!-- Main window -->
			<div class="main_container" id="users_page">

				<div class="row-fluid">
					<ul class="breadcrumb">
						<li>会员管理系统<span class="divider">/</span></li>
						<li>商品管理<span class="divider">/</span></li>
						<li class="active">商品列表</li>
					</ul>
					<h2 class="heading">商品管理</h2>
				</div>
				<!-- /row-fluid -->

				<div class="row-fluid">
					<div class="widget widget-padding span12">
						<div class="widget-header">
							<i class="icon-group"></i>
							<h5>商品列表</h5>

						</div>
						<div class="widget-body">
							<div style="float: right; padding: 5px;">
								<form method="POST" action="${pageContext.request.contextPath}/goods/findByKind"
								class="form-horizontal " role="form">
									<input value="" type="text" name="kindName" class="col-md-4"/> 
									<select name="findByKind" class="form-control col-md-4">
										<option value="1">按种类搜索</option>
										<option value="2">按商品名搜索</option>
									</select>
									<input type="submit" class="btn btn-primary" value="查詢" />
								</form>
							</div>
							<div style="both: clear;"></div>
							<table id="users"
								class="table table-striped table-bordered dataTable">

								<thead>
									<tr>
										<th>序号</th>
										<th>商品名</th>
										<th>种类</th>
										<th>规格</th>
										<th>成本价</th>
										<th>零售价</th>
										<th>总库存</th>
<!-- 										<th>调货数量</th> -->
										<th>出库数量</th>
										<th>现有库存</th>
									
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${page.list}" var="list" varStatus="vs">
										<tr>
											<td>${vs.index+1 }</td>
											<td>${list.name}</td>
											<td>${list.kind}</td>
											<td>${list.spec}</td>
											<td>${list.cost_price}</td>
											<td>${list.retail_price}</td>
											<td>${list.total_inventory}</td>
<%-- 											<td>${list.transfer_cargo}</td> --%>
											<td>${list.out_goods_num}</td>
											<td>${list.existing_inventory}</td>
									
											<td>
												<button class="btn btn-success" data-toggle="modal" data-target="#addGoodsInventoryModal" onclick="initaddGoodsInventory('${list.id}')">入库</button>
												<button class="btn btn-danager" data-toggle="modal" data-target="#deleteGoodsInventoryModal" onclick="initdeleteGoodsInventory('${list.id}')">出库</button>
												<button class="btn btn-info" data-toggle="modal" data-target="#myModal" onclick="initModal('${list.id}')">修改</button>
												<form action="${pageContext.request.contextPath}/goods/deleteGoods" method="post">
													<input type="hidden" name="id" value="${list.id}">
													<button class="btn btn-danger">删除</button>
												</form>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
							<!-- 分页 -->
							<c:if test="${page.pages>0}">
								<jsp:include page="../common/pager.jsp">
									<jsp:param value="c_id" name="paramKey" />
									<jsp:param value="${c_id}" name="paramVal" />
								</jsp:include>
							</c:if>
						</div>
						<!-- /widget-body -->
					</div>
					<!-- /widget -->
				</div>
				<!-- /row-fluid -->

			</div>
			<!-- /Main window -->

		</div>
		<!--/.fluid-container-->
	</div>
	<!-- wrap ends-->
	
	
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" style="display:none;" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
        <h4 class="modal-title" id="myModalLabel">修改商品信息</h4>
      </div>
      <div class="modal-body">
      <form method="POST"  action="${pageContext.request.contextPath}/goods/updateGoods">
      							<input type="hidden" name="id" id="id">
								<div class="form-group" style="margin-left: 5px;">
									<label>商品名：</label> <input type="text" class="form-control"
										id="name" placeholder="" name="name" required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>种类：</label> <input type="text" class="form-control"
										id="kind" placeholder="" name="kind" required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>规格：</label> <input type="text" class="form-control"
										id="spec" placeholder="" name="spec" required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>成本价：</label> <input type="text" class="form-control"
										id="cost_price" placeholder="" name="cost_price" required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>零售价：</label> <input type="text" class="form-control"
										id="retail_price" placeholder="" name="retail_price" required/>
								</div>
								<div class="form-group"
									style="float: right; marign-right: 5px; marign-bottom: 5px;">
									<input type="submit" class="btn btn-success" value="提交" />
								</div>
							</form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="addGoodsInventoryModal" tabindex="-1" style="display:none;" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
        <h4 class="modal-title" id="myModalLabel">入库操作</h4>
      </div>
      <div class="modal-body">
      <form method="POST"  action="${pageContext.request.contextPath}/goods/addGoodsInventory">
      							<input type="hidden" name="id" id="id1">
								<div class="form-group" style="margin-left: 5px;">
									<label>入库数量：</label>
									<input type="text" class="form-control"  placeholder="" name="num" required/>
								</div>
								<div class="form-group"
									style="float: right; marign-right: 5px; marign-bottom: 5px;">
									<input type="submit" class="btn btn-success" value="提交" />
								</div>
							</form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="deleteGoodsInventoryModal" tabindex="-1" style="display:none;" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
        <h4 class="modal-title" id="myModalLabel">出库操作</h4>
      </div>
      <div class="modal-body">
      <form method="POST"  action="${pageContext.request.contextPath}/goods/deleteGoodsInventory">
      							<input type="hidden" name="id" id="id2">
								<div class="form-group" style="margin-left: 5px;">
									<label>出库数量：</label>
									<input type="text" class="form-control"  placeholder="" name="num" required/>
								</div>
								<div class="form-group"
									style="float: right; marign-right: 5px; marign-bottom: 5px;">
									<input type="submit" class="btn btn-success" value="提交" />
								</div>
							</form>
      </div>
    </div>
  </div>
</div>

		<script>
			function initModal(id) {
				var args = {"id":id};
				$.post("${pageContext.request.contextPath}/goods/selectGoods",args,function(data){
					$("#id").val(data.id);
					$("#name").val(data.name);
					$("#kind").val(data.kind);
					$("#spec").val(data.spec);
					$("#cost_price").val(data.cost_price);
					$("#retail_price").val(data.retail_price);
				}),"JSON";
			}
			function initdeleteGoodsInventory(id) {
			$("#id2").val(id);
			}
			function initaddGoodsInventory(id) {
			$("#id1").val(id);
			
			}
		</script>

	<%@ include file="../common/foot.jsp"%>
</body>
</html>
