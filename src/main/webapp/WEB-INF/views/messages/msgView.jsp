<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
</head>
<body>
	<div id="wrap">
		<!-- Top menu -->
		<%@ include file="../common/top_menu.jsp"%>

		<div class="container-fluid">

			<!-- Side menu -->
			<%@ include file="../common/side_menu.jsp"%>

			<!-- Main window -->
			<div class="main_container" id="users_page">

				<div class="row-fluid">
					<ul class="breadcrumb">
						<li>会员管理系统 <span class="divider">/</span></li>
						<li>短信管理 <span class="divider">/</span></li>
						<li class="active">发送短信</li>
					</ul>
					<h2 class="heading">发送短信</h2>
				</div>
				<!-- /row-fluid -->

				<div class="row-fluid">
					<div class="widget widget-padding span12">
						<div class="widget-header">
							<i class="icon-group"></i>
							<span style="color:red;">${ERROR}</span>
						</div>
						<div class="widget-body">
							<div style="float: auto; padding: 5px;">
								<form method="POST"
									action="${pageContext.request.contextPath}/message/insertMessage" onsubmit="return checkContent()">
									<div class="form-group" style="margin-left: 5px;">
										<label>业务经理</label> <select name="ManagerName"><option
												value="0">请选择业务经理</option>
											<c:forEach items="${managerList}" var="list" varStatus="vs">
												<option value="${list.name}">${list.name}</option>
											</c:forEach>
										</select>
									</div>
								
									<div class="form-group" style="margin-left: 5px;">
										<label>地区</label><select name="address"><option
												value="0">请选择地区</option>
											<c:forEach items="${addressList}" var="item" varStatus="vs">
												<option value="${item}">${item}</option>
											</c:forEach>


										</select>
									</div>
									<div class="form-group" style="margin-left: 5px;">
										<label>组织者</label><select name="organizerName"><option
												value="0">请选择组织者</option>
											<c:forEach items="${organizerList}" var="list" varStatus="vs">
												<option value="${list.name}">${list.name}</option>
											</c:forEach>
										</select>
									</div>
									<label>短信主题</label>
									<input  placeholder="请输入短信主题" name="title" id="title" />
									<label>短信内容</label>
									<textarea style="width: 300px; height: 200px;"
										placeholder="请输入短信内容" name="content" id="content"></textarea>
									<input type="submit" class="btn btn-primary" value="发送" />
								</form>
							</div>
							<div style="both: clear;"></div>
							<!-- 分页 -->
							<c:if test="${page.pages>0}">
								<jsp:include page="../common/pager.jsp">
									<jsp:param value="c_id" name="paramKey" />
									<jsp:param value="${c_id}" name="paramVal" />
								</jsp:include>
							</c:if>
						</div>
						<!-- /widget-body -->
					</div>
					<!-- /widget -->
				</div>
				<!-- /row-fluid -->

			</div>
			<!-- /Main window -->

		</div>
		<!--/.fluid-container-->
	</div>
	<!-- wrap ends-->

	<%@ include file="../common/foot.jsp"%>
</body>
<script type="text/javascript">
	function checkContent() {
		var content=$('#content').val();
		if(isEmpty(content)){
			alert('请认真填写短信内容');
			return false;
		}
			return true;
		
	}
</script>
</html>
