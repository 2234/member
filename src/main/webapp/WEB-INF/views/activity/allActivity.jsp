<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">


</head>
<body onload="checkError()">
	<div id="wrap">
		<%@ include file="../common/top_menu.jsp"%>

		<div class="container-fluid">

			<!-- Side menu -->
			<%@ include file="../common/side_menu.jsp"%>

			<!-- Main window -->
			<div class="main_container" id="dashboard_page"
				style="overflow: auto">

				<div class="row-fluid">
					<ul class="breadcrumb">
						<li>会员管理系统 <span class="divider">/</span></li>
						<li>活动管理 <span class="divider">/</span></li>
						<li class="active">新增活动基本信息</li>
					</ul>
				</div>
				<div>
					<input type="hidden" value="${ERROR}" id="error">
					<form method="POST" id="commentForm"
						action="${pageContext.request.contextPath}/activity/InsertChuTuan">
						<label>出团时间：</label>
						<table>
							<tr>
								<th>开始时间</th>
								<th>结束时间</th>
								<th>共计（天）</th>
							</tr>
							<tr>
								<td><input type="date" class="form-control" id="startTime"
									placeholder="请以yyyy-MM-dd格式输入" name="startTime" required /></td>
								<td><input type="date" class="form-control" id="endTime"
									placeholder="请以yyyy-MM-dd格式输入" name="endTime" required /></td>
								<td><span></span><input type="text" digits="true"
									id="dayNum" placeholder="多少(整数)" name="dayNum" required /></td>
							</tr>
						</table>
						<label>路线:</label>
						<table>
							<tr>
								<th>起点</th>
								<th>终点</th>
							</tr>
							<tr>
								<td><input type="text" class="form-control" id="loadStart"
									placeholder="" name="loadStart" required /></td>
								<td><input type="text" class="form-control" id="loadEnd"
									placeholder="" name="loadEnd" required /></td>
							</tr>
							<tr>
								<th>人数</th>
								<th>业务经理</th>
							</tr>
							<tr>
								<td><input type="text" class="form-control" id="peopleNum"
									placeholder="请输入整数" name="peopleNum" digits="true" required
									onchange="CalTuanFei()" /></td>
								<td><input type="text" class="form-control"
									id="managerName" placeholder="" name="managerName" required /></td>
							</tr>
							<tr>
								<th>团费标准</th>
								<th>特殊说明</th>
							</tr>
							<tr>
								<td><input type="text" class="form-control" placeholder=""
									name="groupMonkeyStandard" number="true" required
									onchange="CalTuanFei()" /></td>
								<td><input type="text" class="form-control" placeholder=""
									name="remark" number="true" required onchange="CalTuanFei()" /></td>
							</tr>
						</table>
						<div class="form-group" style="margin-left: 5px;">
							<label>团费收入：</label> <input type="text" class="form-control"
								id="groupMonkey" placeholder="无需手动输入" name="groupMonkey"
								required number="true" />
						</div>
						<div class="form-group" style="margin-left: 5px;">
							<label>餐费：</label>
							<table id="eatlist">
								<tr>
									<th>第几天</th>
									<th>地点</th>
									<th>标准</th>
									<th>备注</th>
									<th>费用小计</th>
									<th><button onclick="addEatActivity()">增加</button></th>
								</tr>

								<tr>
									<td><input type="text" class="form-control"
										name="eatList[0].dayNum" digits="true" required /></td>
									<td><input type="text" class="form-control"
										name="eatList[0].eatPosition" required /></td>
									<td><input type="text" class="form-control"
										name="eatList[0].eatStandard" number="true" required
										onchange="CalEat(this)" required /></td>
									<td><input type="text" class="form-control"
										name="eatList[0].eatRemark" number="true"
										onchange="CalEat(this)" /></td>
									<td><input type="text" class="form-control eatCost"
										name="eatList[0].eatCost" required placeholder="无需手动输入" /></td>
								</tr>

							</table>
						</div>
						<div class="form-group" style="margin-left: 5px;">
							<label>住宿：</label>
							<table id="staylist">
								<tr>
									<th>第几天</th>
									<th>地点</th>
									<th>标准</th>
									<th>备注</th>
									<th>费用小计</th>
									<th><button onclick="addStayList()">增加</button></th>
								</tr>

								<tr>
									<td><input type="text" class="form-control"
										name="stayList[0].dayNum" digits="true" required /></td>
									<td><input type="text" class="form-control"
										name="stayList[0].stayPosition" required /></td>
									<td><input type="text" class="form-control"
										name="stayList[0].stayStandard" number="true"
										onchange="CalStay(this)" required /></td>
									<td><input type="text" class="form-control"
										name="stayList[0].stayRemark" number="true"
										onchange="CalStay(this)" /></td>
									<td><input type="text" class="form-control stayCost"
										name="stayList[0].stayCost" required placeholder="无需手动输入" /></td>
								</tr>

							</table>
						</div>
						<div class="form-group" style="margin-left: 5px;">
							<label>晚会：</label>
							<table>
								<tr>
									<th>晚会场地费</th>
									<th>晚会演出费</th>
									<th>晚会公摊费</th>
									<th>晚会工作人员费用</th>
								</tr>

								<tr>
									<td><input type="text" class="form-control"
										name="eveningSiteCost" number="true" required /></td>
									<td><input type="text" class="form-control"
										name="eveningShowCost" number="true" required /></td>
									<td><input type="text" class="form-control"
										name="eveningShareCost" number="true" required /></td>
									<td><input type="text" class="form-control"
										name="eveningWorkersCost" number="true" required /></td>
								</tr>

							</table>
						</div>
						<div class="form-group" style="margin-left: 5px;">
							<label>会场：</label>
							<table>
								<tr>
									<th>场地费</th>
									<th>会场公摊费用</th>
									<th>会务（工作人员）费用</th>
									<th>费用小计</th>
								</tr>
								<tr>
									<td><input type="text" class="form-control" placeholder=""
										name="siteCost" number="true" onChange="CalSiteCost()"
										required /></td>
									<td><input type="text" class="form-control" placeholder=""
										name="sharedCost" number="true" onChange="CalSiteCost()"
										required /></td>
									<td><input type="text" class="form-control" placeholder=""
										name="siteWorkerCost" number="true" onChange="CalSiteCost()"
										required /></td>
									<td><input type="text" class="form-control"
										placeholder="无需手动输入" name="siteAllCost" number="true" required /></td>
								</tr>
							</table>
						</div>
						<div class="form-group" style="margin-left: 5px;">
							<label>礼品：</label>
							<div>
								<table id="lipinlist">
									<tr>
										<th>礼品名称</th>
										<th>礼品规格</th>
										<th>礼品价值</th>
										<th>礼品数量</th>
										<th>费用小计</th>
										<th><button onclick="addLiPin()">增加</button></th>
									</tr>
									<tr>
										<td><input type="hidden" class="form-control"
											name="lipinList[0].type" value='1' required /> <input
											type="text" class="form-control"
											name="lipinList[0].goodsName" required
											onchange="checkGoods(this)" /></td>
										<td><select name="lipinList[0].goodsSpec"
											class="form-control" required="required"
											onChange="inputGoodsPrice(this)">
										</select></td>
										<td><input type="text" class="form-control"
											name="lipinList[0].goodsPrice" required
											onChange="calGoodsCost(this)" /></td>
										<td><input type="text" class="form-control"
											name="lipinList[0].goodsCount" digits="true" required
											onChange="calGoodsCost(this)" /></td>
										<td><input type="text" class="form-control lipinCost"
											name="lipinList[0].goodsCost" required="required"
											placeholder="无需手动输入" /></td>

									</tr>

								</table>
							</div>
						</div>
						<div class="form-group" style="margin-left: 5px;">
							<label>套餐：</label>
							<div>
								<table id="taochanlist">
									<tr>
										<th>商品名称</th>
										<th>商品规格</th>
										<th>商品价格</th>
										<th>商品数量</th>
										<th>费用小计</th>
										<th><button onclick="addTaochan()">增加</button></th>
									</tr>
									<tr>
										<td><input type="hidden" class="form-control"
											name="taochanList[0].type" value='2' required /> <input
											type="text" class="form-control"
											name="taochanList[0].goodsName" required
											onchange="checkGoods(this)" /></td>
										<td><select name="taochanList[0].goodsSpec"
											class="form-control" required="required"
											onChange="inputGoodsPrice(this)">
										</select></td>
										<td><input type="text" class="form-control"
											name="taochanList[0].goodsPrice" required
											onChange="calGoodsCost(this)" /></td>
										<td><input type="text" class="form-control"
											name="taochanList[0].goodsCount" digits="true" required
											onChange="calGoodsCost(this)" /></td>
										<td><input type="text" class="form-control taochanCost"
											name="taochanList[0].goodsCost" required placeholder="无需手动输入" /></td>
									</tr>

								</table>
							</div>
						</div>
						<div class="form-group" style="margin-left: 5px;">
							<label>车辆：</label>
							<table id="carlist">
								<tr>
									<th>车型</th>
									<th>座位数</th>
									<th>数量</th>
									<th>标准</th>
									<th>费用</th>
									<th><button onclick="addCarList()">增加</button></th>
								</tr>

								<tr>
									<td><input type="text" class="form-control"
										name="carList[0].carModel" required /></td>
									<td><input type="text" class="form-control"
										name="carList[0].carSite" digits="true" required /></td>
									<td><input type="text" class="form-control"
										name="carList[0].carNum" digits="true" required
										onChange="calCarCost(this)" /></td>
									<td><input type="text" class="form-control"
										name="carList[0].carStandard" required
										onChange="calCarCost(this)" /></td>
									<td><input type="text" class="form-control carCost"
										name="carList[0].carCost" required placeholder="无需手动输入" /></td>
								</tr>
							</table>
						</div>
						<div class="form-group" style="margin-left: 5px;">
							<label>保险：</label>
							<table>
								<tr>
									<th>保险标准</th>
									<th>保险费用</th>
								</tr>
								<tr>
									<td><input type="text" class="form-control"
										id="insuranceStandard" placeholder="" name="insuranceStandard"
										required onChange="CalInsuranceCost(this)" /></td>
									<td><input type="text" class="form-control"
										id="insuranceCost" placeholder="无需手动输入" name="insuranceCost"
										required onChange="CalInsuranceCost(this)" /></td>
								</tr>
							</table>
						</div>
						<div class="form-group" style="margin-left: 5px;">
							<label>费用：</label> 
							
							<table>
								<tr>
									<th>手续费用</th>
									<th>讲师费用</th>
								</tr>
								<tr>
									<td><input type="text" class="form-control"
								id="serviceCost" placeholder="" name="serviceCost" required /></td>
									<td><input type="text" class="form-control"
								id="lecturerCost" placeholder="" name="lecturerCost" required /></td>
								</tr>
							</table>
							
						</div>

						<div class="form-group" style="margin-left: 5px;">
							<table>
								<tr>
									<td>送货</td>
								</tr>
								<tr>
									<th>送货开支</th>
									<th>送货车费</th>
								</tr>
								<tr>
									<td><input type="text" class="form-control"
										name="deliveryCost" required /></td>
									<td><input type="text" class="form-control" name="deliveryCarCost"
										required /></td>
								</tr>
								<tr>
									<td>业务完成情况</td>
								</tr>
								<tr>
									<td>订金数量:</td>
									<td><input type="text" class="form-control"
										name="dinghuoNum" digits="true" required /></td>
								</tr>
								<tr>
									<td>已收款数:</td>
									<td><input type="text" class="form-control"
										name="yshouhuokuanNum" digits="true" required
										onchange="calYeWuCost()" /></td>
								</tr>
								<tr>
									<td>未收款数:</td>
									<td><input type="text" class="form-control"
										name="wshouhuokuanNum" digits="true" required
										onchange="calYeWuCost()" /></td>
								</tr>
								<tr>
									<td>已退款:</td>
									<td><input type="text" class="form-control"
										name="tuihuokuanNum" digits="true" required
										onchange="calYeWuCost()" /></td>
								</tr>
								<tr>
									<td>收款合计:</td>
									<td><input type="text" class="form-control"
										name="shoukuanNum" digits="true" required placeholder="无需手动输入" /></td>
								</tr>
								<tr>
									<td>发货:</td>
									<td><input type="text" class="form-control" name="sendNum"
										digits="true" required /></td>
								</tr>
								<tr>
									<td>开支剩余:</td>
									<td><input type="text" class="form-control"
										name="remainingCost" required></td>
								</tr>
								<tr>
									<td>盈亏情况：
										<button onclick="calTotalCost()">合计</button>
									</td>
									<td><input type="text" class="form-control" name="earning"
										required placeholder="请点击合计"></td>
								</tr>
							</table>
						</div>
						<div class="form-group"
							style="float: right; marign-right: 5px; marign-bottom: 5px;">
							<input type="submit" class="btn btn-success" value="提交" />
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		function addEatActivity() {
			var lth = $('#eatlist').children().children().length - 1;
			$("#eatlist")
					.append(
							"<tr>"
									+ "<td>"
									+ "<input type='text' class='form-control' name='eatList["+lth+"].dayNum'   digits='true' required />"
									+ "</td>"
									+ "<td>"
									+ "<input type='text' class='form-control' name='eatList["+lth+"].eatPosition' required />"
									+ "</td>"
									+ "<td>"
									+ "<input type='text' class='form-control' name='eatList["
									+ lth
									+ "].eatStandard'   onchange='CalEat(this)' number='true' required />"
									+ "</td>"
									+ "<td>"
									+ "<input type='text' class='form-control' name='eatList["
									+ lth
									+ "].eatRemark'  onchange='CalEat(this)' number='true'  />"
									+ "</td>"
									+ "<td>"
									+ "<input type='text' class='form-control eatCost' name='eatList["+lth+"].eatCost' number='true' required />"
									+ "</td>"
									+ "<td>"
									+ "<button onclick='deleteTr(this)'>移除</button>"
									+ "</td>" + "</tr>");
		}
		function addCarList() {
			var lth = $('#carlist').children().children().length - 1;
			$("#carlist")
					.append(
							"<tr>"
									+ "<td>"
									+ "<input type='text' class='form-control' name='carList["+lth+"].carModel' required />"
									+ "</td>"
									+ "<td>"
									+ "<input type='text' class='form-control' name='carList["+lth+"].carSite' required />"
									+ "</td>"
									+ "<td>"
									+ "<input type='text' class='form-control' name='carList["
									+ lth
									+ "].carNum'  digits='true'  onChange='calCarCost(this)' required/>"
									+ "</td>"
									+ "<td>"
									+ "<input type='text' class='form-control' name='carList["
									+ lth
									+ "].carStandard' number='true'  onChange='calCarCost(this)' required/>"
									+ "</td>"
									+ "<td>"
									+ "<input type='text' class='form-control carCost' name='carList["+lth+"].carCost' number='true' required />"
									+ "</td>"
									+ "<td>"
									+ "<button onclick='deleteTr(this)'>移除</button>"
									+ "</td>" + "</tr>");
		}
		function addStayList() {
			var lth = $('#staylist').children().children().length - 1;
			$("#staylist")
					.append(
							"<tr>"
									+ "<td>"
									+ "<input type='text' class='form-control' name='stayList["+lth+"].dayNum' digits='true' required />"
									+ "</td>"
									+ "<td>"
									+ "<input type='text' class='form-control' name='stayList["+lth+"].stayPosition' required />"
									+ "</td>"
									+ "<td>"
									+ "<input type='text' class='form-control' name='stayList["
									+ lth
									+ "].stayStandard' onchange='CalStay(this)' number='true' required />"
									+ "</td>"
									+ "<td>"
									+ "<input type='text' class='form-control' name='stayList["
									+ lth
									+ "].stayRemark' onchange='CalStay(this)' number='true' required />"
									+ "</td>"
									+ "<td>"
									+ "<input type='text' class='form-control stayCost' name='stayList["+lth+"].stayCost' number='true'  />"
									+ "</td>"
									+ "<td>"
									+ "<button onclick='deleteTr(this)'>移除</button>"
									+ "</td>" + "</tr>");
		}
		function addLiPin() {
			var lth = $('#lipinlist').children().children().length - 1;
			$("#lipinlist")
					.append(
							"<tr>"
									+ "<td>"
									+ "<input type='hidden' class='form-control' name='lipinList["+lth+"].type' value='1' required />"
									+ "<input type='text' class='form-control' name='lipinList["
									+ lth
									+ "].goodsName' required onchange='checkGoods(this)'/>"
									+ "</td>"
									+ "<td>"
									+ "<select name='lipinList["
									+ lth
									+ "].goodsSpec' class='form-control' required='required' onChange='inputGoodsPrice(this)'>"
									+ "</td>"
									+ "<td>"
									+ "<input type='text' class='form-control' name='lipinList["
									+ lth
									+ "].goodsPrice' number='true' required  onChange='calGoodsCost(this)'/>"
									+ "</td>"
									+ "<td>"
									+ "<input type='text' class='form-control' name='lipinList["
									+ lth
									+ "].goodsCount' digits='true' required onChange='calGoodsCost(this)'/>"
									+ "</td>"
									+ "<td>"
									+ "<input type='text' class='form-control lipinCost' name='lipinList["+lth+"].goodsCost' number='true' required />"
									+ "</td>"
									+ "<td>"
									+ "<button  onclick='deleteTr(this)'>移除</button>"
									+ "</td>" + "</tr>");
		}
		function addTaochan() {
			var lth = $('#taochanlist').children().children().length - 1;
			$("#taochanlist")
					.append(
							"<tr>"
									+ "<td>"
									+ "<input type='hidden' class='form-control' name='taochanList["+lth+"].type' value='2' required />"
									+ "<input type='text' class='form-control' name='taochanList["
									+ lth
									+ "].goodsName' required onchange='checkGoods(this)'/>"
									+ "</td>"
									+ "<td>"
									+ "<select name='taochanList["
									+ lth
									+ "].goodsSpec' class='form-control' required='required'  onChange='inputGoodsPrice(this)' >"
									+ "</td>"
									+ "<td>"
									+ "<input type='text' class='form-control' name='taochanList["
									+ lth
									+ "].goodsPrice' number='true' required onChange='calGoodsCost(this)'/>"
									+ "</td>"
									+ "<td>"
									+ "<input type='text' class='form-control' name='taochanList["
									+ lth
									+ "].goodsCount' digits='true' required  onChange='calGoodsCost(this)'/>"
									+ "</td>"
									+ "<td>"
									+ "<input type='text' class='form-control taochanCost' name='taochanList["+lth+"].goodsCost' number='true' required />"
									+ "</td>"
									+ "<td>"
									+ "<button onclick='deleteTr(this)'>移除</button>"
									+ "</td>" + "</tr>");
		}
		function deleteTr(e) {
			e.parentElement.parentElement.remove();
		}
		function checkGoods(e) {
			var goodsName = e.value;
			var name1 = e.name;
			var name2 = name1.replace(/goodsName/, "goodsSpec");
			var name3 = name1.replace(/goodsName/, "goodsPrice");
			var goodsSpec = $('select[name="' + name2 + '"]');
			var goodsPrice = $('input[name="' + name3 + '"]');
			if (!isEmpty(goodsName)) {
				var args = {
					"goodsName" : goodsName
				};
						$
								.post(
										"${pageContext.request.contextPath}/goods/checkGoods",
										args,
										function(data) {
											if (data.Successful) {
												var num = 0;
												goodsSpec.empty();
												for (; num < data.Data.length; num++) {
													goodsSpec
															.append("<option value='"+data.Data[num].spec+"' price='"+data.Data[num].retail_price+"'>"
																	+ data.Data[num].spec
																	+ "</option>");
												}
												goodsPrice
														.val(data.Data[0].retail_price);

											} else {
												alert('商品名：' + goodsName
														+ ',填写有误，商品不存在!');
											}
										}), "JSON";
			}
		}
		function inputGoodsPrice(e) {
			var price = $('select[name="' + e.name + '"]').find(
					"option:selected").attr("price");

			var name1 = e.name;

			var name2 = name1.replace(/goodsSpec/, "goodsPrice");
			var name3 = name1.replace(/goodsSpec/, "goodsCount");
			$('input[name="' + name2 + '"]').val(price);
			var goodsCount = $('input[name="' + name3 + '"]').val();
			if (!isEmpty(goodsCount)) {
				var name4 = name1.replace(/goodsSpec/, "goodsCost");
				$('input[name="' + name4 + '"]').val(
						parseFloat(goodsCount) * parseFloat(price));
			}

		}
		function calGoodsCost(e) {
			var name1 = e.name;
			if (isContains(name1, 'goodsPrice')) {
				var goodsPrice = e.value;
				var name2 = name1.replace(/goodsPrice/, "goodsCount");
				var goodsCount = $('input[name="' + name2 + '"]').val();
				var name3 = name1.replace(/goodsPrice/, "goodsCost");
			} else {
				var goodsCount = e.value;
				var name2 = name1.replace(/goodsCount/, "goodsPrice");
				var goodsPrice = $('input[name="' + name2 + '"]').val();
				var name3 = name1.replace(/goodsCount/, "goodsCost");
			}
			if (!isEmpty(goodsCount) && !isEmpty(goodsPrice)) {
				$('input[name="' + name3 + '"]').val(
						parseFloat(goodsCount) * parseFloat(goodsPrice));
			}
		}

		function calCarCost(e) {
			var name1 = e.name;
			if (isContains(name1, 'carNum')) {
				var carNum = e.value;
				var name2 = name1.replace(/carNum/, "carStandard");
				var carStandard = $('input[name="' + name2 + '"]').val();
				var name3 = name1.replace(/carNum/, "carCost");
			} else {
				var carStandard = e.value;
				var name2 = name1.replace(/carStandard/, "carNum");
				var carNum = $('input[name="' + name2 + '"]').val();
				var name3 = name1.replace(/carStandard/, "carCost");
			}
			if (!isEmpty(carStandard) && !isEmpty(carNum)) {
				$('input[name="' + name3 + '"]').val(
						parseFloat(carStandard) * parseFloat(carNum));
			}
		}
		function calYeWuCost() {
			var yshouhuokuanNum = $('input[name=yshouhuokuanNum]').val();
			var wshouhuokuanNum = $('input[name=wshouhuokuanNum]').val();
			var tuihuokuanNum = $('input[name=tuihuokuanNum]').val();
			if (!isEmpty(yshouhuokuanNum) && !isEmpty(wshouhuokuanNum)
					&& !isEmpty(tuihuokuanNum)) {
				$('input[name=shoukuanNum]').val(
						parseFloat(yshouhuokuanNum)
								+ parseFloat(wshouhuokuanNum)
								- parseFloat(tuihuokuanNum));
			}

		}

		function calTotalCost() {

			var groupMonkey = $('input[name=groupMonkey]').val();//4
			var shoukuanNum = $('input[name=shoukuanNum]').val();//36
			var remainingCost = $('input[name=remainingCost]').val();//37

			var eatCost = 0;//7
			var eatCostList = $("input.eatCost");
			for (var i = 0; i < eatCostList.length; i++) {
				eatCost = parseFloat(eatCost)
						+ parseFloat(eatCostList[i].value);
			}
			var stayCost = 0;//10
			var stayCostList = $("input.stayCost");
			for (var i = 0; i < stayCostList.length; i++) {
				stayCost = parseFloat(stayCost)
						+ parseFloat(stayCostList[i].value);
			}
			var eveningSiteCost = $('input[name=eveningSiteCost]').val();//11
			var eveningShowCost = $('input[name=eveningShowCost]').val();//12
			var eveningShareCost = $('input[name=eveningShareCost]').val();//13
			var eveningWorkersCost = $('input[name=eveningWorkersCost]').val();//14

			var siteAllCost = $('input[name=siteAllCost]').val();//18

			var carCost = 0;//21
			var carCostList = $("input.carCost");
			for (var i = 0; i < carCostList.length; i++) {
				carCost = parseFloat(carCost)
						+ parseFloat(carCostList[i].value);
			}
			var taochanCost = 0;//24
			var taochanCostList = $("input.taochanCost");
			for (var i = 0; i < taochanCostList.length; i++) {
				taochanCost = parseFloat(taochanCost)
						+ parseFloat(taochanCostList[i].value);
			}

			var lipinCost = 0;//27
			var lipinCostList = $("input.lipinCost");
			for (var i = 0; i < lipinCostList.length; i++) {
				lipinCost = parseFloat(lipinCost)
						+ parseFloat(lipinCostList[i].value);
			}
			var insuranceCost = $('input[name=insuranceCost]').val();//29
			var serviceCost = $('input[name=serviceCost]').val();//30
			var lecturerCost = $('input[name=lecturerCost]').val();// 讲师费用
			var deliveryCost = $('input[name=deliveryCost]').val();//31
			var deliveryCarCost = $('input[name=deliveryCarCost]').val();//32
			var totalCost = parseFloat(groupMonkey) + parseFloat(shoukuanNum)
					- parseFloat(remainingCost)
					- parseFloat(eatCost)- parseFloat(stayCost)
					- parseFloat(eveningSiteCost)
					- parseFloat(eveningShowCost)
					- parseFloat(eveningShareCost)
					- parseFloat(eveningWorkersCost) 
					- parseFloat(siteAllCost)
					 - parseFloat(taochanCost)- parseFloat(lipinCost) 
					- parseFloat(carCost)- parseFloat(insuranceCost)
					- parseFloat(serviceCost)- parseFloat(lecturerCost)
					- parseFloat(deliveryCost)- parseFloat(deliveryCarCost);
			$('input[name=earning]').val(parseFloat(totalCost));
		}
		function isContains(str, substr) {
			return str.indexOf(substr) >= 0;
		}
		function checkError() {
			var text = $("#error").val();
			if (!isEmpty(text)) {
				alert(text);
			}

		}
		function CalSiteCost() {
			var siteCost = $('input[name="siteCost"]').val();
			var sharedCost = $('input[name="sharedCost"]').val();
			var siteWorkerCost = $('input[name="siteWorkerCost"]').val();
			if (!isEmpty(siteCost) && !isEmpty(sharedCost)
					&& !isEmpty(siteWorkerCost)) {
				$('input[name="siteAllCost"]').val(
						parseFloat(siteCost) + parseFloat(sharedCost)
								+ parseFloat(siteWorkerCost));
			}
		}
		function CalInsuranceCost(element) {
			var name1 = element.name;
			var peopleNum = $('input[name="peopleNum"]').val();
			if (isEmpty(peopleNum)) {
				alert("请填写人数");
				element.value = "";
			} else {
				var insuranceStandard = $('input[name="insuranceStandard"]')
						.val();
				if (!isEmpty(insuranceStandard)) {
					$('input[name="insuranceCost"]').val(
							parseFloat(peopleNum)
									* parseFloat(insuranceStandard));
				}
			}

		}
		function CalTuanFei() {
			var peopleNum = $('input[name="peopleNum"]').val();
			var groupMonkeyStandard = $('input[name="groupMonkeyStandard"]')
					.val();
			var remark = $('input[name="remark"]').val();
			if (!isEmpty(peopleNum) && !isEmpty(groupMonkeyStandard)
					&& !isEmpty(remark)) {
				$('input[name="groupMonkey"]').val(
						parseFloat(peopleNum * groupMonkeyStandard)
								+ parseFloat(remark));
			}
		}
		function CalEat(element) {
			var name1 = element.name;
			var peopleNum = $('input[name="peopleNum"]').val();
			if (isEmpty(peopleNum)) {
				alert("请填写人数");
				element.value = "";
			} else {
				if (isContains(name1, 'eatStandard')) {//标准input
					var eatStandard = element.value;
					if (!isEmpty(eatStandard)) {
						var name2 = name1.replace(/eatStandard/, "eatRemark");
						var eatRemark = $('input[name="' + name2 + '"]').val();
						var name3 = name1.replace(/eatStandard/, "eatCost");//
						if (!isEmpty(eatRemark)) {
							$('input[name="' + name3 + '"]').val(
									parseFloat(peopleNum)
											* parseFloat(eatStandard)
											+ parseFloat(eatRemark));
						} else {
							$('input[name="' + name3 + '"]').val(
									parseFloat(peopleNum)
											* parseFloat(eatStandard));
						}
					}

				} else {
					var eatRemark = element.value;
					var name2 = name1.replace(/eatRemark/, "eatStandard");
					var eatStandard = $('input[name="' + name2 + '"]').val();
					var name3 = name1.replace(/eatRemark/, "eatCost");//
					if (!isEmpty(eatRemark) && !isEmpty(eatStandard)) {
						$('input[name="' + name3 + '"]').val(
								parseFloat(peopleNum) * parseFloat(eatStandard)
										+ parseFloat(eatRemark));
					} else {
						if (isEmpty(eatRemark) && !isEmpty(eatStandard)) {
							$('input[name="' + name3 + '"]').val(
									parseFloat(peopleNum)
											* parseFloat(eatStandard));
						}
					}
				}
			}
		}
		function CalStay(element) {
			var name1 = element.name;
			var peopleNum = $('input[name="peopleNum"]').val();
			if (isEmpty(peopleNum)) {
				alert("请填写人数");
				element.value = "";
			} else {
				if (isContains(name1, 'stayStandard')) {//标准input
					var stayStandard = element.value;
					if (!isEmpty(stayStandard)) {
						var name2 = name1.replace(/stayStandard/, "stayRemark");
						var stayRemark = $('input[name="' + name2 + '"]').val();
						var name3 = name1.replace(/stayStandard/, "stayCost");//
						if (!isEmpty(stayRemark)) {
							$('input[name="' + name3 + '"]').val(
									parseFloat(peopleNum)
											* parseFloat(stayStandard)
											+ parseFloat(stayRemark));
						} else {
							$('input[name="' + name3 + '"]').val(
									parseFloat(peopleNum)
											* parseFloat(stayStandard));
						}
					}

				} else {
					var stayRemark = element.value;
					var name2 = name1.replace(/stayRemark/, "stayStandard");
					var stayStandard = $('input[name="' + name2 + '"]').val();
					var name3 = name1.replace(/stayRemark/, "stayCost");//
					if (!isEmpty(stayRemark) && !isEmpty(stayStandard)) {
						$('input[name="' + name3 + '"]').val(
								parseFloat(peopleNum)
										* parseFloat(stayStandard)
										+ parseFloat(stayRemark));
					} else if (isEmpty(stayRemark) && !isEmpty(stayStandard)) {
						$('input[name="' + name3 + '"]').val(
								parseFloat(peopleNum)
										* parseFloat(stayStandard));

					}
				}
			}
		}
	</script>


	<%@ include file="../common/foot.jsp"%>
</body>
</html>
