<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html lang="en">
<head>
<style>
* table th td{
	text-align:center;
}
.mytable{
text-align:center;
}
</style>
</head>
<body>
	<div id="wrap">
		<!-- Top menu -->
		<%@ include file="../common/top_menu.jsp"%>

		<div class="container-fluid">

			<!-- Side menu -->
			<%@ include file="../common/side_menu.jsp"%>

			<!-- Main window -->
			<div class="main_container" id="users_page">

				<div class="row-fluid">
					<ul class="breadcrumb">
						<li>会员管理系统<span class="divider">/</span></li>
						<li>活动管理<span class="divider">/</span></li>
						<li class="active">出团信息列表</li>
					</ul>
					<h2 class="heading">活动管理</h2>
				</div>
				<!-- /row-fluid -->

				<div class="row-fluid">
					<div class="widget widget-padding span12">
						<div class="widget-header">
							<i class="icon-group"></i>
							<h5>出团信息列表</h5>

						</div>
						<div class="widget-body">
							<table id="users"
								class="table table-striped table-bordered dataTable">

								<thead>
									<tr>
										<th>序号</th>
										<th>活动编号</th>
										<th>业务编号</th>
										<th>地点</th>
										<th>讲师</th>
										<th>场务</th>
										<th>创建时间</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${page.list}" var="list" varStatus="vs">
										<tr>
											<td>${vs.index+1 }</td>
											<td>${list.activityId}</td>
											<td>${list.id}</td>
											<td>${list.position}</td>
											<td>${list.lecturer}</td>
											<td>${list.workers}</td>
											<td><fmt:formatDate value="${list.createTime}"
													type="date" pattern="yyyy-MM-dd" /></td>
											<td>
												<button class="btn btn-success" data-toggle="modal"
													data-target="#myModal" onclick="initModal('${list.id}')">查看购买情况</button>
														<form
													action="${pageContext.request.contextPath}/activity/findYeWuById?id=${list.id}"
													method="post">
													<button class="btn btn-info">修改业务活动</button>
											</form>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
							<!-- 分页 -->
							<c:if test="${page.pages>0}">
								<jsp:include page="../common/pager.jsp">
									<jsp:param value="c_id" name="paramKey" />
									<jsp:param value="${c_id}" name="paramVal" />
								</jsp:include>
							</c:if>
						</div>
						<!-- /widget-body -->
					</div>
					<!-- /widget -->
				</div>
				<!-- /row-fluid -->

			</div>
			<!-- /Main window -->

		</div>
		<!--/.fluid-container-->
	</div>
	<!-- wrap ends-->


	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1"
		style="display: none;" role="dialog" aria-labelledby="myModalLabel"
		aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only"></span>
					</button>
					<h4 class="modal-title" id="myModalLabel">套餐购买情况</h4>
				</div>
				<table>
					<tr>
						<th>姓名</th>
						<th>电话</th>
						<th>业务经理</th>
						<th>地址</th>
						<th>定金</th>
						<th>预付款</th>
						<th>尾款</th>
						<th>已付</th>
						<th>支付类别</th>
						<th>状态</th>
					</tr>
					<tbody id="shoppingList" class="mytable">
					</tbody>
				</table>
			</div>
		</div>
	</div>
	</div>

	<script>
		function initModal(id) {
			var args = {
				"id" : id
			};
					$
							.post(
									"${pageContext.request.contextPath}/activity/findShoppingListByYeWuId",
									args,
									function(data) {
										$("#shoppingList").empty();
										for (i = 0; i < data.length; i++) {
											$("#shoppingList")
											.append("<tr>");
											$("#shoppingList")
													.append(
															  "<td>"
																	+ data[i].userName
																	+ "</td>"
																	+ "<td>"
																	+ data[i].userPhone
																	+ "</td>"
																	+ "<td>"
																	+ data[i].managerName
																	+ "</td>"
																	+ "<td>"
																	+ data[i].address
																	+ "</td>"
																	+ "<td>"
																	+ data[i].deposit
																	+ "</td>"
																	+ "<td>"
																	+ data[i].advance
																	+ "</td>"
																	+ "<td>"
																	+ data[i].weikuan
																	+ "</td>"
																	+ "<td>"
																	+ data[i].paid
																	+ "</td>"
																	+ "<td>"
																	+ data[i].payType
																	+ "</td>");
											if (data[i].state == 0) {
												$("#shoppingList")
														.append(
																"<td>正常</td>");
											}
											if (data[i].state == 1) {
												$("#shoppingList").append(
														"<td>已退货</td>");
											}
											$("#shoppingList").append("</tr>");

										}

									}), "JSON";
		}
	</script>

	<%@ include file="../common/foot.jsp"%>
</body>
</html>
