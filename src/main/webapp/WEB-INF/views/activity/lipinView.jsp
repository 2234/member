<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<script
	src="http://static.runoob.com/assets/jquery-validation-1.14.0/lib/jquery.js"></script>
<script
	src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script>
<script
	src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/localization/messages_zh.js"></script>
<script>
	$().ready(function() {
		$("#commentForm").validate();
	});
</script>
<style>
.error {
	color: red;
}

* table tr td {
	text-align: center;
}
</style>
</head>
<body>

	</div>
	<div id="wrap">
		<!-- Top menu -->
		<%@ include file="../common/top_menu.jsp"%>

		<div class="container-fluid">

			<!-- Side menu -->
			<%@ include file="../common/side_menu.jsp"%>

			<!-- Main window -->
			<div class="main_container" id="dashboard_page">

				<div class="row-fluid">
					<ul class="breadcrumb">
						<li>会员管理系统 <span class="divider">/</span></li>
						<li>活动管理 <span class="divider">/</span></li>
						<li class="active">礼品修改</li>
					</ul>
					<h2 class="heading">活动管理</h2>
				</div>

				<div class="row-fluid">
					<div class="widget-header">
						<h5>礼品修改</h5>
						<span>${ERROR}</span>
					</div>

					<div class="widget-body" style="overflow: auto;">
						<!-- 					style="display:none;" -->
						<div id="peihuoContent"></div>
						<table class="table ">
							<!-- 							<tr> -->
							<!-- 								<td style="font-weight: bold; font-size: 14px;">礼品清单</td> -->
							<!-- 								<td></td> -->
							<!-- 								<td></td> -->
							<!-- 								<td></td> -->
							<!-- 								<td></td> -->
							<!-- 							</tr> -->
							<tr>
								<th>商品名称</th>
								<th>商品规格</th>
								<th>商品价格</th>
								<th>商品数量</th>
								<th>商品价值</th>
								<th>操作
									<button onclick="addLiPin(${ActivityId})">新增</button>
								</th>
							</tr>


							<c:forEach items="${Data}" var="list" varStatus="vs">
								<tr>
									<td>${list.goodsName}</td>
									<td>${list.goodsSpec}</td>
									<td>${list.goodsPrice}</td>
									<td>${list.goodsCount}</td>
									<td>${list.goodsCost}</td>
									<td>
										<form
											action="${pageContext.request.contextPath}/activity/deteleLiPin?id=${list.id}&activityId=${ActivityId}"
											method="post">
											<button class="btn btn-defalut">删除</button>
										</form>
									</td>
								</tr>
							</c:forEach>
						</table>
						<div id="lipinlist">
						</div>



					</div>
				</div>



			</div>
			<!-- /widget span5 -->
		</div>
		<!-- /row-fluid -->

	</div>

	<!-- wrap ends-->
	<script type="text/javascript">
	function addLiPin(activityId) {
		$("#lipinlist")
				.append(	
						"<form action='${pageContext.request.contextPath}/activity/insertLiPin' id='commentForm'  method='post'>"
						+"<div class='form-control'>"
						+"<label>商品名称</label>"
								+ "<input type='hidden' class='form-control' name='type' value='1' required />"
								+ "<input type='text' class='form-control' name='goodsName' required onchange='checkGoods(this)'/>"
								+ "</div>"
								+"<div class='form-control'>"
								+"<label>商品规格</label>"
								+ "<select name='goodsSpec' class='form-control' required='required' onChange='inputGoodsPrice(this)'>"
								+ "</select>"
								+ "</div>"
								+"<div class='form-control'>"
								+"<label>商品价格</label>"
								+ "<input type='text' class='form-control' name='goodsPrice' number='true' required  onChange='calGoodsCost(this)'/>"
								+ "</div>"
								+"<div class='form-control'>"
								+"<label>商品数量</label>"
								+ "<input type='text' class='form-control' name='goodsCount' digits='true' required onChange='calGoodsCost(this)'/>"
								+ "</div>"
								+"<div class='form-control'>"
								+"<label>商品费用</label>"
								+ "<input type='text' class='form-control lipinCost' name='goodsCost' number='true' required />"
								+ "</div>"
								+"<div class='form-control'>"
								+"<label>操作</label>"
								+ "<input type='hidden' class='form-control' name='activityId' value='"
								+activityId
								+"' required />"
								+ "<button class='btn btn-success'>提交</button>"
								+ "<button  onclick='deleteTr(this)' class='btn btn-defalut'>移除</button>"
								+ "</div>" 
								+ "</form>"	
									
				);
	}
	function checkGoods(e) {
		var goodsName = e.value;
		var name1 = e.name;
		var name2 = name1.replace(/goodsName/, "goodsSpec");
		var name3 = name1.replace(/goodsName/, "goodsPrice");
		var goodsSpec = $('select[name="' + name2 + '"]');
		var goodsPrice = $('input[name="' + name3 + '"]');
		if (!isEmpty(goodsName)) {
			var args = {
				"goodsName" : goodsName
			};
					$
							.post(
									"${pageContext.request.contextPath}/goods/checkGoods",
									args,
									function(data) {
										if (data.Successful) {
											var num = 0;
											goodsSpec.empty();
											for (; num < data.Data.length; num++) {
												goodsSpec
														.append("<option value='"+data.Data[num].spec+"' price='"+data.Data[num].retail_price+"'>"
																+ data.Data[num].spec
																+ "</option>");
											}
											goodsPrice
													.val(data.Data[0].retail_price);

										} else {
											alert('商品名：' + goodsName
													+ ',填写有误，商品不存在!');
										}
									}), "JSON";
		}
	}
	function inputGoodsPrice(e) {
		var price = $('select[name="' + e.name + '"]').find(
				"option:selected").attr("price");

		var name1 = e.name;

		var name2 = name1.replace(/goodsSpec/, "goodsPrice");
		var name3 = name1.replace(/goodsSpec/, "goodsCount");
		$('input[name="' + name2 + '"]').val(price);
		var goodsCount = $('input[name="' + name3 + '"]').val();
		if (!isEmpty(goodsCount)) {
			var name4 = name1.replace(/goodsSpec/, "goodsCost");
			$('input[name="' + name4 + '"]').val(
					parseFloat(goodsCount) * parseFloat(price));
		}

	}
	function deleteTr(e) {
		e.parentElement.parentElement.remove();
	}
	function calGoodsCost(e) {
		var name1 = e.name;
		if (isContains(name1, 'goodsPrice')) {
			var goodsPrice = e.value;
			var name2 = name1.replace(/goodsPrice/, "goodsCount");
			var goodsCount = $('input[name="' + name2 + '"]').val();
			var name3 = name1.replace(/goodsPrice/, "goodsCost");
		} else {
			var goodsCount = e.value;
			var name2 = name1.replace(/goodsCount/, "goodsPrice");
			var goodsPrice = $('input[name="' + name2 + '"]').val();
			var name3 = name1.replace(/goodsCount/, "goodsCost");
		}
		if (!isEmpty(goodsCount) && !isEmpty(goodsPrice)) {
			$('input[name="' + name3 + '"]').val(
					parseFloat(goodsCount) * parseFloat(goodsPrice));
		}
	}
	function isContains(str, substr) {
		return str.indexOf(substr) >= 0;
	}

			</script>


	<%@ include file="../common/foot.jsp"%>
</body>
</html>
