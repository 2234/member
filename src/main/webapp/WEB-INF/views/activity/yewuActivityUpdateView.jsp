<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<script
	src="http://static.runoob.com/assets/jquery-validation-1.14.0/lib/jquery.js"></script>
<script
	src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script>
<script
	src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/localization/messages_zh.js"></script>
<script>
	$().ready(function() {
		$("#commentForm").validate();
	});
</script>
<style>
.error {
	color: red;
}

* table tr td {
	text-align: center;
}
</style>
</head>
<body>

	</div>
	<div id="wrap">
		<!-- Top menu -->
		<%@ include file="../common/top_menu.jsp"%>

		<div class="container-fluid">

			<!-- Side menu -->
			<%@ include file="../common/side_menu.jsp"%>

			<!-- Main window -->
			<div class="main_container" id="dashboard_page">

				<div class="row-fluid">
					<ul class="breadcrumb">
						<li>会员管理系统 <span class="divider">/</span></li>
						<li>活动管理 <span class="divider">/</span></li>
						<li class="active">新增业务场信息</li>
					</ul>
					<h2 class="heading">活动管理</h2>
				</div>

				<div class="row-fluid">
					<div class="widget-header">
						<i class="icon-plus"></i>
						<h5>新增业务场信息</h5>
						<span>${ERROR}</span>
					</div>

					<div class="widget-body" style="overflow: auto;">
						<!-- 					style="display:none;" -->
						<div id="peihuoContent"></div>
						<form method="POST" id="commentForm"
							action="${pageContext.request.contextPath}/activity/UpdateYeWu">
							<label>活动基本信息：</label>
							<table class="table ">
								<tr>
									<td>出团编号</td>
									<td>
									<input type="hidden" class="form-control" 
										value="${Data.yewu.id}" placeholder="" name="id"
										 required />
									<input type="text" class="form-control"
										value="${Data.yewu.activityId}" id="activityId" placeholder=""
										name="activityId" required readonly="readonly" /></td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<td style="font-weight: bold; font-size: 14px;">套餐清单</td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<th>商品名称</th>
									<th>商品规格</th>
									<th>商品价格</th>
									<th>商品数量</th>
								</tr>
								<c:forEach items="${TaoCan}" var="list" varStatus="vs">
									<tr>
										<td>${list.goodsName}</td>
										<td>${list.goodsSpec}</td>
										<td>${list.goodsPrice}</td>
										<td>${list.goodsCount}</td>
									</tr>
								</c:forEach>
								<tr>
									<td>业务地点:</td>
									<td><input type="text" class="form-control" id="position"
										value="${Data.yewu.position}" placeholder="" name="position"
										minlength="2" required /></td>
									<td>讲师:</td>
									<td><input type="text" class="form-control" id="lecturer"
										value="${Data.yewu.lecturer}" placeholder="" name="lecturer"
										minlength="2" required /></td>
								</tr>
								<tr>
									<td>工作人员</td>
									<td><textarea type="text" class="form-control" cols="200"
											rows="200" id="workers" placeholder="" name="workers"
											required>${Data.yewu.workers}</textarea></td>
									<td></td>
									<td></td>
								</tr>
							</table>


							<div class="form-group" style="margin-left: 5px;">
								<label>业务表格：</label>
								<div>
									<table class="table table-condensed">
										<tr>
											<th>姓名</th>
											<th>电话</th>
											<th>业务经理</th>
											<th>地区</th>
											<th>定金</th>
											<th>预付款</th>
											<th>尾款</th>
											<th>已付</th>
											<th>已付类别</th>
											<th>配货情况</th>
										</tr>
										<c:forEach items="${Data.shopList}" var="list" varStatus="vs">
											<tr>
												<input type="hidden" name="shopList[${vs.index}].id"
													value="${list.id}" required />
												<input type="hidden" name="shopList[${vs.index}].yewuId"
													value="${list.yewuId}" required />
												<input type="hidden" name="shopList[${vs.index}].state"
													value="${list.state}" required />
												<td><input type="text" class="form-control"
													name="shopList[${vs.index}].userName"
													value="${list.userName}" required /></td>
												<td><input type="text" class="form-control"
													name="shopList[${vs.index}].userPhone"
													value="${list.userPhone}" required /></td>
												<td><input type="text" class="form-control"
													name="shopList[${vs.index}].managerName"
													value="${list.managerName}" required /></td>
												<td><input type="text" class="form-control"
													name="shopList[${vs.index}].address"
													value="${list.address}" required /></td>
												<td><input type="text" class="form-control"
													name="shopList[${vs.index}].deposit" required
													onchange="calPay(this)" value="${list.deposit}" /></td>
												<td><input type="text" class="form-control"
													name="shopList[${vs.index}].advance" required
													onchange="calPay(this)" value="${list.advance}" /></td>
												<td><input type="text" class="form-control"
													name="shopList[${vs.index}].weikuan" required
													onchange="calPay(this)" value="${list.weikuan}" /></td>
												<td><input type="text" class="form-control"
													name="shopList[${vs.index}].paid" required
													value="${list.paid}" /></td>

												<td><c:if test='${list.payType=="预付款"}'>
														<input type="radio" class="form-control"
															name="shopList[${vs.index}].payType" value="预付款" checked
															required>
														<span>预付款</span>
														<input type="radio" name="shopList[${vs.index}].payType"
															value="全款" required>
														<span>全款</span></td>
												</c:if>
												<c:if test='${list.payType=="全款"}'>
													<input type="radio" class="form-control"
														name="shopList[${vs.index}].payType" value="预付款" required>
													<span>预付款</span>
													<input type="radio" name="shopList[${vs.index}].payType"
														value="全款" checked required>
													<span>全款</span>
												</c:if>
												</td>
												<td><c:forEach items="${list.pickingList}"
														var="pickinglist" varStatus="va">
														<input type="hidden"
															name="shopList[${vs.index}].pickingList[${va.index}].id"
															value="${pickinglist.id}" required />
			<input type="hidden" value="${pickinglist.shoppingId}" name="shopList[${vs.index}].pickingList[${va.index}].shoppingId"
												>
		商品名:	<input type="text" class="form-control"
															value="${pickinglist.goodsName}"
															name="shopList[${vs.index}].pickingList[${va.index}].goodsName"
															readonly="readonly">
		规格:	<input type="text" class="form-control"
															value="${pickinglist.goodsSpec}"
															name="shopList[${vs.index}].pickingList[${va.index}].goodsSpec"
															readonly="readonly">
		数量:	<input type="text" class="form-control"
															value="${pickinglist.goodsCount}"
															name="shopList[${vs.index}].pickingList[${va.index}].goodsCount"
															readonly="readonly">
														<c:if test='${pickinglist.state==0}'>
															<input type="radio" class="form-control"
																name="shopList[${vs.index}].pickingList[${va.index}].state"
																value="1">已配货
														<input type="radio" class="form-control"
																name="shopList[${vs.index}].pickingList[${va.index}].state"
																checked="checked" value="0">未配货<br />
														</c:if>
														<c:if test='${pickinglist.state==1}'>
															<input type="radio" class="form-control"
																name="shopList[${vs.index}].pickingList[${va.index}].state"
																value="1" checked="checked">已配货
														<input type="radio" class="form-control"
																name="shopList[${vs.index}].pickingList[${va.index}].state"
																value="0">未配货<br />
														</c:if>
													</c:forEach></td>
											</tr>
										</c:forEach>

									</table>
								</div>
							</div>


							<div class="form-group"
								style="float: right; marign-right: 5px; marign-bottom: 5px;">
								<input type="submit" class="btn btn-success" value="提交" />
							</div>
						</form>

					</div>
					<!-- /widget span5 -->
				</div>
				<!-- /row-fluid -->

			</div>

			<!-- wrap ends-->
			<script type="text/javascript">
				function calPay(e) {
					var name1 = e.name;
					if (isContains(name1, 'deposit')) {
						var depositName = name1;
						var advanceName = name1.replace(/deposit/,
								"advanceName");
						var weikuanName = name1.replace(/deposit/, "weikuan");
						var paidName = name1.replace(/deposit/, "paid");
					} else if (isContains(name1, 'advance')) {
						var advanceName = name1;
						var depositName = name1.replace(/advance/, "deposit");
						var weikuanName = name1.replace(/advance/, "weikuan");
						var paidName = name1.replace(/advance/, "paid");

					} else {
						var weikuanName = name1;
						var depositName = name1.replace(/weikuan/, "deposit");
						var advanceName = name1.replace(/weikuan/, "advance");
						var paidName = name1.replace(/weikuan/, "paid");
					}
					var weikuan = $('input[name="' + weikuanName + '"]').val();
					var deposit = $('input[name="' + depositName + '"]').val();
					var advance = $('input[name="' + advanceName + '"]').val();
					if (!isEmpty(weikuan) && !isEmpty(deposit)
							&& !isEmpty(advance)) {
						var paid = $('input[name="' + paidName + '"]').val(
								parseFloat(weikuan) + parseFloat(deposit)
										+ parseFloat(advance));
					}

				}
				function isContains(str, substr) {
					return str.indexOf(substr) >= 0;
				}
			</script>


			<%@ include file="../common/foot.jsp"%>
</body>
</html>
