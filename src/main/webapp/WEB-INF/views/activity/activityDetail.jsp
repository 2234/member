<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<style>
* table tr td {
	text-align: center;
}
.mytitle{
font-weight:bold;
}
*{
font-size: 16px;
}
</style>

</head>
<body>
	<div id="wrap">
		<%@ include file="../common/top_menu.jsp"%>

		<div class="container-fluid">

			<!-- Side menu -->
			<%@ include file="../common/side_menu.jsp"%>

			<!-- Main window -->
			<div class="main_container" id="dashboard_page"
				style="overflow: auto">

				<div class="row-fluid">
					<ul class="breadcrumb">
						<li>会员管理系统 <span class="divider">/</span></li>
						<li>活动管理 <span class="divider">/</span></li>
						<li class="active">活动详情</li>
					</ul>
				</div>
				<div>
					<table border="1">
					<tr><td class="mytitle">活动编号</td><td>${Data.tuan.id}</td></tr>
						<tr>
							<th>开始时间</th>
							<th>结束时间</th>
							<th>共计（天）</th>
						</tr>
						<tr>
							<td><fmt:formatDate value="${Data.tuan.startTime}" type="date" pattern="yyyy-MM-dd"/> </td>
							<td><fmt:formatDate value="${Data.tuan.endTime}" type="date" pattern="yyyy-MM-dd"/></td>
							<td>${Data.tuan.dayNum}</td>
						</tr>
						<tr>
							<th>路线起点</th>
							<th>路线终点</th>
						</tr>
						<tr>
							<td>${Data.tuan.loadStart}</td>
							<td>${Data.tuan.loadEnd}</td>
						</tr>
						<tr>
							<th>人数</th>
							<th>业务经理</th>
						</tr>
						<tr>
							<td>${Data.tuan.peopleNum}</td>
							<td>${Data.tuan.managerName}</td>
						</tr>
						<tr>
							<th>团费标准</th>
							<th>特殊说明</th>
						</tr>
						<tr>
							<td>${Data.tuan.groupMonkeyStandard}</td>
							<td>${Data.tuan.remark}</td>
						</tr>
						<tr>
							<td>团费收入：</td>
							<td>${Data.tuan.groupMonkey}</td>
						</tr>
						<tr>
							<th>餐费第几天</th>
							<th>地点</th>
							<th>标准</th>
							<th>备注</th>
							<th>费用小计</th>
						</tr>
						<c:forEach items="${Data.eatList}" var="list">
							<tr>
								<td>${list.dayNum}</td>
								<td>${list.eatPosition}</td>
								<td>${list.eatStandard}</td>
								<td>${list.eatRemark}</td>
								<td>${list.eatCost}</td>
							</tr>
						</c:forEach>
						<tr>
							<th>住宿第几天</th>
							<th>地点</th>
							<th>标准</th>
							<th>备注</th>
							<th>费用小计</th>
						</tr>
						<c:forEach items="${Data.stayList}" var="list">
							<tr>
								<td>${list.dayNum}</td>
								<td>${list.stayPosition}</td>
								<td>${list.stayStandard}</td>
								<td>${list.stayRemark}</td>
								<td>${list.stayCost}</td>
							</tr>
						</c:forEach>
						<tr>
							<th>晚会场地费</th>
							<th>晚会演出费</th>
							<th>晚会公摊费</th>
							<th>晚会工作人员费用</th>
						</tr>

						<tr>
							<td>${Data.tuan.eveningSiteCost}</td>
							<td>${Data.tuan.eveningShowCost}</td>
							<td>${Data.tuan.eveningShareCost}</td>
							<td>${Data.tuan.eveningWorkersCost}</td>
						</tr>
						<tr>
							<th>场地费</th>
							<th>会场公摊费用</th>
							<th>会务（工作人员）费用</th>
							<th>费用小计</th>
						</tr>
						<tr>
							<td>${Data.tuan.siteCost}</td>
							<td>${Data.tuan.sharedCost}</td>
							<td>${Data.tuan.siteWorkerCost}</td>
							<td>${Data.tuan.siteAllCost}</td>
						</tr>
						<tr>
							<th>礼品名称</th>
							<th>礼品规格</th>
							<th>礼品价值</th>
							<th>礼品数量</th>
							<th>费用小计</th>
						</tr>
						<tr>
								<c:forEach items="${Data.lipinList}" var="list">
							<tr>
								<td>${list.goodsName}</td>
								<td>${list.goodsSpec}</td>
								<td>${list.goodsPrice}</td>
								<td>${list.goodsCount}</td>
								<td>${list.goodsCost}</td>
							</tr>
						</c:forEach>

						</tr>

						<tr>
							<th>商品名称</th>
							<th>商品规格</th>
							<th>商品价格</th>
							<th>商品数量</th>
							<th>费用小计</th>
						</tr>
									<c:forEach items="${Data.taochanList}" var="list">
							<tr>
								<td>${list.goodsName}</td>
								<td>${list.goodsSpec}</td>
								<td>${list.goodsPrice}</td>
								<td>${list.goodsCount}</td>
								<td>${list.goodsCost}</td>
							</tr>
						</c:forEach>

						<tr>
							<th>车型</th>
							<th>座位数</th>
							<th>数量</th>
							<th>标准</th>
							<th>费用</th>
						</tr>

						<tr>
								<c:forEach items="${Data.carList}" var="list">
							<tr>
								<td>${list.carModel}</td>
								<td>${list.carSite}</td>
								<td>${list.carNum}</td>
								<td>${list.carStandard}</td>
								<td>${list.carCost}</td>
							</tr>
						</c:forEach>
						</tr>
						<tr>
							<th>保险标准</th>
							<th>保险费用</th>
						</tr>
						<tr>
							<td>${Data.tuan.insuranceStandard}</td>
							<td>${Data.tuan.insuranceCost}</td>
						</tr>
						<tr>
							<td class="mytitle">手续费用</td>
							<td class="mytitle">讲师费用</td>
						</tr>
						<tr>
							<td>${Data.tuan.serviceCost}</td>
							<td>${Data.tuan.lecturerCost}</td>
						</tr>
						<tr>
							<th>送货开支</th>
							<th>送货车费</th>
						</tr>
						<tr>
							<td>${Data.tuan.deliveryCost}</td>
							<td>${Data.tuan.carCost}</td>
						</tr>
						<tr>
							<td class="mytitle">订金数量:</td>
							<td>${Data.tuan.dinghuoNum}</td>
						</tr>
						<tr>
							<td class="mytitle">已收款数:</td>
							<td>${Data.tuan.yshouhuokuanNum}</td>
						</tr>
						<tr>
							<td class="mytitle">未收款数:</td>
							<td>${Data.tuan.wshouhuokuanNum}</td>
						</tr>
						<tr>
							<td class="mytitle">已退款:</td>
							<td>${Data.tuan.tuihuokuanNum}</td>
						</tr>
						<tr>
							<td class="mytitle">收款合计:</td>
							<td>${Data.tuan.shoukuanNum}</td>
						</tr>
						<tr>
							<td class="mytitle">发货:</td>
							<td>${Data.tuan.sendNum}</td>
						</tr>
						<tr>
							<td class="mytitle">开支剩余:</td>
							<td>${Data.tuan.remainingCost}</td>
						</tr>
						<tr>
							<td class="mytitle">盈亏情况：</td><td>${Data.tuan.earning}</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	</div>



	<%@ include file="../common/foot.jsp"%>
</body>
</html>
