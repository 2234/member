<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<script
	src="http://static.runoob.com/assets/jquery-validation-1.14.0/lib/jquery.js"></script>
<script
	src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script>
<script
	src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/localization/messages_zh.js"></script>
<script>
	$().ready(function() {
		$("#commentForm").validate();
	});
</script>
<style>
.error {
	color: red;
}
</style>
</head>
<body>
	<div id="wrap">
		<!-- Top menu -->
		<%@ include file="../common/top_menu.jsp"%>

		<div class="container-fluid">

			<!-- Side menu -->
			<%@ include file="../common/side_menu.jsp"%>

			<!-- Main window -->
			<div class="main_container" id="dashboard_page">

				<div class="row-fluid">
					<ul class="breadcrumb">
						<li> 会员管理系统 <span class="divider">/</span></li>
						<li> 活动管理 <span class="divider">/</span></li>
						<li class="active">新增业务场信息</li>
					</ul>
					<h2 class="heading">活动管理</h2>
				</div>

				<div class="row-fluid">
					<div class="widget-header">
						<i class="icon-plus"></i>
						<h5>新增业务场信息</h5>
						<span>${ERROR}</span>
					</div>

					<div class="widget-body" style="overflow: hidden;">
						<form method="POST" id="commentForm"
							action="${pageContext.request.contextPath}/activity/InsertBusinessActivity">
							<div class="form-group" style="margin-left: 5px;">
								<label>活动基本信息：</label>
								<table>
									<tr>
										<th>活动编号</th>
										<th>活动第几天</th>
									</tr>
									<tr>
										<td><input type="text" class="form-control"
											id="activityId" placeholder="" name="activityId" required /></td>
										<td><input type="text" class="form-control" id="dayNum"
											placeholder="" name="dayNum" required /></td>
									</tr>
								</table>
								<div class="form-group" style="margin-left: 5px;">
									<label>讲师：</label> <input type="text" class="form-control"
										id="lecturer" placeholder="" name="lecturer" minlength="2"
										required />
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>场务：</label>
									<table>
										<tr>
											<th>场务人员</th>
											<th>场务费</th>
										</tr>
										<tr>
											<td><input type="text" class="form-control" id="workers"
												placeholder="" name="workers" required /></td>
											<td><input type="text" class="form-control"
												id="workersCost" placeholder="" name="workersCost" required /></td>
										</tr>
									</table>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>公摊：</label>
									<table>
										<tr>
											<th>公摊标准</th>
											<th>公摊费用</th>
										</tr>
										<tr>
											<td><input type="text" class="form-control" id="shared"
												placeholder="" name="shared" /></td>
											<td><input type="text" class="form-control"
												id="sharedCost" placeholder="" name="sharedCost" /></td>
										</tr>
									</table>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>保险：</label>
									<table>
										<tr>
											<th>保险标准</th>
											<th>保险费用</th>
										</tr>
										<tr>
											<td><input type="text" class="form-control"
												id="insuranceStandard" placeholder=""
												name="insuranceStandard" required /></td>
											<td><input type="text" class="form-control"
												id="insuranceCost" placeholder="" name="insuranceCost"
												required /></td>
										</tr>
									</table>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>服务费用：</label> <input type="text" class="form-control"
										id="serviceCost" placeholder="" name="serviceCost" required />
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>礼品：</label>
									<div>
										<table id="lipinlist">
											<tr>
												<th>礼品名称</th>
												<th>礼品规格</th>
												<th>礼品数量</th>
												<th>礼品价值</th>
												<th>费用小计</th>
												<th><button onclick="addLiPin()">增加</button></th>
											</tr>
											<tr>
												<td><input type="hidden" class="form-control"
													name="lipinList[0].type" value='1' required /> <input
													type="text" class="form-control"
													name="lipinList[0].goodsName" required /></td>
												<td><input type="text" class="form-control"
													name="lipinList[0].goodsSpec" required /></td>
												<td><input type="text" class="form-control"
													name="lipinList[0].goodsCount" required /></td>
												<td><input type="text" class="form-control"
													name="lipinList[0].goodsPrice" required /></td>
												<td><input type="text" class="form-control"
													name="lipinList[0].goodsCost" required /></td>

											</tr>

										</table>
									</div>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>套餐：</label>
									<div>
										<table id="taochanlist">
											<tr>
												<th>商品名称</th>
												<th>商品规格</th>
												<th>商品数量</th>
												<th>商品价格</th>
												<th>费用小计</th>
												<th><button onclick="addTaochan()">增加</button></th>
											</tr>
											<tr>
												<td><input type="hidden" class="form-control"
													name="taochanList[0].type" value='2' required /> <input
													type="text" class="form-control"
													name="taochanList[0].goodsName" required /></td>
												<td><input type="text" class="form-control"
													name="taochanList[0].goodsSpec" required /></td>
												<td><input type="text" class="form-control"
													name="taochanList[0].goodsCount" required /></td>
												<td><input type="text" class="form-control"
													name="taochanList[0].goodsPrice" required /></td>
												<td><input type="text" class="form-control"
													name="taochanList[0].goodsCost" required /></td>
											</tr>

										</table>
									</div>
								</div>


								<div class="form-group"
									style="float: right; marign-right: 5px; marign-bottom: 5px;">
									<input type="submit" class="btn btn-success" value="提交" />
								</div>
						</form>

					</div>
					<!-- /widget span5 -->
				</div>
				<!-- /row-fluid -->

			</div>
			<!-- wrap ends-->
			<script type="text/javascript">
			function addLiPin() {
				var lth = $('#lipinlist').children().children().length - 1;
				$("#lipinlist")
						.append("<tr>"
										+ "<td>"
										+ "<input type='hidden' class='form-control' name='lipinList["+lth+"].type' value='1' required />"
										+ "<input type='text' class='form-control' name='lipinList["+lth+"].goodsName' required />"
										+ "</td>"
										+ "<td>"
										+ "<input type='text' class='form-control' name='lipinList["+lth+"].goodsSpec' required />"
										+ "</td>"
										+ "<td>"
										+ "<input type='text' class='form-control' name='lipinList["+lth+"].goodsCount' required />"
										+ "</td>"
										+ "<td>"
										+ "<input type='text' class='form-control' name='lipinList["+lth+"].goodsPrice' required />"
										+ "</td>"
										+ "<td>"
										+ "<input type='text' class='form-control' name='lipinList["+lth+"].goodsCost' required />"
										+ "</td>"
										+ "<td>"
										+ "<button  onclick='deleteTr(this)'>移除</button>"
										+ "</td>" + "</tr>");
			}
			function addTaochan() {
				var lth = $('#taochanlist').children().children().length - 1;
				$("#taochanlist")
				.append("<tr>"
						+ "<td>"
						+ "<input type='hidden' class='form-control' name='taochanList["+lth+"].type' value='2' required />"
						+ "<input type='text' class='form-control' name='taochanList["+lth+"].goodsName' required />"
						+ "</td>"
						+ "<td>"
						+ "<input type='text' class='form-control' name='taochanList["+lth+"].goodsSpec' required />"
						+ "</td>"
						+ "<td>"
						+ "<input type='text' class='form-control' name='taochanList["+lth+"].goodsCount' required />"
						+ "</td>"
						+ "<td>"
						+ "<input type='text' class='form-control' name='taochanList["+lth+"].goodsPrice' required />"
						+ "</td>"
						+ "<td>"
						+ "<input type='text' class='form-control' name='taochanList["+lth+"].goodsCost' required />"
						+ "</td>"
						+ "<td>"
						+ "<button onclick='deleteTr(this)'>移除</button>"
						+ "</td>" + "</tr>");
			}
		
			function deleteTr(e) {
				e.parentElement.parentElement.remove();
			}
		</script>


			<%@ include file="../common/foot.jsp"%>
</body>
</html>
