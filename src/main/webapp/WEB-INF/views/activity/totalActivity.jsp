<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/lib/jquery.js"></script>
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script>
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/localization/messages_zh.js"></script>
<script>
$().ready(function() {
    $("#commentForm").validate();
});
</script>
<style>
.error{
	color:red;
}
</style>
</head>
<body>
	<div id="wrap">
		<!-- Top menu -->
		<%@ include file="../common/top_menu.jsp"%>

		<div class="container-fluid">

			<!-- Side menu -->
			<%@ include file="../common/side_menu.jsp"%>

			<!-- Main window -->
			<div class="main_container" id="dashboard_page">

				<div class="row-fluid">
					<ul class="breadcrumb">
						<li>会员管理系统 <span class="divider">/</span></li>
						<li>活动管理 <span class="divider">/</span></li>
						<li class="active">新增活动基本信息</li>
					</ul>
					<h2 class="heading">活动管理</h2>
				</div>

				<div class="row-fluid">
					<div class="widget span12">
						<div class="widget-header">
							<i class="icon-plus"></i>
							<h5>新增活动基本信息</h5>
							<span>${ERROR}</span>
						</div>

						<div class="widget-body" style="overflow: hidden;">
							<form method="POST" id="commentForm" action="${pageContext.request.contextPath}/activity/InsertTotalActivity">
								<div class="form-group" style="margin-left: 5px;">
									<label>开始时间：</label> <input type="date" class="form-control"
										id="startTime" placeholder="请以yyyy-MM-dd格式输入" name="startTime"  required />
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>结束时间：</label> <input type="date" class="form-control"
										id="endTime" placeholder="请以yyyy-MM-dd格式输入" name="endTime" required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>天数：</label> <input type="text" class="form-control"
										id="dayNum" placeholder="" name="dayNum"  required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>人数：</label> <input type="text" class="form-control"
										id="peopleNum" placeholder="" name="peopleNum" required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>业务经理：</label> <input type="text" class="form-control"
										id="managerName" placeholder="" name="managerName" required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>额外说明：</label> <input type="text" class="form-control"
										id="remark" placeholder="" name="remark" required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>路线：</label>
									<label>起点</label> <input type="text" class="form-control"
										id="loadStart" placeholder="" name="loadStart" required/>
									<label>终点</label> <input type="text" class="form-control"
										id="loadEnd" placeholder="" name="loadEnd" required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>套餐价：</label> <input type="text" class="form-control"
										id="groupPrice" placeholder="" name="groupPrice" required/>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>定金：</label> <input type="text" class="form-control"
										id="deposit" placeholder="" name="deposit" required/>
								</div>

								<div class="form-group" style="margin-left: 5px;">
									<label>预付款：</label> <input type="text" class="form-control"
										id="advance" placeholder="" name="advance" required/>
								</div>
								<div class="form-group"
									style="float: right; marign-right: 5px; marign-bottom: 5px;">
									<input type="submit" class="btn btn-success" value="提交" />
								</div>
							</form>
						</div>

					</div>
					<!-- /widget span5 -->
				</div>
				<!-- /row-fluid -->

				<!-- /Main window -->

			</div>
			<!--/.fluid-container-->
		</div>
		<!-- wrap ends-->
<script type="text/javascript">
debugger
function checkName(){
var a = $("#name").val();
$.post("${pageContext.request.contextPath}/user/checkUser",{"name":a},function(data){
	var sp = document.getElementById("ckName");
    var msg="";
    if(data == "1"){
        sp.style.color="red";
        msg="用户名已存在。";
    }else if(data == "0"){
        sp.style.color="green";
        msg="用户名正确。";
    }
    sp.innerHTML = msg;
})
}
</script>


		<%@ include file="../common/foot.jsp"%>
</body>
</html>
