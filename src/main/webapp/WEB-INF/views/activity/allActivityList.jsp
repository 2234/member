<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html lang="en">
<head>
</head>
<body>
	<div id="wrap">
		<!-- Top menu -->
		<%@ include file="../common/top_menu.jsp"%>

		<div class="container-fluid">

			<!-- Side menu -->
			<%@ include file="../common/side_menu.jsp"%>

			<!-- Main window -->
			<div class="main_container" id="users_page">

				<div class="row-fluid">
					<ul class="breadcrumb">
						<li>会员管理系统<span class="divider">/</span></li>
						<li>活动管理<span class="divider">/</span></li>
						<li class="active">出团信息列表</li>
					</ul>
					<h2 class="heading">活动管理</h2>
				</div>
				<!-- /row-fluid -->

				<div class="row-fluid">
					<div class="widget widget-padding span12">
						<div class="widget-header">
							<i class="icon-group"></i>
							<h5>出团信息列表</h5>

						</div>
						<div class="widget-body">
							<table id="users"
								class="table table-striped table-bordered dataTable">

								<thead>
									<tr>
										<th>序号</th>
										<th>出团编号</th>
										<th>路线起点</th>
										<th>路线终点</th>
										<th>开始时间</th>
										<th>结束时间</th>
										<th>业务经理</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${page.list}" var="list" varStatus="vs">
										<tr>
											<td>${vs.index+1 }</td>
											<td>${list.id}</td>
											<td>${list.loadStart}</td>
											<td>${list.loadEnd}</td>
											<td><fmt:formatDate value="${list.startTime}"
													type="date" pattern="yyyy-MM-dd" /></td>
											<td><fmt:formatDate value="${list.endTime}" type="date"
													pattern="yyyy-MM-dd" /></td>
											<td>${list.managerName}</td>
											<td>	
											<form
													action="${pageContext.request.contextPath}/activity/findChuTuanById?id=${list.id}"
													method="post">
													<button class="btn btn-success">查看</button>
											</form>
												<form
													action="${pageContext.request.contextPath}/activity/deleteChuTuanById?id=${list.id}"
													method="post">
													<button class="btn btn-danger">删除</button>
											</form>
											<form
													action="${pageContext.request.contextPath}/activity/lipinView?id=${list.id}"
													method="post">
													<button class="btn btn-defalut">修改出团礼品</button>
											</form>
											<form
													action="${pageContext.request.contextPath}/activity/findChuTuanTaoChanById?id=${list.id}"
													method="post">
													<button class="btn btn-info">添加业务活动</button>
											</form>
											<form
													action="${pageContext.request.contextPath}/activity/chuTuanRecordExport?id=${list.id}"
													method="post">
													<button class="btn btn-primary">导出出团信息</button>
											</form>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
							<!-- 分页 -->
							<c:if test="${page.pages>0}">
								<jsp:include page="../common/pager.jsp">
									<jsp:param value="c_id" name="paramKey" />
									<jsp:param value="${c_id}" name="paramVal" />
								</jsp:include>
							</c:if>
						</div>
						<!-- /widget-body -->
					</div>
					<!-- /widget -->
				</div>
				<!-- /row-fluid -->

			</div>
			<!-- /Main window -->

		</div>
		<!--/.fluid-container-->
	</div>
	<!-- wrap ends-->


	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1"
		style="display: none;" role="dialog" aria-labelledby="myModalLabel"
		aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only"></span>
					</button>
					<h4 class="modal-title" id="myModalLabel">修改商品信息</h4>
				</div>
				<div class="modal-body">
					<form method="POST"
						action="${pageContext.request.contextPath}/goods/updateGoods">
						<input type="hidden" name="id" id="id">
						<div class="form-group" style="margin-left: 5px;">
							<label>商品名：</label> <input type="text" class="form-control"
								id="name" placeholder="" name="name" required />
						</div>
						<div class="form-group" style="margin-left: 5px;">
							<label>种类：</label> <input type="text" class="form-control"
								id="kind" placeholder="" name="kind" required />
						</div>
						<div class="form-group"
							style="float: right; marign-right: 5px; marign-bottom: 5px;">
							<input type="submit" class="btn btn-success" value="提交" />
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<script>
		function initModal(id) {
			var args = {
				"id" : id
			};
			$.post("${pageContext.request.contextPath}/goods/selectGoods",
					args, function(data) {
						// 					var obj = eval(data);
						debugger
						$("#id").val(data.id);
						$("#name").val(data.name);
						$("#kind").val(data.kind);
					}), "JSON";
		}
	</script>

	<%@ include file="../common/foot.jsp"%>
</body>
</html>
