<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<script
	src="http://static.runoob.com/assets/jquery-validation-1.14.0/lib/jquery.js"></script>
<script
	src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script>
<script
	src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/localization/messages_zh.js"></script>
<script>
	$().ready(function() {
		$("#commentForm").validate();
	});
</script>
<style>
.error {
	color: red;
}
</style>
</head>
<body>
	<div id="wrap">
		<!-- Top menu -->
		<%@ include file="../common/top_menu.jsp"%>

		<div class="container-fluid">

			<!-- Side menu -->
			<%@ include file="../common/side_menu.jsp"%>

			<!-- Main window -->
			<div class="main_container" id="dashboard_page">

				<div class="row-fluid">
					<ul class="breadcrumb">
						<li> 会员管理系统 <span class="divider">/</span></li>
						<li> 活动管理 <span class="divider">/</span></li>
						<li class="active">新增业务场信息</li>
					</ul>
					<h2 class="heading">活动管理</h2>
				</div>

				<div class="row-fluid">
					<div class="widget-header">
						<i class="icon-plus"></i>
						<h5>新增业务场信息</h5>
						<span>${ERROR}</span>
					</div>

					<div class="widget-body" style="overflow: hidden;">
						<form method="POST" id="commentForm"
							action="${pageContext.request.contextPath}/activity/InsertUserShop">
							<div class="form-group" style="margin-left: 5px;">
								<label>活动基本信息：</label>
								<table>
									<tr>
										<th>活动编号</th>
										<th>活动第几天</th>
									</tr>
									<tr>
										<td><input type="text" class="form-control"
											id="activityId" placeholder="" name="activityId" required /></td>
										<td><input type="text" class="form-control" id="dayNum"
											placeholder="" name="dayNum" required /></td>
									</tr>
								</table>
							</div>
								<div class="form-group" style="margin-left: 5px;">
										<label>购买情况：</label>
										<div>
											<table id="shoplist" >
												<tr>
													<th>会员名称</th>
													<th>会员手机号</th>
													<th>业务经理</th>
													<th>区域</th>
												</tr>
												<tr>
													<td><input type="text" class="form-control"
														name="userName" required /></td>
													<td><input type="text" class="form-control"
														name="userPhone" required /></td>
													<td><input type="text" class="form-control"
														name="managerName" required /></td>
													<td><input type="text" class="form-control"
														name="address" required /></td>
												</tr>
												<tr>
													<th>定金</th>
													<th>预付款</th>
													<th>已付</th>
												</tr>
												<tr>
													<td><input type="text" class="form-control"
														name="deposit" required /></td>
													<td><input type="text" class="form-control"
														name="advance" required /></td>
													<td><input type="text" class="form-control"
														name="paid" required /></td>
												</tr>
												<tr>
													<th>送货地点</th>
													<th>送货数量</th>
													<th>送货费用</th>
													<th>车费</th>
												</tr>
												<tr>
													<td><input type="text" class="form-control"
														name="deliverySite" required /></td>
													<td><input type="text" class="form-control"
														name="deliveryNum" required /></td>
													<td><input type="text" class="form-control"
														name="deliveryCost" required /></td>
													<td><input type="text" class="form-control"
														name="carCost" required /></td>
												</tr>

											</table>
										</div>
								</div>



								<div class="form-group"
									style="float: right; marign-right: 5px; marign-bottom: 5px;">
									<input type="submit" class="btn btn-success" value="提交" />
								</div>
							
						</form>

					</div>
					<!-- /widget span5 -->
				</div>
				<!-- /row-fluid -->
			</div>
			<!--/.fluid-container-->
					</div>


			<%@ include file="../common/foot.jsp"%>
</body>
</html>
