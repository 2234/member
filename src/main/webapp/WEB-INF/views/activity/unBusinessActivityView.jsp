<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<script
	src="http://static.runoob.com/assets/jquery-validation-1.14.0/lib/jquery.js"></script>
<script
	src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script>
<script
	src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/localization/messages_zh.js"></script>
<script>
	$().ready(function() {
		$("#commentForm").validate();
	});
</script>
<style>
.error {
	color: red;
}
</style>
</head>
<body>
	<div id="wrap">
		<!-- Top menu -->
		<%@ include file="../common/top_menu.jsp"%>

		<div class="container-fluid">

			<!-- Side menu -->
			<%@ include file="../common/side_menu.jsp"%>

			<!-- Main window -->
			<div class="main_container" id="dashboard_page">

				<div class="row-fluid">
					<ul class="breadcrumb">
						<li>会员管理系统 <span class="divider">/</span></li>
						<li>活动管理 <span class="divider">/</span></li>
						<li class="active">新增非业务场信息</li>
					</ul>
					<h2 class="heading">活动管理</h2>
				</div>

				<div class="row-fluid">
					<div class="widget span12">
						<div class="widget-header">
							<i class="icon-plus"></i>
							<h5>新增非业务场信息</h5>
							<span>${ERROR}</span>
						</div>

						<div class="widget-body" style="overflow: hidden;">
							<form method="POST" id="commentForm"
								action="${pageContext.request.contextPath}/activity/InsertUnBusinessActivity">
								<div class="form-group" style="margin-left: 5px;">
									<label>活动基本信息：</label>
									<table>
										<tr>
											<th>活动编号</th>
											<th>活动第几天</th>
										</tr>
										<tr>
											<td><input type="text" class="form-control"
												id="activityId" placeholder="" name="activityId" /></td>
											<td><input type="text" class="form-control" id="dayNum"
												placeholder="" name="dayNum" /></td>
										</tr>
									</table>
									<div class="form-group" style="margin-left: 5px;">
										<label>餐费：</label>
										<div>
											<table id="eatlist">
												<tr>
													<th>早餐、午餐、晚餐(填写早午晚即可)</th>
													<th>地点</th>
													<th>标准</th>
													<th>费用小计</th>
													<th><button onclick="addEatActivity()">增加</button></th>
												</tr>

												<tr>
													<td><input type="text" class="form-control"
														name="eatList[0].eatTime" required /></td>
													<td><input type="text" class="form-control"
														name="eatList[0].eatPosition" required /></td>
													<td><input type="text" class="form-control"
														name="eatList[0].eatStandard" required /></td>
													<td><input type="text" class="form-control"
														name="eatList[0].eatCost" required /></td>
												</tr>

											</table>
										</div>
									</div>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>住宿：</label>
									<div>
										<table id="staylist">
											<tr>
												<th>地点</th>
												<th>标准</th>
												<th>费用小计</th>
												<th><button onclick="addStayList()">增加</button></th>
											</tr>

											<tr>
												<td><input type="text" class="form-control"
													name="stayList[0].stayPosition" required /></td>
												<td><input type="text" class="form-control"
													name="stayList[0].stayStandard" required /></td>
												<td><input type="text" class="form-control"
													name="stayList[0].stayCost" required /></td>
											</tr>

										</table>
									</div>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>晚会：</label>
									<div>
										<table id="eveninglist">
											<tr>
												<th>晚会场地费</th>
												<th>晚会演出费</th>
												<th>晚会公摊费</th>
												<th><button onclick="addEveningList()">增加</button></th>
											</tr>

											<tr>
												<td><input type="text" class="form-control"
													name="eveningList[0].eveningSiteCost" required /></td>
												<td><input type="text" class="form-control"
													name="eveningList[0].eveningShowCost" required /></td>
												<td><input type="text" class="form-control"
													name="eveningList[0].eveningShareCost" required /></td>
											</tr>

										</table>
									</div>

								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>车辆：</label>
									<div>
										<table id="carlist">
											<tr>
												<th>车型</th>
												<th>座位数</th>
												<th>数量</th>
												<th>标准</th>
												<th>费用</th>
												<th><button onclick="addCarList()">增加</button></th>
											</tr>

											<tr>
												<td><input type="text" class="form-control"
													name="carList[0].carModel" required /></td>
												<td><input type="text" class="form-control"
													name="carList[0].carSite" required /></td>
												<td><input type="text" class="form-control"
													name="carList[0].carNum" required /></td>
												<td><input type="text" class="form-control"
													name="carList[0].carStandard" required /></td>
												<td><input type="text" class="form-control"
													name="carList[0].carCost" required /></td>
											</tr>

										</table>
									</div>

								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>讲师：</label> <input type="text" class="form-control"
										id="lecturer" placeholder="" name="lecturer" minlength="2"
										required />
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>场务：</label>
								<table>
									<tr>
										<th>场务人员</th>
										<th>场务费</th>
									</tr>
									<tr>
										<td><input type="text" class="form-control" id="workers"
											placeholder="" name="workers" required /></td>
										<td><input type="text" class="form-control"
											id="workersCost" placeholder="" name="workersCost" required /></td>
									</tr>
								</table>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>公摊：</label>
								<table>
									<tr>
										<th>公摊标准</th>
										<th>公摊费用</th>
									</tr>
									<tr>
										<td><input type="text" class="form-control" id="shared"
											placeholder="" name="shared" /></td>
										<td><input type="text" class="form-control"
											id="sharedCost" placeholder="" name="sharedCost" /></td>
									</tr>
								</table>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>保险：</label>
								<table>
									<tr>
										<th>保险标准</th>
										<th>保险费用</th>
									</tr>
									<tr>
										<td><input type="text" class="form-control"
											id="insuranceStandard" placeholder=""
											name="insuranceStandard" required /></td>
										<td><input type="text" class="form-control"
											id="insuranceCost" placeholder="" name="insuranceCost"
											required /></td>
									</tr>
								</table>
								</div>
								<div class="form-group" style="margin-left: 5px;">
									<label>服务费用：</label> <input type="text" class="form-control"
										id="serviceCost" placeholder="" name="serviceCost" required />
								</div>

								<div class="form-group"
									style="float: right; marign-right: 5px; marign-bottom: 5px;">
									<input type="submit" class="btn btn-success" value="提交" />
								</div>
							</form>
						</div>

					</div>
					<!-- /widget span5 -->
				</div>
				<!-- /row-fluid -->


			</div>
			<!--/.fluid-container-->
		</div>
		<!-- wrap ends-->
		<script type="text/javascript">
			function addEatActivity() {
				var lth = $('#eatlist').children().children().length - 1;
				$("#eatlist")
						.append(
								"<tr>"
										+ "<td>"
										+ "<input type='text' class='form-control' name='eatList["+lth+"].eatTime' required />"
										+ "</td>"
										+ "<td>"
										+ "<input type='text' class='form-control' name='eatList["+lth+"].eatPosition' required />"
										+ "</td>"
										+ "<td>"
										+ "<input type='text' class='form-control' name='eatList["+lth+"].eatStandard' required />"
										+ "</td>"
										+ "<td>"
										+ "<input type='text' class='form-control' name='eatList["+lth+"].eatCost' required />"
										+ "</td>"
										+ "<td>"
										+ "<button onclick='deleteTr(this)'>移除</button>"
										+ "</td>" + "</tr>");
			}
			function addCarList() {
				var lth = $('#carlist').children().children().length - 1;
				$("#carlist")
						.append(
								"<tr>"
										+ "<td>"
										+ "<input type='text' class='form-control' name='carList["+lth+"].carModel' required />"
										+ "</td>"
										+ "<td>"
										+ "<input type='text' class='form-control' name='carList["+lth+"].carSite' required />"
										+ "</td>"
										+ "<td>"
										+ "<input type='text' class='form-control' name='carList["+lth+"].carNum' required />"
										+ "</td>"
										+ "<td>"
										+ "<input type='text' class='form-control' name='carList["+lth+"].carStandard' required />"
										+ "</td>"
										+ "<td>"
										+ "<input type='text' class='form-control' name='carList["+lth+"].eatCost' required />"
										+ "</td>"
										+ "<td>"
										+ "<button onclick='deleteTr(this)'>移除</button>"
										+ "</td>" + "</tr>");
			}
			function addEveningList() {
				var lth = $('#eveninglist').children().children().length - 1;
				$("#eveninglist")
						.append(
								"<tr>"
										+ "<td>"
										+ "<input type='text' class='form-control' name='eveningList["+lth+"].eveningSiteCost' required />"
										+ "</td>"
										+ "<td>"
										+ "<input type='text' class='form-control' name='eveningList["+lth+"].eveningShowCost' required />"
										+ "</td>"
										+ "<td>"
										+ "<input type='text' class='form-control' name='eveningList["+lth+"].eveningShareCost' required />"
										+ "</td>"
										+ "<td>"
										+ "<button onclick='deleteTr(this)'>移除</button>"
										+ "</td>" + "</tr>");
			}
			function addStayList() {
				var lth = $('#staylist').children().children().length - 1;
				$("#staylist")
						.append(
								"<tr>"
										+ "<td>"
										+ "<input type='text' class='form-control' name='stayList["+lth+"].stayPosition' required />"
										+ "</td>"
										+ "<td>"
										+ "<input type='text' class='form-control' name='stayList["+lth+"].stayStandard' required />"
										+ "</td>"
										+ "<td>"
										+ "<input type='text' class='form-control' name='stayList["+lth+"].stayCost' required />"
										+ "</td>"
										+ "<td>"
										+ "<button onclick='deleteTr(this)'>移除</button>"
										+ "</td>" + "</tr>");
			}
			function addEveningList() {
				var lth = $('#staylist').children().children().length - 1;
				$("#eveninglist")
						.append(
								"<tr>"
										+ "<td>"
										+ "<input type='text' class='form-control' name='eveningList["+lth+"].eveningSiteCost' required />"
										+ "</td>"
										+ "<td>"
										+ "<input type='text' class='form-control' name='eveningList["+lth+"].eveningShowCost' required />"
										+ "</td>"
										+ "<td>"
										+ "<input type='text' class='form-control' name='eveningList["+lth+"].eveningShareCost' required />"
										+ "</td>"
										+ "<td>"
										+ "<button onclick='deleteTr(this)'>移除</button>"
										+ "</td>" + "</tr>");
			}
			function deleteTr(e) {
				e.parentElement.parentElement.remove();
			}
		</script>


		<%@ include file="../common/foot.jsp"%>
</body>
</html>
