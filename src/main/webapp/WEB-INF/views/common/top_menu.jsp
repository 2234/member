<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>会员管理系统</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="Bluth Company">
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/resources/ico/favicon.html">
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/css/theme.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/css/alertify.css"
	rel="stylesheet">
<link rel="Favicon Icon" href="favicon.ico">
<link href="${pageContext.request.contextPath}/resources/css/fonts.css"
	rel="stylesheet">
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="${pageContext.request.contextPath}/resources/js/library/html5shiv.min.js"></script>
    <![endif]-->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/validate/jquery.validate.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery.metadata.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/validate/messages_zh.js"></script>
<script>
	$().ready(function() {
		$("#commentForm").validate({meta: "validate"});
	});
</script>
<style>
.error {
	color: red;
}
</style>
</head>
<body>
	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container-fluid">
				<div class="logo">
					<h4 style="color: white;">会员管理系统</h4>
				</div>
				<a class="btn btn-navbar visible-phone" data-toggle="collapse"
					data-target=".nav-collapse"> <span class="icon-bar"></span> <span
					class="icon-bar"></span> <span class="icon-bar"></span>
				</a> <a class="btn btn-navbar slide_menu_left visible-tablet"> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
				</a>
				<div class="top-menu visible-desktop">
					<ul class="pull-right">
						<li><a
							href="${pageContext.request.contextPath}/identity/logout"><i
								class="icon-off"></i> Logout</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</body>
</html>