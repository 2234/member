<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
</head>
<body>
	<div class="sidebar-nav nav-collapse collapse">
		<div class="accordion" id="xialaparent">
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle b_9FDDF6 collapsed"
						data-toggle="collapse" data-parent="#xialaparent" href="#huiyuan"><i
						class="icon-list-alt"></i><span> 会员管理</span></a>
				</div>
				<div id="huiyuan" class="accordion-body collapse">
					<div class="accordion-inner">
						<a class="accordion-toggle"
							href="${pageContext.request.contextPath}/user/userList"><i
							class="icon-list"></i> 会员列表</a> <a class="accordion-toggle"
							href="${pageContext.request.contextPath}/user/tiao"><i
							class="icon-edit"></i> 添加会员</a>
					</div>
				</div>
			</div>
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle b_9FDDF6 collapsed"
						data-toggle="collapse" data-parent="#xialaparent" href="#yuangong"><i
						class="icon-list-alt"></i><span> 员工管理</span></a>
				</div>
				<div id="yuangong" class="accordion-body collapse">
					<div class="accordion-inner">
						<a class="accordion-toggle"
							href="${pageContext.request.contextPath}/employee/employeeList"><i
							class="icon-list"></i> 员工列表</a> <a class="accordion-toggle"
							href="${pageContext.request.contextPath}/employee/tiao"><i
							class="icon-edit"></i> 添加员工</a>
					</div>
				</div>
			</div>
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle b_9FDDF6 collapsed"
						data-toggle="collapse" data-parent="#xialaparent"
						href="#yewujingli"><i class="icon-list-alt"></i><span>
							业务经理管理</span></a>
				</div>
				<div id="yewujingli" class="accordion-body collapse">
					<div class="accordion-inner">
						<a class="accordion-toggle"
							href="${pageContext.request.contextPath}/manager/managerList"><i
							class="icon-list"></i> 业务经理列表</a> <a class="accordion-toggle"
							href="${pageContext.request.contextPath}/manager/tiao"><i
							class="icon-edit"></i> 添加业务经理</a>
					</div>
				</div>
			</div>
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle b_9FDDF6 collapsed"
						data-toggle="collapse" data-parent="#xialaparent" href="#zuzhizhe"><i
						class="icon-list-alt"></i><span> 组织者管理</span></a>
				</div>
				<div id="zuzhizhe" class="accordion-body collapse">
					<div class="accordion-inner">
						<a class="accordion-toggle"
							href="${pageContext.request.contextPath}/organizer/organizerList"><i
							class="icon-list"></i> 组织者列表</a> <a class="accordion-toggle"
							href="${pageContext.request.contextPath}/organizer/tiao"><i
							class="icon-edit"></i> 添加组织者</a>
					</div>
				</div>
			</div>
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle b_9FDDF6 collapsed"
						data-toggle="collapse" data-parent="#xialaparent" href="#shangpin"><i
						class="icon-list-alt"></i><span> 商品管理</span></a>
				</div>
				<div id="shangpin" class="accordion-body collapse">
					<div class="accordion-inner">
						<a class="accordion-toggle"
							href="${pageContext.request.contextPath}/goods/goodsList"><i
							class="icon-list"></i> 商品列表</a> <a class="accordion-toggle"
							href="${pageContext.request.contextPath}/goods/tiao"><i
							class="icon-edit"></i> 添加商品</a>
					</div>
				</div>
			</div>
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle b_9FDDF6 collapsed"
						data-toggle="collapse" data-parent="#xialaparent" href="#duanxin"><i
						class="icon-list-alt"></i><span> 短信发送</span></a>
				</div>
				<div id="duanxin" class="accordion-body collapse">
					<div class="accordion-inner">
						<a class="accordion-toggle"
							href="${pageContext.request.contextPath}/message/tiao"><i
							class="icon-list"></i> 选择人员</a>
					</div>
				</div>
			</div>
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle b_9FDDF6 collapsed"
						data-toggle="collapse" data-parent="#xialaparent" href="#huodong"><i
						class="icon-list-alt"></i><span> 活动管理</span></a>
				</div>
				<div id="huodong" class="accordion-body collapse">
					<div class="accordion-inner">
						<a class="accordion-toggle"
							href="${pageContext.request.contextPath}/activity/allActivityList"><i
							class="icon-list"></i> 出团信息查询</a>
					</div>
					<div class="accordion-inner">
						<a class="accordion-toggle"
							href="${pageContext.request.contextPath}/activity/allActivityView"><i
							class="icon-list"></i> 添加出团信息</a>
					</div>
						<div class="accordion-inner">
						<a class="accordion-toggle"
							href="${pageContext.request.contextPath}/activity/yeWuActivityList"><i
							class="icon-list"></i> 业务活动信息查询</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>