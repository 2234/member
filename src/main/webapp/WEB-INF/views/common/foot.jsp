<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
  
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/realm.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap-colorpicker.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap-timepicker.js"></script>   
    <script type="text/javascript" src='${pageContext.request.contextPath}/resources/js/fullcalendar.min.js'></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.alertify.min.js"></script>   
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>   
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.facybox.js"></script>      
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.gritter.min.js"></script> 
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.imagesloaded.min.js"></script>
    <script type='text/javascript' src='${pageContext.request.contextPath}/resources/js/jquery.knob.js'></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.masonry.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/raphael-min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/select2.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/sparkline.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/checkinput.js"></script>

