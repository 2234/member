<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>会员管理系统</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="Bluth Company">
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/resources/ico/favicon.html">
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/css/theme.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/css/alertify.css"
	rel="stylesheet">
<link rel="Favicon Icon" href="favicon.ico">
<link href="${pageContext.request.contextPath}/resources/css/fonts.css"
	rel="stylesheet">
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
	      <script src="${pageContext.request.contextPath}/resources/js/library/html5shiv.min.js"></script>
    <![endif]-->
</head>
<body>
	<div id="wrap">
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12">
					<div class="row-fluid">
						<div class="widget container-narrow">
							<div class="widget-header">
								<i class="icon-user"></i>
								<h5>管理员登录</h5>
							</div>
							<form action="${pageContext.request.contextPath}/identity/login" method="post">
								<div class="widget-body clearfix" style="padding: 25px;">
									<div class="control-group">
										<div class="controls">
											<input class="btn-block" type="text"  name="account" id="inputTelephone"
												placeholder="输入账号">
										</div>
									</div>
									<div class="control-group">
										<div class="controls">
											<input class="btn-block" type="password"  name="password" id="inputPassword"
												placeholder="输入密码">
										</div>
									</div>
									${msg}
								<button type="submit" class="btn pull-right">登录</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!--/span10-->
			</div>
			<!--/row-fluid-->
		</div>
		<!--/.fluid-container-->
	</div>
	<!-- wrap ends-->


	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/js/realm.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/js/bootstrap.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/js/bootstrap-colorpicker.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/js/bootstrap-timepicker.js"></script>
	<script type="text/javascript"
		src='${pageContext.request.contextPath}/resources/js/echarts.js'></script>
	<script type="text/javascript"
		src='${pageContext.request.contextPath}/resources/js/fullcalendar.min.js'></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/js/jquery.alertify.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/js/jquery.facybox.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/js/jquery.gritter.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/js/jquery.imagesloaded.min.js"></script>
	<script type='text/javascript'
		src='${pageContext.request.contextPath}/resources/js/jquery.knob.js'></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/js/jquery.masonry.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/js/jquery-ui.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/js/morris.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/js/raphael-min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/js/select2.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/js/sparkline.js"></script>
</body>
</html>
